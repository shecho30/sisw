<?php

class ControladorPromocion
{
    /*=============================================
        MOSTRAR PROMOCION
        =============================================*/

        static public function ctrMostrarPromocion($item, $valor)
        {

            $tabla = "tbl_promocion";

            $respuesta = ModeloPromocion::mdlMostrarPromocion($tabla, $item, $valor);

            return $respuesta;
        }

    /*=============================================
	REGISTRO DE PROMOCION
	=============================================*/

	static public function ctrCrearPromocion()
	{

		


        if(isset($_POST["nuevoPosicion"]))
        {
			

           
			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoNombre"]))
            {

                // var_dump("olip");

			   	/*=============================================
				VALIDAR IMAGEN
				=============================================*/

				$ruta = "";

                if(isset($_FILES["fotoPro"]["tmp_name"]))
                {

					list($ancho, $alto) = getimagesize($_FILES["fotoPro"]["tmp_name"]);

					if ($_POST["nuevoPosicion"] == 1){
						$nuevoAncho = 1000;
						$nuevoAlto = 600;
					} else{
						$nuevoAncho = 540;
						$nuevoAlto = 720;
					}

					

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "vistas/img/promocion/".$_POST["nuevoNombre"];

					mkdir($directorio, 0755);

					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

                    if($_FILES["fotoPro"]["type"] == "image/jpeg")
                    {

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/promocion/".$_POST["nuevoNombre"]."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["fotoPro"]["tmp_name"]);

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

                    if($_FILES["fotoPro"]["type"] == "image/png")
                    {

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/promocion/".$_POST["nuevoNombre"]."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["fotoPro"]["tmp_name"]);

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}

				}
				
                $tabla = "tbl_promocion";

				$datos = array("nombre_promocion" 	    => $_POST["nuevoNombre"],
							   "imagen_promocion" 	    => $ruta,
							   "enlace_promocion" 		=> $_POST["nuevoEnlace"],
							   "posicion_promocion" 	=> $_POST["nuevoPosicion"]);
                
                	$respuesta = ModeloPromocion::mdlIngresarPromocion($tabla, $datos);

                    if($respuesta == "ok")
                    {

                		echo '<script>

                		swal({

                			type: "success",
                			title: "¡La promocion se guardo correctamente!",
                			showConfirmButton: true,
                			confirmButtonText: "Cerrar"

                		}).then(function(result){

                			if(result.value){

                				window.location = "promocion";

                			}

                		});


                		</script>';


                    }
                    else
                    {

                        echo '<script>

                            swal({

                                type: "error",
                                title: "¡no se pudo guardar la promocion!",
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"

                            }).then(function(result){

                                if(result.value){

                                    window.location = "promocion";

                                }

                            });


                        </script>';

                    }

                }
		    }
		}

		/*=============================================
	EDITAR USUARIO
	=============================================*/

	static public function ctrEditarPromocion()
	{

		if(isset($_POST["editPromocion"]))
		{

				/*=============================================
				VALIDAR IMAGEN
				=============================================*/

				$ruta = $_POST["fotoActualPromo"];

				if(isset($_FILES["EditFoto"]["tmp_name"]) && !empty($_FILES["EditFoto"]["tmp_name"]))
				{

					list($ancho, $alto) = getimagesize($_FILES["EditFoto"]["tmp_name"]);

					if ($_POST["editPosicion"] == 1){
						$nuevoAncho = 1000;
						$nuevoAlto = 600;
					} else{
						$nuevoAncho = 540;
						$nuevoAlto = 720;
					}

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "vistas/img/promocion/".$_POST["editNombre"];

					/*=============================================
					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD
					=============================================*/

					if(!empty($_POST["fotoActualPromo"]))
					{

						unlink($_POST["fotoActualPromo"]);

					}
					else
					{

						mkdir($directorio, 0755);

					}	

					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["EditFoto"]["type"] == "image/jpeg")
					{

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/promocion/".$_POST["editNombre"]."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["EditFoto"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

					if($_FILES["EditFoto"]["type"] == "image/png")
					{

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/promocion/".$_POST["editNombre"]."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["EditFoto"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}

				}

				$tabla = "tbl_promocion";

				
				/*$datos = array("nombre" => $_POST["editarNombre"],
							   "usuario" => $_POST["editarUsuario"],
							   "password" => $encriptar,
							   "perfil" => $_POST["editarPerfil"],
							   "foto" => $ruta);*/

				$datos = array("id_promocion" 			=> $_POST["editPromocion"],
							   "posicion_promocion" 	=> $_POST["editPosicion"],
							   "nombre_promocion" 		=> $_POST["editNombre"],
							   "enlace_promocion" 		=> $_POST["EditEnlace"],
					           "imagen_promocion"		=> $ruta);

				 $respuesta = ModeloPromocion::mdlEditarPromocion($tabla, $datos);

				if($respuesta == "ok")
				{

					echo'<script>

					swal({
						  type: "success",
						  title: "promocion actualizada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result) {
									if (result.value) {

									window.location = "promocion";

									}
								})

					</script>';

				


				}
				else
				{

					echo'<script>

						swal({
							type: "error",
							title: "¡no se pudo actualizar la promocion!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"
							}).then(function(result) {
								if (result.value) {

								window.location = "promocion";

								}
							})

					</script>';

				}

		}

	}


	/*=============================================
	BORRAR USUARIO
	=============================================*/

	static public function ctrBorrarPromocion()
	{

		if(isset($_GET["idPromocion"]))
		{
			

			$tabla ="tbl_promocion";
			$datos = $_GET["idPromocion"];

			if($_GET["fotoPromocion"] != ""){

				unlink($_GET["fotoPromocion"]);
				rmdir('vistas/img/promocion/'.$_GET["nombrePromocion"]);

			}

			$respuesta = ModeloPromocion::mdlBorrarPromocion($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "La promocion fue borrada",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result) {
								if (result.value) {

								window.location = "promocion";

								}
							})

				</script>';

			}		

		}

	}
}