<?php

class ControladorClientes{

	/*=============================================
	CREAR CLIENTES
	=============================================*/

	static public function ctrCrearCliente()
	{

		if(isset($_POST["newName"]))
		{

			// if(preg_match('/^[0-9]+$/',                     $_POST["newDocument"])        &&
			//    preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',  $_POST["newCompanyCustomer"]) &&
			//    preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',  $_POST["newName"])            &&
			//    preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',  $_POST["newLastName"])        &&
			//    preg_match('/^[0-9]+$/',                     $_POST["newPhone"])           &&
			//    preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',  $_POST["newAddress"])         &&
			//    preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["newEmail"]) && 
			//    preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',  $_POST["newPassword"]))
			// {


				$encriptar = md5($_POST["newPassword"]);

			   	$tabla = "tbl_cliente";

			   	$datos = array("document_cliente"      =>$_POST["newDocument"],
					           "id_tipo_documento"     =>$_POST["newTypeDocument"],
					           "empresa_cliente"       =>$_POST["newCompanyCustomer"],
					           "nombre_repre_cliente"  =>$_POST["newName"],
					           "apellido_repre_cliente"=>$_POST["newLastName"],
					           "telefono_cliente"      =>$_POST["newPhone"],
					       	   "direccion_cliente"     =>$_POST["newAddress"],
					       	   "correo_cliente"        =>$_POST["newEmail"],
					       	   "contrasena_cliente"    =>$encriptar,
					       	   "id_estado_usuario"     =>$_POST["newState"],
					       	   "fecha_creacion_cliente"=>$_POST["newDate"],
					       	   "id_ciudad"             =>$_POST["newCity"]);

				// print_r($datos);

				 $_SESSION["reenvio"] = "2";
				
			   	$respuesta = ModeloClientes::mdlIngresarCliente($tabla, $datos);

				   print_r($respuesta);
				if($respuesta == "ok")

				{

					echo'<script>

					swal({
						  type: "success",
						  title: "El cliente ha sido guardado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){

							if(result.value){
							
								window.location = "clientes";
	
							}
	
						});

					</script>';

				}
				else
				{

					echo'<script>

						swal({
							type: "error",
							title: "¡El cliente no puede ir vacío o llevar caracteres especiales!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"
							})

					</script>';



				}
			// }
		}

	}

	/*=============================================
	MOSTRAR CLIENTES
	=============================================*/

	static public function ctrMostrarClientes($item, $valor){

		$tabla  = "tbl_cliente";

		$respuesta = ModeloClientes::mdlMostrarClientes($tabla, $item, $valor);

		//var_dump($respuesta);

		return $respuesta;

	}
	/*=============================================
	MOSTRAR Id Clientes para poder almacenarlos
	=============================================*/

	static public function ctrMostrarDocumento($item, $valor){

		$tabla  = "tbl_tipo_documento";

		$respuesta = ModeloClientes::mdlMostrarDocumento($tabla, $item, $valor);

		//var_dump($respuesta);

		return $respuesta;

	}

	

	/*=============================================
	MOSTRAR Ciudad
	=============================================*/

	static public function ctrMostrarCiudad($item, $valor){

		$tabla  = "tbl_ciudad";

		$respuesta = ModeloClientes::mdlMostrarCiudad($tabla, $item, $valor);

		//var_dump($respuesta);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR Id del usuario
	=============================================*/

	static public function ctrEstadoUsuario($item, $valor){

		$tabla  = "tbl_estado_usuario";

		$respuesta = ModeloClientes::mdlEstadoUsuario($tabla, $item, $valor);

		//var_dump($respuesta);

		return $respuesta;

	}

	/*=============================================
	EDITAR CLIENTE
	=============================================*/

	static public function ctrEditarCliente(){


		if(isset($_POST["editDocument"])){


			if(preg_match('/^[0-9]+$/', $_POST["editDocument"])){


			   	$tabla = "tbl_cliente";

			   if ($_POST["editPassword"] != "") {

			   	if (preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ]+$/',  $_POST["editPassword"])) {

			   		$encriptar = md5($_POST["editPassword"]);

			    }else{

			    	echo'<script>

					swal({
						  type: "error",
						  title: "¡La contraseña del cliente no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "clientes";

							}
						})

			  	</script>';


			    }

			   }else{

			   		$encriptar = $passwordActual;

			   }

			   	$datos = array( "id_cliente"            =>$_POST["id_Cliente"],
			   					"document_cliente"      =>$_POST["editDocument"],
					            "id_tipo_documento"     =>$_POST["editTypeDocument"],
					            "empresa_cliente"       =>$_POST["editCompanyCustomer"],
					            "nombre_repre_cliente"  =>$_POST["editName"],
					            "apellido_repre_cliente"=>$_POST["editLastName"],
					            "telefono_cliente"      =>$_POST["editPhone"],
					       	    "direccion_cliente"     =>$_POST["editAddress"],
					       	    "correo_cliente"        =>$_POST["editEmail"],
					       	    "contrasena_cliente"    =>$encriptar,
					       	    "id_estado_usuario"     =>$_POST["editState"],
					       	    "id_ciudad"             =>$_POST["editCity"]);

			   	echo json_encode($datos);

				$_SESSION["reenvio"] = "2";
			   	$respuesta = ModeloClientes::mdlEditarCliente($tabla, $datos);

			   	if($respuesta == "ok"){


			   	//echo json_encode($datos);

					echo'<script>

					swal({
						  type: "success",
						  title: "El cliente ha sido Actualizado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "clientes";

									}
								})

					</script>';

				}

			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El cliente no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "clientes";

							}
						})

			  	</script>';



			}

		}


	}

	/*=============================================
	ELIMINAR CLIENTE
	=============================================*/

	static public function ctrEliminarCliente(){

		if(isset($_GET["idCliente"])){

			$tabla ="tbl_cliente";
			$datos = $_GET["idCliente"];

			$respuesta = ModeloClientes::mdlEliminarCliente($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "El cliente ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result){
								if (result.value) {

								window.location = "clientes";

								}
							})

				</script>';

			}		

		}

	}


	/*=============================================
	MOSTRAR CLIENTES PARA VENTA
	=============================================*/

	static public function ctrMostrarClientesV($item, $valor){

		$tabla  = "tbl_cliente";

		$respuesta = ModeloClientes::mdlMostrarClientesV($tabla, $item, $valor);

		return $respuesta;

	}
}
