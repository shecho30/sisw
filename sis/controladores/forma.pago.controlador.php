<?php

class ControladorFormaPago{

	/*=============================================
	CREAR CATEGORIAS
	=============================================*/

	static public function ctrCrearFormaPago(){

		if(isset($_POST["nuevaFormaPago"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaFormaPago"])){

				$tabla = "tbl_forma_pago";

				$datos = $_POST["nuevaFormaPago"];

				/*$datos = array("nombre_forma_pago"=>$_POST["nuevaFormaPago"],
							   /*"descripcion_categoria"=>$_POST["descripcionCategoria"]);*/

				//print_r($datos);
				$respuesta = ModeloFormaPago::mdlIngresarFormaPago($tabla, $datos);


				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La forma de pago ha sido guardada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "formapago";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La forma de pago no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "formapago";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	MOSTRAR CATEGORIAS
	=============================================*/

	static public function ctrMostrarFormaPago($item, $valor){

		$tabla = "tbl_forma_pago";

		$respuesta = ModeloFormaPago::mdlMostrarFormaPago(
			$tabla, 
			$item, $valor);

		return $respuesta;
	
	}

	/*=============================================
	EDITAR CATEGORIA
	=============================================*/

	static public function ctrEditarFormaPago(){

		if(isset($_POST["editarFormaPago"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', 
				$_POST["editarFormaPago"])){

				$tabla = "tbl_forma_pago";
//$datos= $_POST["idFormaPago"];
				
				

				$datos = array(
					"nombre_forma_pago"=>$_POST["editarFormaPago"],
					"id_forma_pago"=>$_POST["idFormaPago"]
				);

				//print_r($datos);

				$respuesta = ModeloFormaPago::mdlEditarFormaPago($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La forma de pago ha sido cambiada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "formapago";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La forma de pago no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "formapago";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	BORRAR CATEGORIA
	=============================================*/

	static public function ctrBorrarCategoria(){

		if(isset($_GET["idCategoria"])){

			$tabla ="Categorias";
			$datos = $_GET["idCategoria"];

			$respuesta = ModeloCategorias::mdlBorrarCategoria($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "La categoría ha sido borrada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "categorias";

									}
								})

					</script>';
			}
		}
		
	}
}
