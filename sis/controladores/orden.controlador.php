<?php

class ControladorOrden
{
/*=============================================
	MOSTRAR ORDEN COMPRA
	=============================================*/

    static public function ctrMostrarOrden($item, $valor)
    {

		$tabla = "tbl_orden_compra";

		$respuesta = ModeloOrden::mdlMostrarOrden($tabla, $item, $valor);

		return $respuesta;
	}

  static public function ctrCosultarOrdenPdf($item,$tabla){


		$respuesta = ModeloOrden::mdlMostrarOrdenPdf($item,$tabla);

		return $respuesta;

  }

	/*=============================================
	MOSTRAR ORDEN Producto
	=============================================*/

    static public function ctrMostrarOrdenProducto($item, $valor)
    {

		$tabla = "tbl_orden_producto";

		$respuesta = ModeloOrden::mdlMostrarOrdenProducto($tabla, $item, $valor);
		return $respuesta;
	}


	/*=============================================
	MOSTRAR ORDEN COMPRA DETALLADA
	=============================================*/

    static public function ctrMostrarOrdenDetalle($valor)
    {
		$respuesta = ModeloOrden::mdlMostrarOrdenDetallada($valor);

		return $respuesta;
	}

/*=============================================
	INSERTAR ORDEN COMPRA
	=============================================*/

		static public function ctrCrearOrden()
	{

		if(isset($_POST["nueva_orden"]))
		{
			$tabla = "tbl_orden_compra";

				$datos = array("nueva_orden" => $_POST["nueva_orden"],
							   "nuevo_proveedor" => $_POST["nuevo_proveedor"],
                  "nuevo_detalle" => $_POST["nuevo_detalle"]);

				//print_r($datos);
				$respuesta = ModeloOrden::mdlIngresarOrden($tabla, $datos);

				if($respuesta == "ok")
				{

					echo '<script>

					swal({

						type: "success",
						title: "¡Orden de compra Creada Correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "orden";

						}

					});


					</script>';


				}
				else
				{

					echo '<script>

						swal({

							type: "error",
							title: "¡Error al crear orden de compra!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){

								window.location = "orden";

							}

						});


					</script>';

				}
		}
	}



/*=============================================
	INSERTAR ORDEN PRODUCTO COMPRA
	=============================================*/

		static public function ctrCrearOrdenProducto()
		{
			if(isset($_POST["nuevo_precio"]))
			{
				$tabla = "tbl_orden_producto";

				$total = $_POST["nuevo_precio"] * $_POST["nuevo_cantidad"];

				$datos = array("insert_id_producto" => $_POST["insert_id_producto"],
							   "insert_id_orden" => $_POST["insert_id_orden"],
							   "nuevo_precio" => $_POST["nuevo_precio"],
							   "nuevo_cantidad" => $_POST["nuevo_cantidad"],
							   "total_final" => $total);

        print_r($datos);

				$respuesta = ModeloOrden::mdlIngresarOrdenProducto($tabla, $datos);

				if($respuesta == "ok")
				{

					echo '<script>

					swal({

						type: "success",
						title: "¡Orden de compra Creada Correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "orden";

						}

					});


					</script>';


				}
				else
				{

					echo '<script>

						swal({

							type: "error",
							title: "¡Error al crear orden de compra!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){

								window.location = "orden";

							}

						});


					</script>';

				}

			}
		}


	/*=============================================
	Eliminar Orden de compra
	=============================================*/

	static public function ctrEliminarOrdenCompra()
	{
		if(isset($_GET["idOrden"]))
		{
			$valor = $_GET["idOrden"];
			$response = ModeloOrden::mdlBorrarOrdenCompra($valor);

			if($response == "ok")
			{

				echo '<script>

				swal({

					type: "success",
					title: "¡Orden de compra Creada Correctamente!",
					showConfirmButton: true,
					confirmButtonText: "Cerrar"

				}).then(function(result){

					if(result.value){

						window.location = "orden";

					}

				});


				</script>';


			}
			else
			{

				echo '<script>

					swal({

						type: "error",
						title: "¡Error al crear orden de compra!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "orden";

						}

					});


				</script>';

			}

		}
	}


	/*=============================================
	ACTUALIZAR Orden de compra
	=============================================*/

	static public function ctrEditarOrden()
	{

		if(isset($_POST["nueva_orden_edit"]))
		{

			$tabla = "tbl_orden_compra";

			$datos = array("id_orden" => $_POST["nuevo_id_edit"],
							"nombre_orden" => $_POST["nueva_orden_edit"],
							"id_proveedor" =>$_POST["nuevo_proveedor_edit"],
							"detalle_orden" => $_POST["edit_nuevo_detalle"]);

			$response = ModeloOrden::mdlEditarOrdenCompra($tabla, $datos);

			if($response == "ok")
			{

				echo '<script>

				swal({

					type: "success",
					title: "¡Orden de compra Creada Correctamente!",
					showConfirmButton: true,
					confirmButtonText: "Cerrar"

				}).then(function(result){

					if(result.value){

						window.location = "orden";

					}

				});


				</script>';


			}
			else
			{

				echo '<script>

					swal({

						type: "error",
						title: "¡Error al crear orden de compra!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "orden";

						}

					});


				</script>';

			}
		}
	}


	/*=============================================
	Eliminar Orden de Producto
	=============================================*/

	static public function ctrEliminarOrdenProducto()
	{
		if(isset($_GET["id_prod"]))
		{
			$valor = $_GET["id_prod"];
			$tabla = "tbl_orden_producto";
			$response = ModeloOrden::mdlBorrarOrdenProducto($tabla,$valor);


			if($response == "ok")
			{

				echo '<script>

				swal({

					type: "success",
					title: "¡Orden de compra Creada Correctamente!",
					showConfirmButton: true,
					confirmButtonText: "Cerrar"

				}).then(function(result){

					if(result.value){

						window.location = "orden";

					}

				});


				</script>';


			}
			else
			{

				echo '<script>

					swal({

						type: "error",
						title: "¡Error al crear orden de compra!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "orden";

						}

					});


				</script>';

			}

		}
	}

/*=============================================
	ACTUALIZAR ORDEN PRODUCTO
	=============================================*/

	static public function ctrCrearOrdenProEditar()
	{

		if(isset($_POST["_id"]))
		{

			$tabla = "tbl_orden_producto";
			$total = $_POST["_nuevo_precio"] * $_POST["_nuevo_cantidad"];

			$datos = array("_id" => $_POST["_id"],
							"_nuevo_precio" => $_POST["_nuevo_precio"],
							"_nuevo_cantidad" =>$_POST["_nuevo_cantidad"],
							"_nuevo_total" => $total);

			// print_r($datos);

			$response = ModeloOrden::mdlEditarOrdenProducto($tabla, $datos);

			if($response == "ok")
			{

				echo '<script>

				swal({

					type: "success",
					title: "¡Orden de compra Creada Correctamente!",
					showConfirmButton: true,
					confirmButtonText: "Cerrar"

				}).then(function(result){

					if(result.value){

						window.location = "orden";

					}

				});


				</script>';


			}
			else
			{

				echo '<script>

					swal({

						type: "error",
						title: "¡Error al crear orden de compra!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){

							window.location = "orden";

						}

					});


				</script>';

			}
		}
	}

}
