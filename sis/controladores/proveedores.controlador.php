<?php

class ControladorProveedor{


/*=============================================
	MOSTRAR PROVEEDORES
	=============================================*/

    static public function ctrMostrarProveedores($item, $valor)
    {

		$tabla = "tbl_proveedor";

		$respuesta = ModeloProveedor::mdlMostrarProveedor($tabla, $item, $valor);

		return $respuesta;
	}

	/*=============================================
	CREAR PROVEEDORES
	=============================================*/
	
	static public function ctrCrearProveedor()
	{

		if(isset($_POST["nuevoTiDocu"]))
		{
			$tabla = "tbl_proveedor";

				//$encriptar = crypt($_POST["nuevoPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$datos = array("tipo_documento" 	=> $_POST["nuevoTiDocu"],
							   "numero_documento" 	=> $_POST["nuevoNuDocu"],
							   "nombre_empresa"		=> $_POST["nuevoNombreEmp"],
							   "nombre_repre"   	=> $_POST["nuevoNombre"],
							   "apellido_repre"     => $_POST["nuevoApellido"],
							   "correo_repre"       => $_POST["nuevoCorreo"],
					           "telefono_repre" 	=> $_POST["nuevoTelefono"],
					           "direccion_empresa" 	=> $_POST["nuevoDir"],
							   "ciudad_proveedor"   => $_POST["nuevoCiudad"],
							   "producto_proveedor" => $_POST["nuevoPro"],
					           "imagen"            	=> null);

				//print_r($datos);
				$respuesta = ModeloProveedor::mdlIngresarProveedor($tabla, $datos);
			
				if($respuesta == "ok")
				{

					echo '<script>

					swal({

						type: "success",
						title: "¡El usuario ha sido guardado correctamente!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"

					}).then(function(result){

						if(result.value){
						
							window.location = "proveedores";

						}

					});
				

					</script>';	


				}
				else
				{

					echo '<script>

						swal({

							type: "error",
							title: "¡El usuario no puede ir vacío o llevar caracteres especiales!",
							showConfirmButton: true,
							confirmButtonText: "Cerrar"

						}).then(function(result){

							if(result.value){
							
								window.location = "proveedores";

							}

						});
					

					</script>';

				} 
		}
	}


	/*=============================================
	BORRAR PROVEEDORES
	=============================================*/
	static public function ctrBorrarProveedor()
	{
		if(isset($_GET["idProveedor"]))
		{

			$tabla ="tbl_proveedor";
			$datos = $_GET["idProveedor"];

			/*if($_GET["fotoUsuario"] != ""){

				unlink($_GET["fotoUsuario"]);
				rmdir('vistas/img/usuarios/'.$_GET["usuario"]);

			}*/

			/*$respuesta = ModeloUsuarios::mdlBorrarUsuario($tabla, $datos);*/
			$respuesta = ModeloProveedor::mdlBorrarProveedor($tabla, $datos);
			
			// echo "<script>alert($datos);</script>";
			if($respuesta == "ok")
			{

				echo'<script>

				swal({
					  type: "success",
					  title: "El proveedor ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result) {
								if (result.value) {

								window.location = "proveedores";

								}
							})

				</script>';

			}

		}

	}

	/*=============================================
	EDITAR PROVEEDOR
	=============================================*/

	static public function ctrEditarProveedor()
	{

		if(isset($_POST["nuevoTiDocuEdit"]))
		{
			
			$tabla = "tbl_proveedor";


			/*$datos = array("nombre" => $_POST["editarNombre"],
						   "usuario" => $_POST["editarUsuario"],
						   "password" => $encriptar,
						   "perfil" => $_POST["editarPerfil"],
						   "foto" => $ruta);*/

			$datos = array("idProveedor"        => $_POST["idProveedor"],
						   "nuevoTiDocuEdit" 	=> $_POST["nuevoTiDocuEdit"],
						   "nuevoNuDocuEdit" 	=> $_POST["nuevoNuDocuEdit"],
						   "nuevoNombreEmpEdit" => $_POST["nuevoNombreEmpEdit"],
						   "nuevoNombreEdit" 	=> $_POST["nuevoNombreEdit"],
						   "nuevoApellidoEdit"  => $_POST["nuevoApellidoEdit"],
						   "nuevoCorreoEdit"    => $_POST["nuevoCorreoEdit"],
						   "nuevoTelefonoEdit" 	=> $_POST["nuevoTelefonoEdit"],
						   "nuevoDirEdit" 	    => $_POST["nuevoDirEdit"],
						   "nuevoCiudadEdit"    => $_POST["nuevoCiudadEdit"],
						   "nuevoProEdit" 	    => $_POST["nuevoProEdit"],
						   "nuevaFotoEdit" 	    => null);
						  
			//print_r($datos);
			
			$respuesta = ModeloProveedor::mdlEditarProveedor($tabla, $datos);

			if($respuesta == "ok")
			{

				echo'<script>

				swal({
					  type: "success",
					  title: "El proveedor ha sido editado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar"
					  }).then(function(result) {
								if (result.value) {

								window.location = "proveedores";

								}
							})

				</script>';

			}
			else
			{
				echo'<script>

					swal({
						type: "error",
						title: "¡El nombre no puede ir vacío o llevar caracteres especiales!",
						showConfirmButton: true,
						confirmButtonText: "Cerrar"
						}).then(function(result) {
							if (result.value) {

							window.location = "proveedores";

							}
						})

				</script>';
		 	}
		}
	}

}


?>
