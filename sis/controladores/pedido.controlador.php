<?php 
/**
 * 
 */
class ControladorPedidos 
{
	public function ctrMostrarPedidos($item, $valor){
		$tabla = "tbl_pedido";

		$respuesta = ModeloPedidos::mdlMostrarPedidos($tabla,$item,$valor);
		return $respuesta;
	}
	public function ctrMostrarClientes($item, $valor){
		$tabla = "tbl_cliente";

		$respuesta = ModeloPedidos::mdlMostrarClientes($tabla, $item, $valor);

		return $respuesta;
	}
	public function ctrMostrarProductos($item, $valor){
		$tabla = "tbl_producto";

		$respuesta = ModeloPedidos::mdlMostrarProductos($tabla, $item, $valor);
		
		return $respuesta;
	}
	/*Agregar pedido */
	public function ctrCrearPedido(){
		if (isset($_POST['newPedido'])) {
			if (preg_match('/^[0-9]+$/', $_POST["newPedido"])&&
				preg_match('/^[0-9]+$/', $_POST["newCantidad"])) {

				$tablaProductos = "tbl_producto";

			    $item = "id_producto";
				$valor = $_POST["newProducto"];
				

				$traerProducto = ModeloProductos::mdlMostrarProductosv($tablaProductos, $item, $valor);
				

				$item1a = "cantidad_producto";
				$valor1a = $_POST["newCantidad"] + $traerProducto["cantidad_producto"];

			    $nuevasVentas = ModeloProductos::mdlActualizarProductoV($tablaProductos, $item1a, $valor1a, $valor);
				
				$tabla= "tbl_pedido";

				$datos= array ("numero_pedido"         =>$_POST["newPedido"],
							   "id_cliente"            =>$_POST["newCliente"],
							   "id_producto"           =>$_POST["newProducto"],
							   "fecha_creacion_pedido" =>$_POST["newDate"],
							   "cantidad_pedido"       =>$_POST["newCantidad"]);

				$respuesta= ModeloPedidos::mdlAgregarPedidos($tabla,$datos);
				
					if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El pedido ha sido guardado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "crear-pedido";

									}
								})

					</script>';

				}

			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El pedido no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "crear-pedido";

							}
						})

			  	</script>';



			}

		}
	}
	public function ctrEditarPedido(){
		if (isset($_POST['editPedido'])) {
			if (preg_match('/^[0-9]+$/', $_POST["editPedido"])&&
				preg_match('/^[0-9]+$/', $_POST["editCantidad"])) {
				
				$tabla= "tbl_pedido";

				$datos= array ("id_pedido"             =>$_POST["id_pedido"],
							   "numero_pedido"         =>$_POST["editPedido"],
							   "id_cliente"            =>$_POST["editCliente"],
							   "id_producto"           =>$_POST["editProducto"],
							   "cantidad_pedido"       =>$_POST["editCantidad"]);

				$respuesta= ModeloPedidos::mdlEditarPedidos($tabla,$datos);
				
					if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El pedido ha sido editado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "crear-pedido";

									}
								})

					</script>';

				}

			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El pedido no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "crear-pedido";

							}
						})

			  	</script>';

			}

		}
	}

	static public function ctrEliminarPedido(){

		if(isset($_GET["idPedido"])){

			$tabla ="tbl_pedido";
			$datos = $_GET["idPedido"];


			$respuesta = ModeloPedidos::mdlEliminarPedido($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "El pedido ha sido borrado correctamente",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result){
								if (result.value) {

								window.location = "crear-pedido";

								}
							})

				</script>';

			}		

		}

	}

}

?>