<?php

class ControladorEstadoProducto{

  /*=============================================
  CREAR ESTADO PRODUCTO
  =============================================*/

  static public function ctrCrearEstadoProducto(){
    if(isset($_POST["nuevoEstado"])){

      if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoEstado"])){

        $tabla = "tbl_estado_producto";

        $datos = $_POST["nuevoEstado"];

        $respuesta = ModeloEstadosProductos::mdlIngresarEstadoProducto($tabla, $datos);

        if($respuesta == "ok"){

          echo'<script>

          swal({
              type: "success",
              title: "La categoría ha sido guardada correctamente",
              showConfirmButton: true,
              confirmButtonText: "Cerrar"
              }).then(function(result){
                  if (result.value) {

                  window.location = "estado-productos";

                  }
                })

          </script>';

        }


      }else{

        echo'<script>

          swal({
              type: "error",
              title: "¡La categoría no puede ir vacía o llevar caracteres especiales!",
              showConfirmButton: true,
              confirmButtonText: "Cerrar"
              }).then(function(result){
              if (result.value) {

              window.location = "estado-productos";

              }
            })

          </script>';

      }

    }
  }

  /*=============================================
  MOSTRAR ESTADO PRODUCTO
  =============================================*/

  static public function ctrMostrarEstadoProducto($item, $valor){

    $tabla = "tbl_estado_producto";

    $respuesta = ModeloEstadosProductos::mdlMostrarEstadoProductos($tabla, $item, $valor);

    return $respuesta;
  }

  /*=============================================
  ACTUALIZAR ESTADO PRODUCTO
  =============================================*/

  static public function ctrActualizarEstadoProducto(){
     if(isset($_POST["nuevoEstadoProducto"])){
      if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoEstadoProducto"])){

        $tabla = "tbl_estado_producto";

        $datos = ["nombre_estado_producto" => $_POST['nuevoEstadoProducto'],
                  "id_estado_producto" => $_POST["idEProducto"]];

        $respuesta = ModeloEstadosProductos::mdlActualizarEstadoProductos($tabla, $datos);
        if($respuesta){
          echo'<script>
            swal({
              type: "success",
              title: "El nombre del estado ha sido cambiado",
              showConfirmButton: true,
              confirmButtonText: "Cerrar"
              }).then(function(result){
                  if (result.value) {
                  window.location = "estado-productos";
                  }
                })
          </script>';
        }else{
          echo'<script>
            swal({
                type: "error",
                title: "¡El estado no pudo ser actualizado!",
                showConfirmButton: true,
                confirmButtonText: "Cerrar"
                }).then(function(result){
                if (result.value) {
                  window.location = "estado-productos";
                }
              })
            </script>';
          }
      }      
    }  
  }

  /*=============================================
  BORRAR ESTADO PRODUCTO
  =============================================*/

  static public function ctrEliminarEstadoProducto(){   
      $tabla ="tbl_estado_producto";
      $datos = $_POST["idEstadoProductoEliminar"];

      $respuesta = ModeloEstadosProductos::mdlEliminarEstadoProducto($tabla, $datos);
      return $respuesta;    
  }
}
