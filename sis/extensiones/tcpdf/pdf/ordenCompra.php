<?php

require_once "../../../controladores/orden.controlador.php";
require_once "../../../modelos/orden.modelo.php";

class OrdenCompra {


public function get_Orden($item){

$direccion = "Calle 44B 92-11";
$telefono = "300 786 52 49";
$nit = "71.759.963-9";
$correo = "ventas@inventorysystem.com";

$resultCabecera = ControladorOrden::ctrCosultarOrdenPdf($item,"tbl_orden_compra");
    //print_r($result);
$idOrdenCompra = $resultCabecera['id_orden_compra'];
$nombreOrdenCompra = $resultCabecera['nombre_orden_compra'];
$fechaOrdenCompra = substr($resultCabecera['fecha_creacion_orden_compra'], 0, 10);
$detalleOrden = $resultCabecera['detalle_orden'];
$documentoProveedor = $resultCabecera['documento_proveedor'];
$empresaProveeedor = $resultCabecera['empresa_proveedor'];

$productos='';

$resultProducto = ControladorOrden::ctrCosultarOrdenPdf($item,"tbl_orden_producto");

$count = 1;
$totalOrden = 0;

foreach ($resultProducto as $value) {
		$productos .='<tr>';
		$productos .='<td style="border: 1px solid #666; color:#333; background-color:white; width:45px; text-align:center"><b>'.$count++.'</b></td>';
		$productos .='<td style="border: 1px solid #666; color:#333; background-color:white; width:260px; text-align:center">'.$value['nombre_producto'].'</td>';
		$productos .='<td style="border: 1px solid #666; color:#333; background-color:white; width:65px; text-align:center">'.$value['cantidad_producto'].'</td>';
		$productos .='<td style="border: 1px solid #666; color:#333; background-color:white; width:85px; text-align:center">$ '.$value['precio_unidad'].'</td>';
		$productos .='<td style="border: 1px solid #666; color:#333; background-color:white; width:85px; text-align:center">$ '.$value['total_producto'].'</td>';
		$productos .='</tr>';

		$totalOrden += $value['total_producto'];
}


require_once('tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->startPageGroup();

$pdf->AddPage();


$bloque1 = <<<EOF

	<table>

		<tr>

			<td style="width:150px"><img src="images/logo-negro-bloque.png"></td>

			<td style="background-color:white; width:140px">

				<div style="font-size:8.5px; text-align:right; line-height:15px;">

					<br>
					<b>NIT: $nit</b>

					<br>
					<b>Dirección: $direccion</b>

				</div>

			</td>

			<td style="background-color:white; width:140px">

				<div style="font-size:8.5px; text-align:right; line-height:15px;">

					<br>
					<b>Teléfono: $telefono</b>

					<br>
					<b>$correo</b>

				</div>

			</td>

			<td style="background-color:white; width:110px; text-align:center; color:red"><br><br>ORDEN DE COMPRA N. $idOrdenCompra<br></td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($bloque1, false, false, false, false, '');


$bloque2 = <<<EOF

	<table>

		<tr>

			<td style="width:540px"><img src="images/back.jpg"></td>

		</tr>

	</table>

	<table style="font-size:10px; padding:5px 10px;">

		<tr>

			<td style="border: 1px solid #666; background-color:white; width:290px">

				<b>Proveedor:</b> $empresaProveeedor

			</td>

      <td style="border: 1px solid #666; background-color:white; width:100px">

				<b>NIT:</b> $documentoProveedor

			</td>

			<td style="border: 1px solid #666; background-color:white; width:150px; text-align:right">

				<b>Fecha Orden:</b> $fechaOrdenCompra

			</td>

		</tr>

		<tr>

			<td style="border: 1px solid #666; background-color:white; width:540px"><b>Nombre Orden de Compra:</b> $nombreOrdenCompra</td>

		</tr>

    <tr>

			<td style="border: 1px solid #666; background-color:white; width:540px"><b>Detalle Orden de Compra:</b> $detalleOrden</td>

		</tr>

		<tr>

		<td style="border-bottom: 1px solid #666; background-color:white; width:540px"></td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($bloque2, false, false, false, false, '');


$bloque3 = <<<EOF

	<table style="font-size:10px; padding:5px 10px;">

		<tr>
    <td style="border: 1px solid #666; background-color:white; width:45px; text-align:center"><b>Item</b></td>
		<td style="border: 1px solid #666; background-color:white; width:260px; text-align:center"><b>Producto</b></td>
		<td style="border: 1px solid #666; background-color:white; width:65px; text-align:center"><b>Cantidad</b></td>
		<td style="border: 1px solid #666; background-color:white; width:85px; text-align:center"><b>Valor Unit.</b></td>
		<td style="border: 1px solid #666; background-color:white; width:85px; text-align:center"><b>Valor Total</b></td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($bloque3, false, false, false, false, '');


$bloque4 = <<<EOF

	<table style="font-size:10px; padding:5px 10px;">

			$productos

	</table>


EOF;

$pdf->writeHTML($bloque4, false, false, false, false, '');


$bloque5 = <<<EOF

	<table style="font-size:10px; padding:5px 10px;">

		<tr>

			<td style="border-bottom: 1px solid #666; color:#333; background-color:white; width:540px; text-align:center"></td>

		</tr>

		<tr>

			<td style="border: 1px solid #666; color:#333; background-color:white; width:455px; text-align:center">
				<b>Valor Total de Orden de Compra</b>
			</td>

			<td style="border: 1px solid #666; color:#333; background-color:white; width:85px; text-align:center">
				$ $totalOrden
			</td>

		</tr>


	</table>

EOF;

$pdf->writeHTML($bloque5, false, false, false, false, '');


$bloque6 = <<<EOF

	<table style="font-size:10px; padding:5px 10px;">

		<tr>

			<td style="border-bottom: 1px solid #666; color:#333; background-color:white; width:540px; text-align:center"></td>

		</tr>

		<tr>

			<td style="border: 1px solid #666; color:#333; background-color:white; width:200px; text-align:center">
				<b>Departamento de Compras</b><br><br><br><br><br>
				_____________________________
				<b>Firma</b>
			</td>

			<td style="border: 1px solid #666; color:#333; background-color:white; width:340px; text-align:center">
				<b>Observaciones</b>
			</td>

		</tr>


	</table>

EOF;

$pdf->writeHTML($bloque6, false, false, false, false, '');

$pdf->Output('OrdenCompra.pdf');


}
}

$item;
if (isset($_GET["idOrden"])) {
	// code...
	$item=$_GET["idOrden"];
} else {

	$IdOrden = null;
}
$Orden = new OrdenCompra();
$Orden -> get_Orden($item);
