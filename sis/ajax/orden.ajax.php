<?php

require_once "../controladores/orden.controlador.php";
require_once "../modelos/orden.modelo.php";

class AjaxOrden
{
    /*Consultar Orden Compra*/

    public $id_orden;

    public function ajaxConsultarOrden()
    {
        $valor = $this->id_orden;
        $response = ControladorOrden::ctrMostrarOrdenDetalle($valor);
    

        echo json_encode($response);
        
    }

    public function ajaxConsultarOrdenSola()
    {
        $valor = $this->id_orden;
        $item = "id_orden_compra";
        $response = ControladorOrden::ctrMostrarOrden($item,$valor);
        echo json_encode($response);

    }

    public function ajaxConsultarOrdenProducto()
    {
        $valor = $this->id_orden;
        $item = "id_orden_producto";
        $response = ControladorOrden::ctrMostrarOrdenProducto($item,$valor);
        echo json_encode($response);
    }

}


if(isset($_POST["id_orden"]))
{
    $obj = new AjaxOrden();
    $obj->id_orden = $_POST["id_orden"];
    $obj->ajaxConsultarOrden();
}

if(isset($_POST["id_edit_orden"]))
{
    $obj = new AjaxOrden();
    $obj->id_orden = $_POST["id_edit_orden"];
    $obj->ajaxConsultarOrdenSola();
}

if(isset($_POST["id_editar"]))
{
    $obj = new AjaxOrden();
    $obj->id_orden = $_POST["id_editar"];
    $obj->ajaxConsultarOrdenProducto();
}