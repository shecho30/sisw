<?php

require_once "../controladores/forma.pago.controlador.php";
require_once "../modelos/forma.pago.modelo.php";

class AjaxFormaPago{

	/*=============================================
	EDITAR CATEGORÍA
	=============================================*/	

	public $idFormaPago;

	public function ajaxEditarFormaPago()
	{

		$item = "id_forma_pago";
		$valor = $this->idFormaPago;

		$respuesta = ControladorFormaPago::ctrMostrarFormaPago(
			$item, $valor);

		echo json_encode($respuesta);

	}
}

/*=============================================
EDITAR CATEGORÍA
=============================================*/	
if(isset($_POST["id_forma_pago"])){

	$formapago = new AjaxFormaPago();
	$formapago -> idFormaPago = $_POST["id_forma_pago"];
	$formapago -> ajaxEditarFormaPago();
}