<?php

require_once "../controladores/clientes.controlador.php";
require_once "../modelos/clientes.modelo.php";

class AjaxClientes{

	/*=============================================
	EDITAR CLIENTE
	=============================================*/	

	public $idliente;
	public function ajaxEditarCliente(){

		
		$item  = "id_cliente";

		$valor = $this->idCliente;

		$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);

		echo json_encode($respuesta);

	}
	/*=============================================
	ACTIVAR CLIENTE
	=============================================*/	
	public $activarId;
	public $activarCliente;

	public function ajaxActivarCliente(){

		$tabla  = "tbl_cliente";

		$item1  = "id_estado_usuario";

		$valor1 =  $this->activarCliente;

		$item2  = "id_cliente";

		$valor2 = $this->activarId;

		$respuesta = ModeloClientes::mdlActualizarCliente($tabla, $item1, $valor1, $item2, $valor2);
		echo json_encode($respuesta);


	}


	/*=============================================
	VALIDAR NO REPETIR CORREO
	=============================================*/	

	public $validarCorreoU;

	public function ajaxValidarCorreoU(){

		$item = "correo_cliente";
		$valor = $this->validarCorreoU;

		$respuestaCu = ControladorClientes::ctrMostrarClientes($item, $valor);

		echo json_encode($respuestaCu);

	}


	/*=============================================
	VALIDAR NO REPETIR DOCUMENTO
	=============================================*/	

	public $validarDocumento;

	public function ajaxvalidarDocumento(){

		$item = "document_cliente";
		$valor = $this->validarDocumento;

		$respuestad = ControladorClientes::ctrMostrarClientes($item, $valor);

		echo json_encode($respuestad);

	}


}

/*=============================================
EDITAR CLIENTE
=============================================*/	

if(isset($_POST["idCliente"])){

	$editCliente = new AjaxClientes();
	$editCliente -> idCliente   = $_POST["idCliente"];
	$editCliente -> ajaxEditarCliente();

}
if (isset($_POST["activarCliente"])) {
	
	$activarCliente = new AjaxClientes();
	$activarCliente -> activarCliente = $_POST["activarCliente"];
	$activarCliente -> activarId      = $_POST["activarId"];
	$activarCliente -> ajaxActivarCliente();

}

/*=============================================
VALIDAR NO REPETIR CORREO
=============================================*/

if(isset( $_POST["validarCorreoU"])){

	$valUsuario = new AjaxClientes();
	$valUsuario -> validarCorreoU = $_POST["validarCorreoU"];
	$valUsuario -> ajaxValidarCorreoU();

}


/*=============================================
VALIDAR NO REPETIR DOCUMENTO
=============================================*/

if(isset( $_POST["validarDocumento"])){

	$valUsuario = new AjaxClientes();
	$valUsuario -> validarDocumento = $_POST["validarDocumento"];
	$valUsuario -> ajaxvalidarDocumento();

}