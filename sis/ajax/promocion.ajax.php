<?php

require_once "../controladores/promocion.controlador.php";
require_once "../modelos/promocion.modelo.php";

class AjaxPromocion
{

	/*=============================================
	EDITAR USUARIO
	=============================================*/	

	public $idPromocion;

	public function ajaxEditarPromocion(){

		$item = "id_promocion";
		$valor = $this->idPromocion;

		$respuesta = ControladorPromocion::ctrMostrarPromocion($item, $valor);

		echo json_encode($respuesta);

	}
	
	/*=============================================
	VALIDAR NO REPETIR PROMOCION
	=============================================*/	

	public $validarPromocion;

	public function ajaxValidarPromocion()
	{

		$item = "posicion_promocion";
		$valor = $this->validarPromocion;

		$respuesta = ControladorPromocion::ctrMostrarPromocion($item, $valor);

		echo json_encode($respuesta);

	}
}



/*=============================================
EDITAR PROMOCION
=============================================*/
if(isset($_POST["idPromocion"]))
{
	$editar = new AjaxPromocion();
	$editar -> idPromocion = $_POST["idPromocion"];
	$editar -> ajaxEditarPromocion();
}

/*=============================================
VALIDAR PROMOCION
=============================================*/
if(isset($_POST["validarPromo"]))
{
	$editar = new AjaxPromocion();
	$editar -> validarPromocion = $_POST["validarPromo"];
	$editar -> ajaxValidarPromocion();
}