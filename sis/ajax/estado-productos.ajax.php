<?php

require_once "../controladores/estadoProductos.controlador.php";
require_once "../modelos/estadoProductos.modelo.php";

//require_once "../controladores/categorias.controlador.php";
//require_once "../modelos/categorias.modelo.php";

class AjaxEstadosProductos{

  /*=============================================
  EDITAR PRODUCTO
  =============================================*/ 

  public $idEstadoProducto;

  public function ajaxEditarEstadoProducto(){

    $item = "id_estado_producto";
    $valor = $this->idEstadoProducto;

    $respuesta = ControladorEstadoProducto::ctrMostrarEstadoProducto($item, $valor);

    echo json_encode($respuesta);
  }

  public function ajaxEliminarEstadoProducto(){
    $item = "id_estado_producto";
    $valor = $this->idEstadoProducto;

    $respuesta = ControladorEstadoProducto::ctrEliminarEstadoProducto($item, $valor);

    echo json_encode($respuesta);
  }

}


/*=============================================
GENERAR CÓDIGO A PARTIR DE ID CATEGORIA
=============================================*/ 

/*if(isset($_POST["idCategoria"])){

  $codigoProducto = new AjaxProductos();
  $codigoProducto -> idCategoria = $_POST["idCategoria"];
  $codigoProducto -> ajaxCrearCodigoProducto();

}*/
/*=============================================
EDITAR PRODUCTO
=============================================*/ 

if(isset($_POST["idEstadoProducto"])){
  $editarProducto = new AjaxEstadosProductos();
  $editarProducto -> idEstadoProducto = $_POST["idEstadoProducto"];
  $editarProducto -> ajaxEditarEstadoProducto();
}

if(isset($_POST["idEstadoProductoEliminar"])){
  $editarProducto = new AjaxEstadosProductos();
  $editarProducto -> idEstadoProducto = $_POST["idEstadoProductoEliminar"];
  $editarProducto -> ajaxEliminarEstadoProducto();
}





