<?php

require_once "../controladores/productos.controlador.php";
require_once "../modelos/productos.modelo.php";

class TablaProductos{

 	/*=============================================
 	 MOSTRAR LA TABLA DE PRODUCTOS
  	=============================================*/ 

	public function mostrarTablaProductos(){

		$item = null;
    	$valor = null;

  		$productos = ControladorProductos::ctrMostrarProductos($item, $valor);	
		
  		$datosJson = '{
		  "data": [';

		  for($i = 0; $i < count($productos); $i++){

		  	/*=============================================
 	 		TRAEMOS LA IMAGEN
  			=============================================*/ 

		  	$imagen = "";
        if(!empty($productos[$i]->imagen_producto) || !is_null($productos[$i]->imagen_producto)){
            $imagen = '<td><img src="'.$productos[$i]->imagen_producto.'" class="img-thumbnail" width="40px"></td>';
        }else{
          $imagen = '<td><img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="40px"></td>';
        }

		
		  	/*=============================================
 	 		STOCK
  			=============================================*/ 

  			if($productos[$i]->cantidad_producto <= 10){

  				$stock = "<button class='btn btn-danger'>".$productos[$i]->cantidad_producto."</button>";

  			}else if($productos[$i]->cantidad_producto > 11 && $productos[$i]->cantidad_producto <= 15){

  				$stock = "<button class='btn btn-warning'>".$productos[$i]->cantidad_producto."</button>";

  			}else{

  				$stock = "<button class='btn btn-success'>".$productos[$i]->cantidad_producto."</button>";

  			}

		  	/*=============================================
 	 		TRAEMOS LAS ACCIONES
  			=============================================*/ 


            if ($_SESSION["perfil"] == 1 ) {
                                    
			  $botones =   "<div class='btn-group'>
			  <button class='btn btn-warning btnEditarProducto' idProducto='".$productos[$i]->id_producto."' data-toggle='modal' 
				  data-target='#modalEditarProducto'>
				  <i class='fa fa-pencil'></i>";
			}else{
				$botones =  
						"<div class='btn-group'>
					<button class='btn btn-warning btnEditarProducto' idProducto='".$productos[$i]->id_producto."' data-toggle='modal' 
						data-target='#modalEditarProducto'>
						<i class='fa fa-pencil'></i>
					</button>
					<button class='btn btn-danger btnEliminarProducto' idProducto='".$productos[$i]->id_producto."' 
																		codigo='".$productos[$i]->codigo_barra_producto."' 
																		imagen='".$productos[$i]->imagen_producto."'>
						<i class='fa fa-times'></i>
					</button></div>";
			}
		  	$datosJson .='[
			      "'.$productos[$i]->id_producto.'",
            "'.$productos[$i]->nombre_producto.'",
            "'.$productos[$i]->codigo_barra_producto.'",
            "'.$stock.'",
            "'.$productos[$i]->precio_compra_producto.'",
            "'.$productos[$i]->precio_salida_producto.'",
            "'.$productos[$i]->unidad_medicion_producto.'",
            "'.$productos[$i]->presentacion_producto.'",
            "'.$productos[$i]->fecha_creacion_producto.'",
            "'.$imagen.'",
            "'.$productos[$i]->description_producto.'",
            "'.$productos[$i]->nombre_categoria.'",
            "'.$productos[$i]->nombre_estado_producto.'",
            "'.$botones.'"               
			    ],';

		  
		}
		  $datosJson = substr($datosJson, 0, -1);

		 $datosJson .=   '] 

		 }';
		
		echo $datosJson;

	}



}

/*=============================================
ACTIVAR TABLA DE PRODUCTOS
=============================================*/ 
$activarProductos = new TablaProductos();
$activarProductos -> mostrarTablaProductos();

