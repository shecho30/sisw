<?php

require_once "../controladores/proveedores.controlador.php";
require_once "../modelos/proveedores.modelo.php";

class AjaxProveedor
{
    /*=============================================
	EDITAR USUARIO
	=============================================*/	

	public $idProveedor;

	public function ajaxEditarProveedor(){

		$item = "id_proveedor";
		$valor = $this->idProveedor;

		$respuesta = ControladorProveedor::ctrMostrarProveedores($item, $valor);

		echo json_encode($respuesta);

	}

}


/*=============================================
EDITAR USUARIO
=============================================*/
if(isset($_POST["id_proveedor"])){

	$editar = new AjaxProveedor();
	$editar -> idProveedor = $_POST["id_proveedor"];
	$editar -> ajaxEditarProveedor();

}
?>
