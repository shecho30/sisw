<?php

require_once "../controladores/pedido.controlador.php";
require_once "../modelos/pedidos.modelo.php";

class AjaxPedidos{

	/*=============================================
	EDITAR CLIENTE
	=============================================*/	

	public $idPedido;
	
	public function ajaxEditarPedidos(){

		
		$item  = "id_pedido";

		$valor = $this->idPedido;

		$respuesta = ControladorPedidos::ctrMostrarPedidos($item, $valor);

		echo json_encode($respuesta);

	}
}
if(isset($_POST["idPedido"])){

	$editPedido = new AjaxPedidos();
	$editPedido ->idPedido = $_POST["idPedido"];
	$editPedido ->ajaxEditarPedidos();
	//echo json_encode($editPedido->idPedido);

}

?>