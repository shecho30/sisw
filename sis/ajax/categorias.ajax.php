<?php

require_once "../controladores/categorias.controlador.php";
require_once "../modelos/categorias.modelo.php";


class AjaxCategorias{

	/*=============================================
	EDITAR CATEGORÍA
	=============================================*/	

	public $idCategoria;

	public function ajaxEditarCategoria(){

		$item = "id_categoria";
		$valor = $this->idCategoria;

		$respuesta = ControladorCategorias::ctrMostrarCategorias($item, $valor);

		echo json_encode($respuesta);

	}
	public function ajaxConsultar()
	{
		$valor = $this->idCategoria;

		$respuesta = ModeloCategorias::Consultar($valor);

		echo json_encode($respuesta);
	}
}

/*=============================================
EDITAR CATEGORÍA
=============================================*/	
if(isset($_POST["idCategoria"])){

	$categoria = new AjaxCategorias();
	$categoria -> idCategoria = $_POST["idCategoria"];
	$categoria -> ajaxEditarCategoria();
}

if(isset($_POST["id"])){

	$categoria = new AjaxCategorias();
	$categoria -> idCategoria = $_POST["id"];
	$categoria -> ajaxConsultar();
}

