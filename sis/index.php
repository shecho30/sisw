<?php

require_once "controladores/plantilla.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/categorias.controlador.php";
require_once "controladores/productos.controlador.php";
require_once "controladores/estadoProductos.controlador.php";
require_once "controladores/clientes.controlador.php";
require_once "controladores/ventas.controlador.php";
require_once "controladores/proveedores.controlador.php";
require_once "controladores/orden.controlador.php";
require_once "controladores/pedido.controlador.php";
require_once "controladores/forma.pago.controlador.php";
require_once "controladores/promocion.controlador.php";

require_once "modelos/usuarios.modelo.php";
require_once "modelos/categorias.modelo.php";
require_once "modelos/productos.modelo.php";
require_once "modelos/estadoProductos.modelo.php";
require_once "modelos/clientes.modelo.php";
require_once "modelos/ventas.modelo.php";
require_once "modelos/proveedores.modelo.php";
require_once "modelos/orden.modelo.php";
require_once "modelos/pedidos.modelo.php";
require_once "modelos/forma.pago.modelo.php";
require_once "modelos/promocion.modelo.php";


require_once "config/service_productos.php";

$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();