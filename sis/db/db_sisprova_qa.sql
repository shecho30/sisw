-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-10-2018 a las 18:39:38
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_sisprova_qa`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_producto` ()  BEGIN
	SELECT id_producto,nombre_producto,codigo_barra_producto,cantidad_producto,precio_compra_producto,precio_salida_producto,unidad_medicion_producto,presentacion_producto,fecha_creacion_producto,
		imagen_producto,description_producto,id_usuario,id_categoria,id_estado_producto FROM db_sisprova_qa.tbl_producto ORDER BY nombre_producto;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_usuario` ()  BEGIN
    	
		SELECT id_usuario,nombre_usuario,apellido_usuario,nick_user,correo_usuario,contrasena_usuario,	
				fecha_creacion_usuario,	imagen_usuario,	id_estado_usuario, id_tipo_usuario
                FROM tbl_usuario WHERE id_estado_usuario = 1 ORDER BY id_usuario;
						
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_usuario` (IN `_nombre_usuario` VARCHAR(50), IN `_apellido_usuario` VARCHAR(50), IN `_nick_user` VARCHAR(50), IN `_correo_usuario` VARCHAR(255), IN `_contrasena_usuario` VARCHAR(60), IN `_id_tipo_usuario` INT)  BEGIN
    
		DECLARE _id_max INT;
        SET	 	_id_max = (SELECT IFNULL(MAX(id_usuario),0)+1 FROM tbl_usuario);
	
		INSERT INTO tbl_usuario(id_usuario,		nombre_usuario,		apellido_usuario,
								nick_user,		correo_usuario,		contrasena_usuario,	
                                fecha_creacion_usuario,	imagen_usuario,	id_estado_usuario,	id_tipo_usuario)
						VALUES(_id_max,		_nombre_usuario,		_apellido_usuario,
								_nick_user,		_correo_usuario,	_contrasena_usuario,
                                NOW(),'null',1,_id_tipo_usuario);
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_logear_usuario` (IN `_correo_usuario` VARCHAR(255), IN `_contrasena_usuario` VARCHAR(200))  BEGIN
		SELECT id_usuario,		nombre_usuario,		apellido_usuario,    nick_user,		
			   correo_usuario,	contrasena_usuario,	fecha_creacion_usuario,	
               imagen_usuario,	id_estado_usuario,	id_tipo_usuario
        FROM tbl_usuario WHERE correo_usuario = _correo_usuario
        AND contrasena_usuario = _contrasena_usuario LIMIT 1;
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_prueba` (OUT `_num2` VARCHAR(100))  BEGIN
		SET _num2 = 'this';
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_categoria`
--

CREATE TABLE `tbl_categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(50) NOT NULL,
  `fecha_creacion_categoria` datetime NOT NULL,
  `imagen_categoria` varchar(255) DEFAULT NULL,
  `descripcion_categoria` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ciudad`
--

CREATE TABLE `tbl_ciudad` (
  `id_ciudad` int(11) NOT NULL,
  `nombre_ciudad` varchar(50) NOT NULL,
  `pais_ciudad` varchar(50) NOT NULL,
  `codigo_ciudad` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cliente`
--

CREATE TABLE `tbl_cliente` (
  `id_cliente` int(11) NOT NULL,
  `document_cliente` varchar(50) NOT NULL,
  `id_tipo_documento` int(11) NOT NULL,
  `empresa_cliente` varchar(50) DEFAULT NULL,
  `nombre_repre_cliente` varchar(50) NOT NULL,
  `apellido_repre_cliente` varchar(50) NOT NULL,
  `telefono_cliente` varchar(50) NOT NULL,
  `direccion_cliente` varchar(50) NOT NULL,
  `correo_cliente` varchar(50) NOT NULL,
  `contrasena_cliente` varchar(60) NOT NULL,
  `id_estado_usuario` int(11) NOT NULL,
  `fecha_creacion_cliente` datetime NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_detalle_factura`
--

CREATE TABLE `tbl_detalle_factura` (
  `id_detalle_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_factura` int(11) NOT NULL,
  `cantidad_detalle_factura` float NOT NULL,
  `total_detalle_factura` varchar(50) NOT NULL,
  `fecha_creacion_detalle_factura` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_detalle_oc`
--

CREATE TABLE `tbl_detalle_oc` (
  `id_detalle_oc` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_orden_compra` int(11) NOT NULL,
  `cantidad_detalle_oc` float NOT NULL,
  `total_detalle_oc` varchar(50) NOT NULL,
  `fecha_creacion_detalle_oc` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_devolucion`
--

CREATE TABLE `tbl_devolucion` (
  `id_dovolucion` int(11) NOT NULL,
  `id_detalle_factura` int(11) NOT NULL,
  `motivo_devolucion` varchar(250) NOT NULL,
  `fecha_devolucion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estado_producto`
--

CREATE TABLE `tbl_estado_producto` (
  `id_estado_producto` int(11) NOT NULL,
  `nombre_estado_producto` varchar(50) NOT NULL,
  `fecha_creacion_usuario` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estado_usuario`
--

CREATE TABLE `tbl_estado_usuario` (
  `id_estado_usuario` int(11) NOT NULL,
  `nombre_estado_usuario` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_estado_usuario`
--

INSERT INTO `tbl_estado_usuario` (`id_estado_usuario`, `nombre_estado_usuario`) VALUES
(1, 'Inactivo'),
(2, 'Activo'),
(3, 'Bloqueado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_factura`
--

CREATE TABLE `tbl_factura` (
  `id_factura` int(11) NOT NULL,
  `id_dian_factura` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_creacion_factura` datetime NOT NULL,
  `id_forma_pago` int(11) NOT NULL,
  `total_factura` float NOT NULL,
  `iva_factura` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_forma_pago`
--

CREATE TABLE `tbl_forma_pago` (
  `id_forma_pago` int(11) NOT NULL,
  `nombre_forma_pago` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_orden_compra`
--

CREATE TABLE `tbl_orden_compra` (
  `id_orden_compra` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha_creacion_orden_compra` datetime NOT NULL,
  `id_forma_pago` int(11) NOT NULL,
  `total_orden_compra` float NOT NULL,
  `iva_orden_compra` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_pedido`
--

CREATE TABLE `tbl_pedido` (
  `id_pedido` int(11) NOT NULL,
  `numero_pedido` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creacion_pedido` datetime NOT NULL,
  `cantidad_pedido` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_producto`
--

CREATE TABLE `tbl_producto` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(50) NOT NULL,
  `codigo_barra_producto` varchar(50) NOT NULL,
  `cantidad_producto` int(11) NOT NULL,
  `precio_compra_producto` float NOT NULL,
  `precio_salida_producto` float NOT NULL,
  `unidad_medicion_producto` varchar(255) NOT NULL,
  `presentacion_producto` varchar(50) NOT NULL,
  `fecha_creacion_producto` datetime NOT NULL,
  `imagen_producto` varchar(255) DEFAULT NULL,
  `description_producto` varchar(255) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_estado_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_proveedor`
--

CREATE TABLE `tbl_proveedor` (
  `id_proveedor` int(11) NOT NULL,
  `documento_proveedor` varchar(50) NOT NULL,
  `id_tipo_documento` int(11) NOT NULL,
  `empresa_proveedor` varchar(50) NOT NULL,
  `nombre_repre_proveedor` varchar(50) NOT NULL,
  `apellido_repre_proveedor` varchar(50) NOT NULL,
  `telefono_proveedor` varchar(50) NOT NULL,
  `direccion_proveedor` varchar(50) NOT NULL,
  `correo_proveedor` varchar(50) NOT NULL,
  `imagen_proveedor` varchar(255) DEFAULT NULL,
  `fecha_creacion_proveedor` datetime NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipo_documento`
--

CREATE TABLE `tbl_tipo_documento` (
  `id_tipo_documento` int(11) NOT NULL,
  `nombre_tipo_documento` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipo_usuario`
--

CREATE TABLE `tbl_tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL,
  `nombre_tipo_usuario` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_tipo_usuario`
--

INSERT INTO `tbl_tipo_usuario` (`id_tipo_usuario`, `nombre_tipo_usuario`) VALUES
(1, 'Admin'),
(2, 'Analista'),
(3, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuario`
--

CREATE TABLE `tbl_usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(50) NOT NULL,
  `apellido_usuario` varchar(50) NOT NULL,
  `nick_user` varchar(50) NOT NULL,
  `correo_usuario` varchar(255) NOT NULL,
  `contrasena_usuario` varchar(255) NOT NULL,
  `fecha_creacion_usuario` datetime NOT NULL,
  `imagen_usuario` varchar(255) DEFAULT NULL,
  `id_estado_usuario` int(11) NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `nombre_usuario`, `apellido_usuario`, `nick_user`, `correo_usuario`, `contrasena_usuario`, `fecha_creacion_usuario`, `imagen_usuario`, `id_estado_usuario`, `id_tipo_usuario`) VALUES
(1, 'administrador', 'trador', 'admin', 'admin@gmail.com', 'admin123', '2018-08-17 14:30:06', 'null', 1, 1),
(2, 'ASD', 'ASD', 'ASD', 'ASD', 'asd', '2018-08-17 14:31:07', 'null', 1, 3),
(3, 'sergio', 'buitrago', 'shecho30', 'sergiobuitrago11.4@gmail.com', '123456', '2018-08-17 14:31:57', 'null', 1, 1),
(4, 'sergio', 'buitrago', 'sergiobuitrago11.4@gmail.com', 'sergiobuitrago11.4@gmail.com', 'asd', '2018-08-17 14:32:45', 'null', 1, 3),
(5, 'sergio', 'buitrago', 'sergiobuitrago11.4@gmail.com', 'asdad', 'ads', '2018-08-17 14:33:42', 'null', 1, 3),
(6, 'luis', 'mazuera', 'lageycha', 'luis@hotmail.com', 'asd', '2018-08-17 18:49:19', 'null', 1, 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_categoria`
--
ALTER TABLE `tbl_categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `tbl_ciudad`
--
ALTER TABLE `tbl_ciudad`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD KEY `tbl_cliente_ibfk_1` (`id_tipo_documento`),
  ADD KEY `tbl_cliente_ibfk_2` (`id_ciudad`);

--
-- Indices de la tabla `tbl_detalle_factura`
--
ALTER TABLE `tbl_detalle_factura`
  ADD PRIMARY KEY (`id_detalle_factura`),
  ADD KEY `tbl_detalle_factura_ibfk_1` (`id_producto`),
  ADD KEY `tbl_detalle_factura_ibfk_2` (`id_factura`);

--
-- Indices de la tabla `tbl_detalle_oc`
--
ALTER TABLE `tbl_detalle_oc`
  ADD PRIMARY KEY (`id_detalle_oc`),
  ADD KEY `tbl_detalle_oc_ibfk_1` (`id_producto`),
  ADD KEY `tbl_detalle_oc_ibfk_2` (`id_orden_compra`);

--
-- Indices de la tabla `tbl_devolucion`
--
ALTER TABLE `tbl_devolucion`
  ADD PRIMARY KEY (`id_dovolucion`),
  ADD KEY `tbl_devolucion_ibfk_1` (`id_detalle_factura`);

--
-- Indices de la tabla `tbl_estado_producto`
--
ALTER TABLE `tbl_estado_producto`
  ADD PRIMARY KEY (`id_estado_producto`);

--
-- Indices de la tabla `tbl_estado_usuario`
--
ALTER TABLE `tbl_estado_usuario`
  ADD PRIMARY KEY (`id_estado_usuario`);

--
-- Indices de la tabla `tbl_factura`
--
ALTER TABLE `tbl_factura`
  ADD PRIMARY KEY (`id_factura`),
  ADD KEY `tbl_factura_ibfk_1` (`id_forma_pago`),
  ADD KEY `tbl_factura_ibfk_2` (`id_usuario`),
  ADD KEY `tbl_factura_ibfk_3` (`id_cliente`);

--
-- Indices de la tabla `tbl_forma_pago`
--
ALTER TABLE `tbl_forma_pago`
  ADD PRIMARY KEY (`id_forma_pago`);

--
-- Indices de la tabla `tbl_orden_compra`
--
ALTER TABLE `tbl_orden_compra`
  ADD PRIMARY KEY (`id_orden_compra`),
  ADD KEY `tbl_orden_compra_ibfk_1` (`id_forma_pago`),
  ADD KEY `tbl_orden_compra_ibfk_2` (`id_usuario`),
  ADD KEY `tbl_orden_compra_ibfk_3` (`id_proveedor`);

--
-- Indices de la tabla `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `tbl_pedido_ibfk_1` (`id_cliente`),
  ADD KEY `tbl_pedido_ibfk_2` (`id_producto`);

--
-- Indices de la tabla `tbl_producto`
--
ALTER TABLE `tbl_producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `tbl_producto_ibfk_1` (`id_categoria`),
  ADD KEY `tbl_producto_ibfk_2` (`id_estado_producto`);

--
-- Indices de la tabla `tbl_proveedor`
--
ALTER TABLE `tbl_proveedor`
  ADD PRIMARY KEY (`id_proveedor`),
  ADD KEY `tbl_proveedor_ibfk_1` (`id_producto`);

--
-- Indices de la tabla `tbl_tipo_documento`
--
ALTER TABLE `tbl_tipo_documento`
  ADD PRIMARY KEY (`id_tipo_documento`);

--
-- Indices de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indices de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `tbl_usuario_ibfk_1` (`id_estado_usuario`),
  ADD KEY `tbl_usuario_ibfk_2` (`id_tipo_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_categoria`
--
ALTER TABLE `tbl_categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_ciudad`
--
ALTER TABLE `tbl_ciudad`
  MODIFY `id_ciudad` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_detalle_factura`
--
ALTER TABLE `tbl_detalle_factura`
  MODIFY `id_detalle_factura` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_detalle_oc`
--
ALTER TABLE `tbl_detalle_oc`
  MODIFY `id_detalle_oc` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_devolucion`
--
ALTER TABLE `tbl_devolucion`
  MODIFY `id_dovolucion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_estado_producto`
--
ALTER TABLE `tbl_estado_producto`
  MODIFY `id_estado_producto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_estado_usuario`
--
ALTER TABLE `tbl_estado_usuario`
  MODIFY `id_estado_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_factura`
--
ALTER TABLE `tbl_factura`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_forma_pago`
--
ALTER TABLE `tbl_forma_pago`
  MODIFY `id_forma_pago` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_orden_compra`
--
ALTER TABLE `tbl_orden_compra`
  MODIFY `id_orden_compra` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_producto`
--
ALTER TABLE `tbl_producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_proveedor`
--
ALTER TABLE `tbl_proveedor`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_tipo_documento`
--
ALTER TABLE `tbl_tipo_documento`
  MODIFY `id_tipo_documento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  MODIFY `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  ADD CONSTRAINT `tbl_cliente_ibfk_1` FOREIGN KEY (`id_tipo_documento`) REFERENCES `tbl_tipo_documento` (`id_tipo_documento`),
  ADD CONSTRAINT `tbl_cliente_ibfk_2` FOREIGN KEY (`id_ciudad`) REFERENCES `tbl_ciudad` (`id_ciudad`);

--
-- Filtros para la tabla `tbl_detalle_factura`
--
ALTER TABLE `tbl_detalle_factura`
  ADD CONSTRAINT `tbl_detalle_factura_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`),
  ADD CONSTRAINT `tbl_detalle_factura_ibfk_2` FOREIGN KEY (`id_factura`) REFERENCES `tbl_factura` (`id_factura`);

--
-- Filtros para la tabla `tbl_detalle_oc`
--
ALTER TABLE `tbl_detalle_oc`
  ADD CONSTRAINT `tbl_detalle_oc_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`),
  ADD CONSTRAINT `tbl_detalle_oc_ibfk_2` FOREIGN KEY (`id_orden_compra`) REFERENCES `tbl_orden_compra` (`id_orden_compra`);

--
-- Filtros para la tabla `tbl_devolucion`
--
ALTER TABLE `tbl_devolucion`
  ADD CONSTRAINT `tbl_devolucion_ibfk_1` FOREIGN KEY (`id_detalle_factura`) REFERENCES `tbl_detalle_factura` (`id_detalle_factura`);

--
-- Filtros para la tabla `tbl_factura`
--
ALTER TABLE `tbl_factura`
  ADD CONSTRAINT `tbl_factura_ibfk_1` FOREIGN KEY (`id_forma_pago`) REFERENCES `tbl_forma_pago` (`id_forma_pago`),
  ADD CONSTRAINT `tbl_factura_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`),
  ADD CONSTRAINT `tbl_factura_ibfk_3` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_cliente` (`id_cliente`);

--
-- Filtros para la tabla `tbl_orden_compra`
--
ALTER TABLE `tbl_orden_compra`
  ADD CONSTRAINT `tbl_orden_compra_ibfk_1` FOREIGN KEY (`id_forma_pago`) REFERENCES `tbl_forma_pago` (`id_forma_pago`),
  ADD CONSTRAINT `tbl_orden_compra_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`),
  ADD CONSTRAINT `tbl_orden_compra_ibfk_3` FOREIGN KEY (`id_proveedor`) REFERENCES `tbl_proveedor` (`id_proveedor`);

--
-- Filtros para la tabla `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  ADD CONSTRAINT `tbl_pedido_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_cliente` (`id_cliente`),
  ADD CONSTRAINT `tbl_pedido_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`);

--
-- Filtros para la tabla `tbl_producto`
--
ALTER TABLE `tbl_producto`
  ADD CONSTRAINT `tbl_producto_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `tbl_categoria` (`id_categoria`),
  ADD CONSTRAINT `tbl_producto_ibfk_2` FOREIGN KEY (`id_estado_producto`) REFERENCES `tbl_estado_producto` (`id_estado_producto`);

--
-- Filtros para la tabla `tbl_proveedor`
--
ALTER TABLE `tbl_proveedor`
  ADD CONSTRAINT `tbl_proveedor_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`);

--
-- Filtros para la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD CONSTRAINT `tbl_usuario_ibfk_1` FOREIGN KEY (`id_estado_usuario`) REFERENCES `tbl_estado_usuario` (`id_estado_usuario`),
  ADD CONSTRAINT `tbl_usuario_ibfk_2` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tbl_tipo_usuario` (`id_tipo_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
