-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2018 a las 18:04:50
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_sisprova_qa`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_producto` ()  BEGIN
	SELECT id_producto,nombre_producto,codigo_barra_producto,cantidad_producto,precio_compra_producto,precio_salida_producto,unidad_medicion_producto,presentacion_producto,fecha_creacion_producto,
		imagen_producto,description_producto,id_usuario,id_categoria,id_estado_producto FROM db_sisprova_qa.tbl_producto ORDER BY nombre_producto;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_usuario` ()  BEGIN
    	
		SELECT id_usuario,nombre_usuario,apellido_usuario,nick_user,correo_usuario,contrasena_usuario,	
				fecha_creacion_usuario,	imagen_usuario,	id_estado_usuario, id_tipo_usuario
                FROM tbl_usuario WHERE id_estado_usuario = 1 ORDER BY id_usuario;
						
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_usuario` (IN `_nombre_usuario` VARCHAR(50), IN `_apellido_usuario` VARCHAR(50), IN `_nick_user` VARCHAR(50), IN `_correo_usuario` VARCHAR(255), IN `_contrasena_usuario` VARCHAR(60), IN `_id_tipo_usuario` INT)  BEGIN
    
		DECLARE _id_max INT;
        SET	 	_id_max = (SELECT IFNULL(MAX(id_usuario),0)+1 FROM tbl_usuario);
	
		INSERT INTO tbl_usuario(id_usuario,		nombre_usuario,		apellido_usuario,
								nick_user,		correo_usuario,		contrasena_usuario,	
                                fecha_creacion_usuario,	imagen_usuario,	id_estado_usuario,	id_tipo_usuario)
						VALUES(_id_max,		_nombre_usuario,		_apellido_usuario,
								_nick_user,		_correo_usuario,	_contrasena_usuario,
                                NOW(),'null',1,_id_tipo_usuario);
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_logear_usuario` (IN `_correo_usuario` VARCHAR(255), IN `_contrasena_usuario` VARCHAR(200))  BEGIN
		SELECT id_usuario,		nombre_usuario,		apellido_usuario,    nick_user,		
			   correo_usuario,	contrasena_usuario,	fecha_creacion_usuario,	
               imagen_usuario,	id_estado_usuario,	id_tipo_usuario
        FROM tbl_usuario WHERE correo_usuario = _correo_usuario
        AND contrasena_usuario = _contrasena_usuario LIMIT 1;
	END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_prueba` (OUT `_num2` VARCHAR(100))  BEGIN
		SET _num2 = 'this';
    END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_categoria`
--

CREATE TABLE `tbl_categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(50) NOT NULL,
  `fecha_creacion_categoria` datetime NOT NULL,
  `imagen_categoria` varchar(255) DEFAULT NULL,
  `descripcion_categoria` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_categoria`
--

INSERT INTO `tbl_categoria` (`id_categoria`, `nombre_categoria`, `fecha_creacion_categoria`, `imagen_categoria`, `descripcion_categoria`) VALUES
(1, 'Computadores', '2018-10-26 00:00:00', NULL, 'Tecnologia'),
(2, 'Portatiles', '2018-10-26 11:01:28', NULL, 'Tecnologia'),
(3, 'Andamios', '2018-11-04 00:00:00', NULL, 'van andamios'),
(4, 'Televisores', '2018-10-26 16:33:15', NULL, 'Tecnologia'),
(5, 'energia', '2018-11-04 00:00:00', NULL, 'va energia'),
(6, 'materiales de construccion', '2018-11-10 14:25:54', NULL, 'va todo lo que sirve para construir, cemento, arena, etc.'),
(7, 'Plomeria', '2018-11-11 12:00:04', NULL, 'todo con agua y tuberia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ciudad`
--

CREATE TABLE `tbl_ciudad` (
  `id_ciudad` int(11) NOT NULL,
  `nombre_ciudad` varchar(50) NOT NULL,
  `pais_ciudad` varchar(50) NOT NULL,
  `codigo_ciudad` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_ciudad`
--

INSERT INTO `tbl_ciudad` (`id_ciudad`, `nombre_ciudad`, `pais_ciudad`, `codigo_ciudad`) VALUES
(1, 'Cali', 'Colombia', '1'),
(2, 'Bogota', 'Colombia', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_cliente`
--

CREATE TABLE `tbl_cliente` (
  `id_cliente` int(11) NOT NULL,
  `document_cliente` varchar(50) NOT NULL,
  `id_tipo_documento` int(11) NOT NULL,
  `empresa_cliente` varchar(50) DEFAULT NULL,
  `nombre_repre_cliente` varchar(50) NOT NULL,
  `apellido_repre_cliente` varchar(50) NOT NULL,
  `telefono_cliente` varchar(50) NOT NULL,
  `direccion_cliente` varchar(50) NOT NULL,
  `correo_cliente` varchar(50) NOT NULL,
  `contrasena_cliente` varchar(60) NOT NULL,
  `id_estado_usuario` int(11) NOT NULL,
  `fecha_creacion_cliente` datetime NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `compras` int(11) DEFAULT NULL,
  `ultima_compra` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_cliente`
--

INSERT INTO `tbl_cliente` (`id_cliente`, `document_cliente`, `id_tipo_documento`, `empresa_cliente`, `nombre_repre_cliente`, `apellido_repre_cliente`, `telefono_cliente`, `direccion_cliente`, `correo_cliente`, `contrasena_cliente`, `id_estado_usuario`, `fecha_creacion_cliente`, `id_ciudad`, `compras`, `ultima_compra`) VALUES
(1, '1143862026', 1, 'Pd Partner', 'Sergio', 'Buitrago', '2147483647', 'Crr 45 # 12b33', 'SergioBuitrago11.4@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auI6BrvDKVFpVAlTdqogIZAEPSE2tm/Y6', 1, '2018-11-18 11:46:29', 1, 39, '2018-11-22 23:30:27'),
(5, '1234', 1, 'eso', 'David', 'Bermudez', '3123', 'cra 23 # 213', 'asd@gmail.com', '$2a$07$asxx54ahjppf45sd87a5au4BM6WWaRJDiFd34u0n2uHxx9ttCZSBO', 1, '2018-11-23 00:24:05', 1, 391, '2018-11-24 00:13:52'),
(17, '123456789', 1, '', 'Carlos', 'Ramirez', '3673225', 'cra 45 # 12-43', 'cramirez@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auGZEtGHuyZwm.Ur.FJvWLCql3nmsMbXy', 1, '2018-11-23 20:34:09', 1, 269, '2018-11-23 21:55:20'),
(18, '987654321', 2, '', 'Andres', 'Gonzales', '6756324', 'calle 10 # 12 - 89', 'agonzales@hotmail.com', '$2a$07$asxx54ahjppf45sd87a5auGZEtGHuyZwm.Ur.FJvWLCql3nmsMbXy', 1, '2018-11-23 20:35:44', 2, 67, '2018-11-23 20:42:48'),
(20, '1532785', 1, 'Meraky', 'Sara', 'Hernandez', '74323653', 'Diagonal 34 # 15b - 23', 'saraih@gmail.com', '$2a$07$asxx54ahjppf45sd87a5auGZEtGHuyZwm.Ur.FJvWLCql3nmsMbXy', 1, '2018-11-23 20:40:04', 1, 113, '2018-11-23 20:55:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_detalle_factura`
--

CREATE TABLE `tbl_detalle_factura` (
  `id_detalle_factura` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_factura` int(11) NOT NULL,
  `cantidad_detalle_factura` float NOT NULL,
  `total_detalle_factura` varchar(50) NOT NULL,
  `fecha_creacion_detalle_factura` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_detalle_oc`
--

CREATE TABLE `tbl_detalle_oc` (
  `id_detalle_oc` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_orden_compra` int(11) NOT NULL,
  `cantidad_detalle_oc` float NOT NULL,
  `total_detalle_oc` varchar(50) NOT NULL,
  `fecha_creacion_detalle_oc` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_devolucion`
--

CREATE TABLE `tbl_devolucion` (
  `id_dovolucion` int(11) NOT NULL,
  `id_detalle_factura` int(11) NOT NULL,
  `motivo_devolucion` varchar(250) NOT NULL,
  `fecha_devolucion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estado_producto`
--

CREATE TABLE `tbl_estado_producto` (
  `id_estado_producto` int(11) NOT NULL,
  `nombre_estado_producto` varchar(50) NOT NULL,
  `fecha_creacion_usuario` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_estado_producto`
--

INSERT INTO `tbl_estado_producto` (`id_estado_producto`, `nombre_estado_producto`, `fecha_creacion_usuario`) VALUES
(1, 'Activo', '2011-09-18 00:00:00'),
(2, 'Inactivo', '2011-09-18 00:00:00'),
(3, 'Inventario', '2011-09-18 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estado_usuario`
--

CREATE TABLE `tbl_estado_usuario` (
  `id_estado_usuario` int(11) NOT NULL,
  `nombre_estado_usuario` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_estado_usuario`
--

INSERT INTO `tbl_estado_usuario` (`id_estado_usuario`, `nombre_estado_usuario`) VALUES
(1, 'Activo'),
(2, 'Inactivo'),
(3, 'Bloqueado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_factura`
--

CREATE TABLE `tbl_factura` (
  `id_factura` int(11) NOT NULL,
  `id_dian_factura` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `detalle_factura` text NOT NULL,
  `id_forma_pago` int(11) NOT NULL,
  `iva_factura` float NOT NULL,
  `neto_factura` float NOT NULL,
  `total_factura` float NOT NULL,
  `fecha_creacion_factura` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_factura`
--

INSERT INTO `tbl_factura` (`id_factura`, `id_dian_factura`, `id_cliente`, `id_usuario`, `detalle_factura`, `id_forma_pago`, `iva_factura`, `neto_factura`, `total_factura`, `fecha_creacion_factura`) VALUES
(1, 10001, 1, 1, '[{\"id\":\"4\",\"descripcion\":\"taladro inalambrico\",\"cantidad\":\"1\",\"stock\":\"57\",\"precio\":\"380\",\"total\":\"380\"}]', 1, 3.8, 380, 383.8, '2018-01-22 00:03:43'),
(2, 10002, 1, 1, '[{\"id\":\"4\",\"descripcion\":\"taladro inalambrico\",\"cantidad\":\"6\",\"stock\":\"51\",\"precio\":\"380\",\"total\":\"2280\"},{\"id\":\"26\",\"descripcion\":\"Taladro percutor\",\"cantidad\":\"4\",\"stock\":\"246\",\"precio\":\"741.986\",\"total\":\"2967.94\"},{\"id\":\"25\",\"descripcion\":\"Tijera para andamio\",\"cantidad\":\"1\",\"stock\":\"499\",\"precio\":\"14\",\"total\":\"14\"},{\"id\":\"21\",\"descripcion\":\"Escuadra\",\"cantidad\":\"3\",\"stock\":\"197\",\"precio\":\"42\",\"total\":\"126\"},{\"id\":\"9\",\"descripcion\":\"Toma corriente\",\"cantidad\":\"5\",\"stock\":\"245\",\"precio\":\"6.5\",\"total\":\"32.5\"},{\"id\":\"28\",\"descripcion\":\"Llaves para agua\",\"cantidad\":\"5\",\"stock\":\"445\",\"precio\":\"8.4\",\"total\":\"42\"}]', 1, 546.244, 5462.44, 6008.68, '2018-02-22 01:09:14'),
(3, 10003, 1, 9, '[{\"id\":\"4\",\"descripcion\":\"taladro inalambrico\",\"cantidad\":\"1\",\"stock\":\"50\",\"precio\":\"380\",\"total\":\"380\"}]', 1, 19, 380, 399, '2018-03-01 05:00:00'),
(4, 10004, 1, 1, '[{\"id\":\"10\",\"descripcion\":\"Brocas\",\"cantidad\":\"1\",\"stock\":\"499\",\"precio\":\"22\",\"total\":\"22\"},{\"id\":\"15\",\"descripcion\":\"Martillo demoledor\",\"cantidad\":\"1\",\"stock\":\"159\",\"precio\":\"9.8\",\"total\":\"9.8\"}]', 1, 6.042, 31.8, 37.842, '2018-03-16 02:18:30'),
(9, 10005, 1, 1, '[{\"id\":\"15\",\"descripcion\":\"Martillo demoledor\",\"cantidad\":\"6\",\"stock\":\"153\",\"precio\":\"9.8\",\"total\":\"58.8\"}]', 1, 11.172, 58.8, 69.972, '2018-03-25 04:30:28'),
(10, 10006, 5, 1, '[{\"id\":\"10\",\"descripcion\":\"Brocas\",\"cantidad\":\"1\",\"stock\":\"498\",\"precio\":\"22\",\"total\":\"22\"}]', 1, 4.18, 22, 26.18, '2018-04-23 18:28:18'),
(11, 10007, 5, 9, '[{\"id\":\"26\",\"descripcion\":\"Taladro percutor\",\"cantidad\":\"1\",\"stock\":\"245\",\"precio\":\"741.986\",\"total\":\"741.99\"},{\"id\":\"20\",\"descripcion\":\"Elevador de construccion\",\"cantidad\":\"1\",\"stock\":\"149\",\"precio\":\"47.6\",\"total\":\"47.6\"},{\"id\":\"15\",\"descripcion\":\"Martillo demoledor\",\"cantidad\":\"1\",\"stock\":\"152\",\"precio\":\"9.8\",\"total\":\"9.8\"}]', 1, 151.884, 799.39, 951.274, '2018-06-23 23:13:44'),
(12, 10008, 5, 9, '[{\"id\":\"5\",\"descripcion\":\"cascos\",\"cantidad\":\"1\",\"stock\":\"99\",\"precio\":\"60\",\"total\":\"60\"},{\"id\":\"2\",\"descripcion\":\"Pala\",\"cantidad\":\"1\",\"stock\":\"49\",\"precio\":\"17\",\"total\":\"17\"},{\"id\":\"8\",\"descripcion\":\"Extenciones\",\"cantidad\":\"1\",\"stock\":\"499\",\"precio\":\"75.5\",\"total\":\"75.5\"},{\"id\":\"19\",\"descripcion\":\"Gafas\",\"cantidad\":\"6\",\"stock\":\"294\",\"precio\":\"15.4\",\"total\":\"92.4\"},{\"id\":\"18\",\"descripcion\":\"Porra\",\"cantidad\":\"1\",\"stock\":\"349\",\"precio\":\"35\",\"total\":\"35\"},{\"id\":\"29\",\"descripcion\":\"Empaque de manguera\",\"cantidad\":\"2\",\"stock\":\"348\",\"precio\":\"300\",\"total\":\"600\"},{\"id\":\"28\",\"descripcion\":\"Llaves para agua\",\"cantidad\":\"4\",\"stock\":\"441\",\"precio\":\"8.4\",\"total\":\"33.6\"},{\"id\":\"11\",\"descripcion\":\"Ladrillos\",\"cantidad\":\"1\",\"stock\":\"1999\",\"precio\":\"70\",\"total\":\"70\"}]', 1, 186.865, 983.5, 1170.36, '2018-07-15 01:31:51'),
(13, 10009, 18, 9, '[{\"id\":\"20\",\"descripcion\":\"Elevador de construccion\",\"cantidad\":\"1\",\"stock\":\"148\",\"precio\":\"47.6\",\"total\":\"47.6\"},{\"id\":\"24\",\"descripcion\":\"Escalera\",\"cantidad\":\"6\",\"stock\":\"224\",\"precio\":\"63\",\"total\":\"378\"},{\"id\":\"30\",\"descripcion\":\"Tejas\",\"cantidad\":\"10\",\"stock\":\"690\",\"precio\":\"11.2\",\"total\":\"112\"},{\"id\":\"3\",\"descripcion\":\"grava\",\"cantidad\":\"50\",\"stock\":\"950\",\"precio\":\"22\",\"total\":\"1100\"}]', 1, 311.144, 1637.6, 1948.74, '2018-08-06 01:42:48'),
(14, 10010, 20, 14, '[{\"id\":\"24\",\"descripcion\":\"Escalera\",\"cantidad\":\"8\",\"stock\":\"216\",\"precio\":\"63\",\"total\":\"504\"},{\"id\":\"11\",\"descripcion\":\"Ladrillos\",\"cantidad\":\"100\",\"stock\":\"1899\",\"precio\":\"70\",\"total\":\"7000\"},{\"id\":\"16\",\"descripcion\":\"Tapa oidos\",\"cantidad\":\"5\",\"stock\":\"295\",\"precio\":\"42\",\"total\":\"210\"}]', 1, 1465.66, 7714, 9179.66, '2018-08-22 01:55:00'),
(15, 10011, 17, 9, '[{\"id\":\"24\",\"descripcion\":\"Escalera\",\"cantidad\":\"1\",\"stock\":\"215\",\"precio\":\"63\",\"total\":\"63\"},{\"id\":\"20\",\"descripcion\":\"Elevador de construccion\",\"cantidad\":\"1\",\"stock\":\"147\",\"precio\":\"47.6\",\"total\":\"47.6\"},{\"id\":\"27\",\"descripcion\":\"Botas de seguridad\",\"cantidad\":\"40\",\"stock\":\"360\",\"precio\":\"112\",\"total\":\"4480\"},{\"id\":\"17\",\"descripcion\":\"Varillas\",\"cantidad\":\"200\",\"stock\":\"400\",\"precio\":\"2.8\",\"total\":\"560\"}]', 1, 978.614, 5150.6, 6129.21, '2018-09-12 01:58:46'),
(16, 10012, 17, 9, '[{\"id\":\"10\",\"descripcion\":\"Brocas\",\"cantidad\":\"27\",\"stock\":\"471\",\"precio\":\"22\",\"total\":\"594\"}]', 1, 112.86, 594, 706.86, '2018-10-04 02:55:21'),
(17, 10013, 5, 9, '[{\"id\":\"4\",\"descripcion\":\"taladro inalambrico\",\"cantidad\":\"4\",\"stock\":\"46\",\"precio\":\"380\",\"total\":\"1520\"},{\"id\":\"26\",\"descripcion\":\"Taladro percutor\",\"cantidad\":\"13\",\"stock\":\"232\",\"precio\":\"741.986\",\"total\":\"9645.82\"}]', 1, 1786.53, 11165.8, 12952.4, '2018-10-14 05:06:01'),
(18, 10014, 5, 9, '[{\"id\":\"10\",\"descripcion\":\"Brocas\",\"cantidad\":\"5\",\"stock\":\"466\",\"precio\":\"22\",\"total\":\"110\"},{\"id\":\"26\",\"descripcion\":\"Taladro percutor\",\"cantidad\":\"1\",\"stock\":\"231\",\"precio\":\"741.986\",\"total\":\"741.99\"},{\"id\":\"24\",\"descripcion\":\"Escalera\",\"cantidad\":\"1\",\"stock\":\"214\",\"precio\":\"63\",\"total\":\"63\"},{\"id\":\"20\",\"descripcion\":\"Elevador de construccion\",\"cantidad\":\"1\",\"stock\":\"146\",\"precio\":\"47.6\",\"total\":\"47.6\"},{\"id\":\"5\",\"descripcion\":\"cascos\",\"cantidad\":\"28\",\"stock\":\"71\",\"precio\":\"60\",\"total\":\"1680\"},{\"id\":\"9\",\"descripcion\":\"Toma corriente\",\"cantidad\":\"17\",\"stock\":\"228\",\"precio\":\"6.5\",\"total\":\"110.5\"},{\"id\":\"8\",\"descripcion\":\"Extenciones\",\"cantidad\":\"70\",\"stock\":\"429\",\"precio\":\"75.5\",\"total\":\"5285\"},{\"id\":\"30\",\"descripcion\":\"Tejas\",\"cantidad\":\"30\",\"stock\":\"660\",\"precio\":\"11.2\",\"total\":\"336\"},{\"id\":\"3\",\"descripcion\":\"grava\",\"cantidad\":\"200\",\"stock\":\"750\",\"precio\":\"22\",\"total\":\"4400\"}]', 1, 2427.08, 12774.1, 15201.2, '2018-11-11 05:13:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_forma_pago`
--

CREATE TABLE `tbl_forma_pago` (
  `id_forma_pago` int(11) NOT NULL,
  `nombre_forma_pago` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_forma_pago`
--

INSERT INTO `tbl_forma_pago` (`id_forma_pago`, `nombre_forma_pago`) VALUES
(1, 'Efectivo'),
(2, 'Credito'),
(3, 'PayPal'),
(4, 'PSE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_orden_compra`
--

CREATE TABLE `tbl_orden_compra` (
  `id_orden_compra` int(11) NOT NULL,
  `nombre_orden_compra` varchar(45) NOT NULL,
  `fecha_creacion_orden_compra` datetime NOT NULL,
  `id_proveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_orden_compra`
--

INSERT INTO `tbl_orden_compra` (`id_orden_compra`, `nombre_orden_compra`, `fecha_creacion_orden_compra`, `id_proveedor`) VALUES
(1, 'Compra Chatarreria', '2018-11-18 14:53:59', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_orden_producto`
--

CREATE TABLE `tbl_orden_producto` (
  `id_orden_producto` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_orden_compra` int(11) NOT NULL,
  `precio_unidad` float NOT NULL,
  `cantidad_producto` float NOT NULL,
  `total_producto` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbl_orden_producto`
--

INSERT INTO `tbl_orden_producto` (`id_orden_producto`, `id_producto`, `id_orden_compra`, `precio_unidad`, `cantidad_producto`, `total_producto`) VALUES
(1, 1, 1, 20000, 2, 40000),
(2, 3, 1, 2000, 2, 4000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_pedido`
--

CREATE TABLE `tbl_pedido` (
  `id_pedido` int(11) NOT NULL,
  `numero_pedido` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_creacion_pedido` datetime NOT NULL,
  `cantidad_pedido` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_pedido`
--

INSERT INTO `tbl_pedido` (`id_pedido`, `numero_pedido`, `id_cliente`, `id_producto`, `fecha_creacion_pedido`, `cantidad_pedido`) VALUES
(1, 1, 1, 1, '2018-11-18 14:09:23', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_producto`
--

CREATE TABLE `tbl_producto` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(50) NOT NULL,
  `codigo_barra_producto` varchar(50) NOT NULL,
  `cantidad_producto` int(11) NOT NULL,
  `precio_compra_producto` float NOT NULL,
  `precio_salida_producto` float NOT NULL,
  `unidad_medicion_producto` varchar(255) NOT NULL,
  `presentacion_producto` varchar(50) NOT NULL,
  `fecha_creacion_producto` datetime NOT NULL,
  `imagen_producto` varchar(255) DEFAULT NULL,
  `description_producto` varchar(255) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_estado_producto` int(11) NOT NULL,
  `vendidos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_producto`
--

INSERT INTO `tbl_producto` (`id_producto`, `nombre_producto`, `codigo_barra_producto`, `cantidad_producto`, `precio_compra_producto`, `precio_salida_producto`, `unidad_medicion_producto`, `presentacion_producto`, `fecha_creacion_producto`, `imagen_producto`, `description_producto`, `id_categoria`, `id_estado_producto`, `vendidos`) VALUES
(1, 'Pulidoras', '4321345', 8, 153.9, 155, 'Unidad', 'cajas', '2011-06-18 00:00:00', 'vistas/img/productos/4321345/504.jpg', 'Pulidora De 4- 1/2\" 650W G650: velocidad 12000/min.Eje 5/8??.Cable 2 m.Potencia 650W.', 1, 1, 5),
(2, 'Pala', '5436792', 49, 15, 17, 'Unidad', 'ninguna', '2011-11-10 00:00:00', 'vistas/img/productos/5436792/783.jpg', 'Pala redonda o de punta #2 estampada en acero, tratada termicamente', 4, 1, 1),
(3, 'grava', '4521679', 750, 20, 22, 'unidad', 'ninguna', '2018-11-17 20:34:22', 'vistas/img/productos/4521679/116.png', 'grava para construccion', 6, 1, 250),
(4, 'taladro inalambrico', '2764891', 46, 379, 380, 'unidad', 'cajas', '2018-11-17 20:25:38', 'vistas/img/productos/2764891/264.jpg', 'TALADRO PERCUTOR INALAMBRICO ION DE LITIO con velocidadvariable. MANDRIL:1/2\". POTENCIA: 14,4V. VELOCIDAD: 0-500 / 0-1600RPM ', 2, 1, 14),
(5, 'cascos', '3264810', 71, 59, 60, 'unidad', 'cajas empaque', '2018-11-17 20:26:08', 'vistas/img/productos/3264810/111.jpg', 'casco de construccion', 4, 1, 29),
(6, 'Guantes', '8426537', 100, 30, 32.5, 'par', 'empaque', '2018-11-17 20:26:18', 'vistas/img/productos/8426537/107.jpg', 'Guante tipo soldador de carnaza, afelpado por dentro y con costuras de hilo kevlar. ', 4, 1, 0),
(7, 'Puntillas de acero', '4164793', 600, 4.5, 6, 'cajas', 'cajas', '2018-11-17 20:27:40', 'vistas/img/productos/4164793/965.jpg', 'puntillas de acero para concreeto y pared', 6, 1, 0),
(8, 'Extenciones', '8427536', 429, 70, 75.5, 'metros', 'cajas empaques', '2018-11-17 20:26:56', 'vistas/img/productos/8427536/439.jpg', 'Extension C/Piloto 3X10 5Mt Roja La extensión eléctrica con luz piloto 3x10AWG marca Techbay ', 5, 1, 71),
(9, 'Toma corriente', '1439482', 228, 5, 6.5, 'unidad', 'empaque', '2018-11-17 20:27:10', 'vistas/img/productos/1439482/621.jpg', 'Toma Corriente Doble Con Polo A Tierra Color Blanco15 Amperios150 VoltiosLínea Boreale ', 5, 1, 22),
(10, 'Brocas', '4928429', 466, 20, 22, 'unidad', 'cajas', '2018-11-17 20:27:22', 'vistas/img/productos/4928429/659.jpg', 'brocas de titanio para pared y concreto', 2, 1, 34),
(11, 'Ladrillos', '5932048', 1899, 50, 70, 'unidad', 'ninguna', '2018-11-17 20:28:51', 'vistas/img/productos/5932048/504.jpg', 'ladrillos para construccion', 6, 1, 101),
(12, 'Tablas', '5021958', 850, 11, 15.4, 'unidad', 'ninguna', '2018-11-17 20:29:14', 'vistas/img/productos/5021958/581.jpg', 'tablas para construccion', 6, 1, 0),
(13, 'Flexometro', '3029483', 430, 25, 35, 'unidad', 'cajas', '2018-11-17 20:29:25', 'vistas/img/productos/3029483/536.jpg', 'flexometro de tres metros para mediciones de largo alcance', 4, 1, 0),
(14, 'Segueta', '4359201', 250, 50, 70, 'unidad', 'cajas', '2018-11-17 20:29:38', 'vistas/img/productos/4359201/357.jpg', 'segueta de acero para corte de madera u otro material', 4, 1, 0),
(15, 'Martillo demoledor', '7294816', 152, 7, 9.8, 'unidad', 'ninguna', '2018-11-17 20:29:53', 'vistas/img/productos/7294816/941.jpg', 'MARTILLO DEMOLEDOR 69J 2000W BOSCH GSH 27 VCEl martillo de demolición eléctrico', 2, 1, 8),
(16, 'Tapa oidos', '5210582', 295, 30, 42, 'unidad', 'cajas', '2018-11-17 20:30:07', 'vistas/img/productos/5210582/411.jpg', 'tapa oidos para el cuidado de ruidos fuertes', 4, 1, 5),
(17, 'Varillas', '2059184', 400, 2, 2.8, 'unidad', 'ninguna', '2018-11-17 20:30:24', 'vistas/img/productos/2059184/716.jpg', 'barilla para construccion', 6, 1, 200),
(18, 'Porra', '4839284', 349, 25, 35, 'unidad', 'ninguna', '2018-11-17 20:30:37', 'vistas/img/productos/4839284/544.jpg', ' hecho de hierro con mango de fibra de vidrio. 5Kg Largo: 880mm. Ancho: 180mm. Diametro: 64mm 6Kg Largo: 880mm', 4, 1, 1),
(19, 'Gafas', '3394038', 294, 11, 15.4, 'unidad', 'cajas', '2018-11-17 20:31:10', 'vistas/img/productos/3394038/384.jpg', 'gafas de proteccion', 4, 1, 6),
(20, 'Elevador de construccion', '38295837', 146, 34, 47.6, 'unidad', 'ninguna', '2018-11-17 20:31:30', 'vistas/img/productos/38295837/334.jpg', 'elevador de construcción para alturas', 3, 1, 4),
(21, 'Escuadra', '9424859', 197, 30, 42, 'unidad', 'empaque', '2018-11-17 20:31:43', 'vistas/img/productos/9424859/526.jpg', 'escuadra para mediciones cortas', 4, 1, 3),
(22, 'Focos', '3729482', 350, 3, 4.2, 'unidad', 'cajas', '2018-11-17 20:31:56', 'vistas/img/productos/3729482/164.jpg', 'focos de energia', 5, 1, 0),
(24, 'Escalera', '2395036', 214, 45, 63, 'unidad', 'ninguna', '2018-11-17 20:32:13', 'vistas/img/productos/2395036/641.jpg', 'escalera para trabajos de media altura', 3, 1, 16),
(25, 'Tijera para andamio', '3928494', 499, 10, 14, 'unidad', 'ninguna', '2018-11-17 20:32:26', 'vistas/img/productos/3928494/830.jpg', 'tijera para asegurar andamio', 3, 1, 1),
(26, 'Taladro percutor', '3463285', 231, 529.99, 741.986, 'unidad', 'cajas', '2018-11-17 20:32:37', 'vistas/img/productos/3463285/945.jpg', 'taladro para trabajos exigentes', 2, 1, 19),
(27, 'Botas de seguridad', '4938274', 360, 80, 112, 'unidad', 'cajas', '2018-11-17 20:32:48', 'vistas/img/productos/4938274/829.jpg', 'botas de seguridad para trabajos de riesgo', 4, 1, 40),
(28, 'Llaves para agua', '39295824', 441, 6, 8.4, 'unidad', 'empaque', '2018-11-17 20:33:01', 'vistas/img/productos/39295824/156.jpg', 'llaves para salida de agua', 7, 1, 9),
(29, 'Empaque de manguera', '2364895', 348, 200, 300, 'unidad', 'ninguna', '2018-11-17 20:33:13', 'vistas/img/productos/2364895/567.jpg', 'empaque de manguera para evitar fugas', 7, 1, 2),
(30, 'Tejas', '2538684', 660, 8, 11.2, 'unidad', 'ninguna', '2018-11-17 20:33:26', 'vistas/img/productos/2538684/267.jpg', 'tejas para construcción de techos', 6, 1, 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_promociones`
--

CREATE TABLE `tbl_promociones` (
  `id_promocion` int(11) NOT NULL,
  `imagen_promocion` varchar(255) NOT NULL,
  `enlace_promocion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_proveedor`
--

CREATE TABLE `tbl_proveedor` (
  `id_proveedor` int(11) NOT NULL,
  `documento_proveedor` varchar(50) NOT NULL,
  `id_tipo_documento` int(11) NOT NULL,
  `empresa_proveedor` varchar(50) NOT NULL,
  `nombre_repre_proveedor` varchar(50) NOT NULL,
  `apellido_repre_proveedor` varchar(50) NOT NULL,
  `telefono_proveedor` varchar(50) NOT NULL,
  `direccion_proveedor` varchar(50) NOT NULL,
  `correo_proveedor` varchar(50) NOT NULL,
  `imagen_proveedor` varchar(255) DEFAULT NULL,
  `fecha_creacion_proveedor` datetime NOT NULL,
  `id_ciudad` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_proveedor`
--

INSERT INTO `tbl_proveedor` (`id_proveedor`, `documento_proveedor`, `id_tipo_documento`, `empresa_proveedor`, `nombre_repre_proveedor`, `apellido_repre_proveedor`, `telefono_proveedor`, `direccion_proveedor`, `correo_proveedor`, `imagen_proveedor`, `fecha_creacion_proveedor`, `id_ciudad`, `id_producto`) VALUES
(1, '123456', 1, 'Miselania la 15', 'Miselania la 15', 'Molina', '3146854214', 'La base', 'Miselaniala15@hotmail.com.comq', NULL, '0000-00-00 00:00:00', 1, 1),
(2, '83465741', 1, 'Tornillos el Loco', 'Tornillos El loco', 'Booyle', '3214964752', 'calle 6 norte 50-12', 'Charlie.Booyle@lenovo.com', NULL, '2018-11-15 08:41:31', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipo_documento`
--

CREATE TABLE `tbl_tipo_documento` (
  `id_tipo_documento` int(11) NOT NULL,
  `nombre_tipo_documento` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_tipo_documento`
--

INSERT INTO `tbl_tipo_documento` (`id_tipo_documento`, `nombre_tipo_documento`) VALUES
(1, 'Cedula'),
(2, 'Extranjeria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipo_usuario`
--

CREATE TABLE `tbl_tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL,
  `nombre_tipo_usuario` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_tipo_usuario`
--

INSERT INTO `tbl_tipo_usuario` (`id_tipo_usuario`, `nombre_tipo_usuario`) VALUES
(1, 'Admin'),
(2, 'Analista'),
(3, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuario`
--

CREATE TABLE `tbl_usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(50) NOT NULL,
  `apellido_usuario` varchar(50) NOT NULL,
  `nick_user` varchar(50) NOT NULL,
  `correo_usuario` varchar(255) NOT NULL,
  `contrasena_usuario` varchar(255) NOT NULL,
  `fecha_creacion_usuario` datetime NOT NULL,
  `imagen_usuario` varchar(255) DEFAULT NULL,
  `id_estado_usuario` int(11) NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`id_usuario`, `nombre_usuario`, `apellido_usuario`, `nick_user`, `correo_usuario`, `contrasena_usuario`, `fecha_creacion_usuario`, `imagen_usuario`, `id_estado_usuario`, `id_tipo_usuario`) VALUES
(1, 'administrador', 'trador', 'admin', 'admin@gmail.com', '0192023a7bbd73250516f069df18b500', '2018-08-17 14:30:06', 'vistas/img/usuarios/admin/admin.jpg', 1, 1),
(3, 'sergio', 'buitrago', 'shecho30', 'sergiobuitrago11.4@gmail.com', '1503dc538fa341ea6b2f0089d7bc2eb9', '2018-08-17 14:31:57', 'vistas/img/usuarios/shecho30/100.png', 1, 1),
(7, 'Antonio', 'Cobo', 'Viejo', 'ViejoDies@hotmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2018-10-26 14:59:29', '', 1, 1),
(9, 'David', 'Bermudez', 'keegan', 'asd@gmail.com', '202cb962ac59075b964b07152d234b70', '2018-11-21 23:16:05', 'vistas/img/usuarios/keegan/681.jpg', 1, 1),
(14, 'Aquiles', 'Traigo', 'vendedor1', 'vendedor1@sena.edu', '202cb962ac59075b964b07152d234b70', '2018-11-23 20:53:13', 'vistas/img/usuarios/vendedor1/345.png', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usersess`
--

CREATE TABLE `usersess` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usersess`
--

INSERT INTO `usersess` (`id`, `expire`, `data`) VALUES
('lik36boop08ogmlrcdb3i88jrj', 1542952437, 0x33336266653764306663316536306366393366303334366232623337313236315f5f72657475726e55726c7c613a313a7b693a303b733a31303a22736974652f696e646578223b7d5969692e4343617074636861416374696f6e2e66636534613831642e736974652e636170746368617c733a373a22726f6669736f64223b5969692e4343617074636861416374696f6e2e66636534613831642e736974652e63617074636861636f756e747c693a313b);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_categoria`
--
ALTER TABLE `tbl_categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `tbl_ciudad`
--
ALTER TABLE `tbl_ciudad`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD KEY `tbl_cliente_ibfk_1` (`id_tipo_documento`),
  ADD KEY `tbl_cliente_ibfk_2` (`id_ciudad`);

--
-- Indices de la tabla `tbl_detalle_factura`
--
ALTER TABLE `tbl_detalle_factura`
  ADD PRIMARY KEY (`id_detalle_factura`),
  ADD KEY `tbl_detalle_factura_ibfk_1` (`id_producto`),
  ADD KEY `tbl_detalle_factura_ibfk_2` (`id_factura`);

--
-- Indices de la tabla `tbl_detalle_oc`
--
ALTER TABLE `tbl_detalle_oc`
  ADD PRIMARY KEY (`id_detalle_oc`),
  ADD KEY `tbl_detalle_oc_ibfk_1` (`id_producto`),
  ADD KEY `tbl_detalle_oc_ibfk_2` (`id_orden_compra`);

--
-- Indices de la tabla `tbl_devolucion`
--
ALTER TABLE `tbl_devolucion`
  ADD PRIMARY KEY (`id_dovolucion`),
  ADD KEY `tbl_devolucion_ibfk_1` (`id_detalle_factura`);

--
-- Indices de la tabla `tbl_estado_producto`
--
ALTER TABLE `tbl_estado_producto`
  ADD PRIMARY KEY (`id_estado_producto`);

--
-- Indices de la tabla `tbl_estado_usuario`
--
ALTER TABLE `tbl_estado_usuario`
  ADD PRIMARY KEY (`id_estado_usuario`);

--
-- Indices de la tabla `tbl_factura`
--
ALTER TABLE `tbl_factura`
  ADD PRIMARY KEY (`id_factura`),
  ADD KEY `tbl_factura_ibfk_1` (`id_forma_pago`),
  ADD KEY `tbl_factura_ibfk_2` (`id_usuario`),
  ADD KEY `tbl_factura_ibfk_3` (`id_cliente`);

--
-- Indices de la tabla `tbl_forma_pago`
--
ALTER TABLE `tbl_forma_pago`
  ADD PRIMARY KEY (`id_forma_pago`);

--
-- Indices de la tabla `tbl_orden_compra`
--
ALTER TABLE `tbl_orden_compra`
  ADD PRIMARY KEY (`id_orden_compra`),
  ADD KEY `tbl_orden_compra_ibfk_3` (`id_proveedor`);

--
-- Indices de la tabla `tbl_orden_producto`
--
ALTER TABLE `tbl_orden_producto`
  ADD PRIMARY KEY (`id_orden_producto`),
  ADD KEY `fk_product_orden` (`id_producto`),
  ADD KEY `fk_orden_id` (`id_orden_compra`);

--
-- Indices de la tabla `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `tbl_pedido_ibfk_1` (`id_cliente`),
  ADD KEY `tbl_pedido_ibfk_2` (`id_producto`);

--
-- Indices de la tabla `tbl_producto`
--
ALTER TABLE `tbl_producto`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `tbl_producto_ibfk_1` (`id_categoria`),
  ADD KEY `tbl_producto_ibfk_2` (`id_estado_producto`);

--
-- Indices de la tabla `tbl_promociones`
--
ALTER TABLE `tbl_promociones`
  ADD PRIMARY KEY (`id_promocion`);

--
-- Indices de la tabla `tbl_proveedor`
--
ALTER TABLE `tbl_proveedor`
  ADD PRIMARY KEY (`id_proveedor`),
  ADD KEY `tbl_proveedor_ibfk_1` (`id_producto`);

--
-- Indices de la tabla `tbl_tipo_documento`
--
ALTER TABLE `tbl_tipo_documento`
  ADD PRIMARY KEY (`id_tipo_documento`);

--
-- Indices de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indices de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `tbl_usuario_ibfk_1` (`id_estado_usuario`),
  ADD KEY `tbl_usuario_ibfk_2` (`id_tipo_usuario`);

--
-- Indices de la tabla `usersess`
--
ALTER TABLE `usersess`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_categoria`
--
ALTER TABLE `tbl_categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tbl_ciudad`
--
ALTER TABLE `tbl_ciudad`
  MODIFY `id_ciudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `tbl_detalle_factura`
--
ALTER TABLE `tbl_detalle_factura`
  MODIFY `id_detalle_factura` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_detalle_oc`
--
ALTER TABLE `tbl_detalle_oc`
  MODIFY `id_detalle_oc` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_devolucion`
--
ALTER TABLE `tbl_devolucion`
  MODIFY `id_dovolucion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_estado_producto`
--
ALTER TABLE `tbl_estado_producto`
  MODIFY `id_estado_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_estado_usuario`
--
ALTER TABLE `tbl_estado_usuario`
  MODIFY `id_estado_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_factura`
--
ALTER TABLE `tbl_factura`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `tbl_forma_pago`
--
ALTER TABLE `tbl_forma_pago`
  MODIFY `id_forma_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_orden_compra`
--
ALTER TABLE `tbl_orden_compra`
  MODIFY `id_orden_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_orden_producto`
--
ALTER TABLE `tbl_orden_producto`
  MODIFY `id_orden_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_producto`
--
ALTER TABLE `tbl_producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `tbl_promociones`
--
ALTER TABLE `tbl_promociones`
  MODIFY `id_promocion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_proveedor`
--
ALTER TABLE `tbl_proveedor`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_tipo_documento`
--
ALTER TABLE `tbl_tipo_documento`
  MODIFY `id_tipo_documento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  MODIFY `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_cliente`
--
ALTER TABLE `tbl_cliente`
  ADD CONSTRAINT `tbl_cliente_ibfk_1` FOREIGN KEY (`id_tipo_documento`) REFERENCES `tbl_tipo_documento` (`id_tipo_documento`),
  ADD CONSTRAINT `tbl_cliente_ibfk_2` FOREIGN KEY (`id_ciudad`) REFERENCES `tbl_ciudad` (`id_ciudad`);

--
-- Filtros para la tabla `tbl_detalle_factura`
--
ALTER TABLE `tbl_detalle_factura`
  ADD CONSTRAINT `tbl_detalle_factura_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`),
  ADD CONSTRAINT `tbl_detalle_factura_ibfk_2` FOREIGN KEY (`id_factura`) REFERENCES `tbl_factura` (`id_factura`);

--
-- Filtros para la tabla `tbl_detalle_oc`
--
ALTER TABLE `tbl_detalle_oc`
  ADD CONSTRAINT `tbl_detalle_oc_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`),
  ADD CONSTRAINT `tbl_detalle_oc_ibfk_2` FOREIGN KEY (`id_orden_compra`) REFERENCES `tbl_orden_compra` (`id_orden_compra`);

--
-- Filtros para la tabla `tbl_devolucion`
--
ALTER TABLE `tbl_devolucion`
  ADD CONSTRAINT `tbl_devolucion_ibfk_1` FOREIGN KEY (`id_detalle_factura`) REFERENCES `tbl_detalle_factura` (`id_detalle_factura`);

--
-- Filtros para la tabla `tbl_factura`
--
ALTER TABLE `tbl_factura`
  ADD CONSTRAINT `tbl_factura_ibfk_1` FOREIGN KEY (`id_forma_pago`) REFERENCES `tbl_forma_pago` (`id_forma_pago`),
  ADD CONSTRAINT `tbl_factura_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`),
  ADD CONSTRAINT `tbl_factura_ibfk_3` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_cliente` (`id_cliente`);

--
-- Filtros para la tabla `tbl_orden_compra`
--
ALTER TABLE `tbl_orden_compra`
  ADD CONSTRAINT `tbl_orden_compra_ibfk_3` FOREIGN KEY (`id_proveedor`) REFERENCES `tbl_proveedor` (`id_proveedor`);

--
-- Filtros para la tabla `tbl_orden_producto`
--
ALTER TABLE `tbl_orden_producto`
  ADD CONSTRAINT `fk_orden_id` FOREIGN KEY (`id_orden_compra`) REFERENCES `tbl_orden_compra` (`id_orden_compra`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_product_orden` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`);

--
-- Filtros para la tabla `tbl_pedido`
--
ALTER TABLE `tbl_pedido`
  ADD CONSTRAINT `tbl_pedido_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_cliente` (`id_cliente`),
  ADD CONSTRAINT `tbl_pedido_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`);

--
-- Filtros para la tabla `tbl_producto`
--
ALTER TABLE `tbl_producto`
  ADD CONSTRAINT `tbl_producto_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `tbl_categoria` (`id_categoria`),
  ADD CONSTRAINT `tbl_producto_ibfk_2` FOREIGN KEY (`id_estado_producto`) REFERENCES `tbl_estado_producto` (`id_estado_producto`);

--
-- Filtros para la tabla `tbl_proveedor`
--
ALTER TABLE `tbl_proveedor`
  ADD CONSTRAINT `tbl_proveedor_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id_producto`);

--
-- Filtros para la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD CONSTRAINT `tbl_usuario_ibfk_1` FOREIGN KEY (`id_estado_usuario`) REFERENCES `tbl_estado_usuario` (`id_estado_usuario`),
  ADD CONSTRAINT `tbl_usuario_ibfk_2` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tbl_tipo_usuario` (`id_tipo_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
