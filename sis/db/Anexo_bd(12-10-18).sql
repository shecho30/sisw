/*Orden de compra*/

/* se edita la tabla orden de compra quedando con la siguiente estructura.
	id_orden_compra	int(11)
	nombre_orden_compra	varchar(45)
	fecha_creacion_orden_compra	datetime
	id_proveedor	int(11)
*/

/*
	Se agrega la tabla orden producto, para hacer la relacion de la orden de compra con la de productos
    Se edita la tabla orden de compra, para crear una asociativa.
*/
CREATE TABLE tbl_orden_producto
(
	id_orden_producto INT PRIMARY KEY AUTO_INCREMENT,
    id_producto INT NOT NULL,
    id_orden_compra INT NOT NULL,
    precio_unidad FLOAT NOT NULL,
    cantidad_producto FLOAT NOT NULL,
    total_producto FLOAT NOT NULL
);

/*Se agrega la llave que una la tabla intermedia con la tabla orden compra*/

ALTER TABLE tbl_orden_producto
ADD CONSTRAINT fk_orden
FOREIGN KEY (id_orden_compra) 
REFERENCES tbl_orden_compra(id_orden_compra);

/*Se agrega la llave que una la tabla intermedia con la tabla producto*/

ALTER TABLE tbl_orden_producto
ADD CONSTRAINT fk_product
FOREIGN KEY (id_producto) 
REFERENCES tbl_producto(id_producto);


