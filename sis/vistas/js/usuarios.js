/*=============================================
SUBIENDO LA FOTO DEL USUARIO
=============================================*/
$(".nuevaFoto").change(function(){

	var imagen = this.files[0];
	
	/*=============================================
  	VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
  	=============================================*/

  	if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

  		$(".nuevaFoto").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen debe estar en formato JPG o PNG!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else if(imagen["size"] > 2000000){

  		$(".nuevaFoto").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen no debe pesar más de 2MB!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else{

  		var datosImagen = new FileReader;
  		datosImagen.readAsDataURL(imagen);

  		$(datosImagen).on("load", function(event){

  			var rutaImagen = event.target.result;

  			$(".previsualizar").attr("src", rutaImagen);

  		})

  	}
})

/*=============================================
EDITAR USUARIO
=============================================*/
$(".tablas").on("click", ".btnEditarUsuario", function(){

	var idUsuario = $(this).attr("idUsuario");
	//alert(idUsuario);
	var datos = new FormData();
	datos.append("idUsuario", idUsuario);

	$.ajax({

		url:"ajax/usuarios.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
			//alert(respuesta)
			$("#id_usuario").val(respuesta["id_usuario"]);
			$("#editarNombre").val(respuesta["nombre_usuario"]);
			$("#editarApellido").val(respuesta["apellido_usuario"]);
			$("#editarUsuario").val(respuesta["nick_user"]);
			$("#editarCorreo").val(respuesta["correo_usuario"]);
			$("#editarEstado").val(respuesta["id_estado_usuario"]);
			//$("#editarPerfil").html(respuesta["id_tipo_usuario"]);
			$("#editarPerfil").val(respuesta["id_tipo_usuario"]);
			$("#fotoActual").val(respuesta["imagen_usuario"]);

			
			// $("#editarPassword").val(respuesta["contrasena_usuario"]);

			if(respuesta["imagen_usuario"] != ""){

				$(".previsualizarEditar").attr("src", respuesta["imagen_usuario"]);

			}else{

				$(".previsualizarEditar").attr("src", "vistas/img/usuarios/default/anonymous.png");

			}

		}

	});

})

/*=============================================
ACTIVAR USUARIO
=============================================*/
$(".tablas").on("click", ".btnActivar", function(){

	var idUsuario = $(this).attr("idUsuario");
	var estadoUsuario = $(this).attr("estadoUsuario");

	var datos = new FormData();
 	datos.append("activarId", idUsuario);
  	datos.append("activarUsuario", estadoUsuario);

  	$.ajax({

	  url:"ajax/usuarios.ajax.php",
	  method: "POST",
	  data: datos,
	  cache: false,
      contentType: false,
      processData: false,
      success: function(respuesta){

      		if(window.matchMedia("(max-width:767px)").matches){

	      		 swal({
			      title: "El usuario ha sido actualizado",
			      type: "success",
			      confirmButtonText: "¡Cerrar!"
			    }).then(function(result) {
			        if (result.value) {

			        	window.location = "usuarios";

			        }


				});

	      	}

      }

  	})

  	if(estadoUsuario == 0){

  		$(this).removeClass('btn-success');
  		$(this).addClass('btn-danger');
  		$(this).html('Desactivado');
  		$(this).attr('estadoUsuario',1);

  	}else{

  		$(this).addClass('btn-success');
  		$(this).removeClass('btn-danger');
  		$(this).html('Activado');
  		$(this).attr('estadoUsuario',0);

  	}

})

/*=============================================
REVISAR SI EL USUARIO YA ESTÁ REGISTRADO
=============================================*/

$("#nuevoUsuario").change(function(){

	$(".alert").remove();

	var usuario = $(this).val();

	var datos = new FormData();
	datos.append("validarUsuario", usuario);

	 $.ajax({
	    url:"ajax/usuarios.ajax.php",
	    method:"POST",
	    data: datos,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "json",
	    success:function(respuesta){
	    	
	    	if(respuesta){

	    		$("#nuevoUsuario").parent().after('<div class="alert alert-warning">Este usuario ya existe en la base de datos</div>');

	    		$("#nuevoUsuario").val("");

	    	}

	    }

	})
})


/*=============================================
REVISAR SI EL CORREO YA ESTÁ REGISTRADO
=============================================*/

$("#nuevoCorreo").change(function(){

	$(".alert").remove();

	var correo = $(this).val();

	var datosc = new FormData();
	datosc.append("validarCorreo", correo);

	 $.ajax({
	    url:"ajax/usuarios.ajax.php",
	    method:"POST",
	    data: datosc,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "json",
	    success:function(respuestaC){
	    	
	    	if(respuestaC){

	    		$("#nuevoCorreo").parent().after('<div class="alert alert-warning">Este Correo ya existe en la base de datos</div>');

	    		$("#nuevoCorreo").val("");

	    	}

	    }

	})
})

/*=============================================
ELIMINAR USUARIO
=============================================*/
$(".tablas").on("click", ".btnEliminarUsuario", function(){

  var idUsuario = $(this).attr("idUsuario");
  var fotoUsuario = $(this).attr("fotoUsuario");
  var usuario = $(this).attr("usuario");

  swal({
    title: '¿Está seguro de borrar el usuario?',
    text: "¡Si no lo está puede cancelar la accíón!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, borrar usuario!'
  }).then(function(result){

    if(result.value){

      window.location = "index.php?ruta=usuarios&idUsuario="+idUsuario+"&usuario="+usuario+"&fotoUsuario="+fotoUsuario;

    }

  })

})

/*=============================================
VALIDACION DE CREACION DE USUARIOS
=============================================*/

/*=============================================
VALIDACION DE CARACTERES NOMBRE
=============================================*/
$("#nuevoNombre").keypress(function(){
	
	var nuevoNombre = $(this).val();
	if (nuevoNombre.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoNombre").change(function(){
	$(".alert").remove();
	var nuevoNombre = $(this).val();
	if (nuevoNombre.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES APELLIDO
=============================================*/
$("#nuevoApellido").keypress(function(){
	
	var nuevoApellido = $(this).val();
	if (nuevoApellido.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El apellido no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoApellido").change(function(){
	$(".alert").remove();
	var nuevoApellido = $(this).val();
	if (nuevoApellido.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El apellido no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES USUARIO
=============================================*/
$("#nuevoUsuario").keypress(function(){

	var nuevoUsuario = $(this).val();
	if (nuevoUsuario.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El usuario no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoUsuario").change(function(){
	$(".alert").remove();
	var nuevoUsuario = $(this).val();
	if (nuevoUsuario.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El usuario no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES CORREO
=============================================*/
$("#nuevoCorreo").keypress(function(){
	
	var nuevoCorreo = $(this).val();
	if (nuevoCorreo.length > 70){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El correo no puede llevar mas de 70 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoCorreo").change(function(){
	$(".alert").remove();
	var nuevoCorreo = $(this).val();
	if (nuevoCorreo.length > 70){
		$(this).parent().after('<div class="alert alert-warning">El correo no puede llevar mas de 70 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES CONTRASEÑA
=============================================*/
$("#nuevoPassword").keypress(function(){
	
	var nuevoPassword = $(this).val();
	if (nuevoPassword.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La contraseña no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoPassword").change(function(){
	$(".alert").remove();
	var nuevoPassword = $(this).val();
	if (nuevoPassword.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El contraseña no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})


/*=============================================
VALIDACION DE EDICION DE USUARIOS
=============================================*/

/*=============================================
VALIDACION DE CARACTERES NOMBRE
=============================================*/
$("#editarNombre").keypress(function(){
	
	var editarNombre = $(this).val();
	if (editarNombre.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editarNombre").change(function(){
	$(".alert").remove();
	var editarNombre = $(this).val();
	if (editarNombre.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES APELLIDO
=============================================*/
$("#editarApellido").keypress(function(){
	
	var editarApellido = $(this).val();
	if (editarApellido.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El apellido no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editarApellido").change(function(){
	$(".alert").remove();
	var editarApellido = $(this).val();
	if (nueveditarApellidooNombre.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El apellido no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES USUARIO
=============================================*/
$("#editarUsuario").keypress(function(){

	var editarUsuario = $(this).val();
	if (editarUsuario.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El usuario no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editarUsuario").change(function(){
	$(".alert").remove();
	var editarUsuario = $(this).val();
	if (editarUsuario.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El usuario no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES CORREO
=============================================*/
$("#editarCorreo").keypress(function(){
	
	var editarCorreo = $(this).val();
	if (editarCorreo.length > 70){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El correo no puede llevar mas de 70 caracteres</div>');

		$(this).val("");
	}
})

$("#editarCorreo").change(function(){
	$(".alert").remove();
	var editarCorreo = $(this).val();
	if (editarCorreo.length > 70){
		$(this).parent().after('<div class="alert alert-warning">El correo no puede llevar mas de 70 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES CONTRASEÑA
=============================================*/
$("#editarPassword").keypress(function(){
	
	var editarPassword = $(this).val();
	if (editarPassword.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La contraseña no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editarPassword").change(function(){
	$(".alert").remove();
	var editarPassword = $(this).val();
	if (editarPassword.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El contraseña no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})






