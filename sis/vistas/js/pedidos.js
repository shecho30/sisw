/*EDITAR PEDIDO*/
$(".tablas tbody").on("click", ".btnEditarPedido", function(){

	var idPedido   = $(this).attr("idPedido");

  //console.log("id Pedido",idPedido);

  //alert(idPedido);

	var datos = new FormData();
  datos.append("idPedido", idPedido);

  $.ajax({
        url: 'ajax/pedidos.ajax.php',
        type: 'POST',
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(response)
        {
        
        $("#id_pedido").val(response["id_pedido"]);
        $("#editPedido").val(response["numero_pedido"]);
        $("#editCliente").val(response["id_cliente"]);
        $("#editCliente").html(response["nombre_repre_cliente"]);
        $("#editProducto").val(response["id_producto"]);
        $("#editProducto").html(response["nombre_producto"]);
        $("#editDate").val(response["fecha_creacion_pedido"]);
        $("#editCantidad").val(response["cantidad_pedido"]);
        }
      })
     
	  

})

/*ELIMINAR PEDIDO*/

$(".tablas").on("click", "#btnEliminarPedido", function(){

  var idPedido = $(this).attr("idPedido");
  
  swal({
        title: '¿Está seguro de borrar el pedido?',
        text: "¡Si no lo está puede cancelar la acción!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar pedido'
      }).then(function(result){
        if (result.value) {
          
            window.location = "index.php?ruta=crear-pedido&idPedido="+idPedido;
        }

  })

})
