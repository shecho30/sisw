$(function(){
    $(".tablas").on("click", ".btnEditarFormaPago", function(){
        var idCategoria = $(this).attr("idFormaPago");

        var datos = new FormData();
        datos.append("id_forma_pago",idCategoria);

        $.ajax(
            {
                url:"ajax/forma.pago.ajax.php",
                method: "post",
                data: datos,
                cache: false,
                contentType: false,
                processData: false,
                dataType:"json",
                success: function(respuesta)
                {
                    $("#editarFormaPago").val(respuesta["nombre_forma_pago"]);
                    $("#idFormaPago").val(respuesta["id_forma_pago"]);
                }
            }
        )
        
    });
});