
/*=============================================
SUBIENDO LA FOTO DEL PROMOCION
=============================================*/
$(".fotoPro").change(function(){

	var imagen = this.files[0];
	
	/*=============================================
  	VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
  	=============================================*/

  	if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

  		$(".fotoPro").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen debe estar en formato JPG o PNG!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else if(imagen["size"] > 2000000){

  		$(".fotoPro").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen no debe pesar más de 2MB!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else{

  		var datosImagen = new FileReader;
  		datosImagen.readAsDataURL(imagen);

  		$(datosImagen).on("load", function(event){

  			var rutaImagen = event.target.result;

  			$(".mostrarFoto").attr("src", rutaImagen);

  		})

  	}
})

/*=============================================
ELIMINAR PROMOCION
=============================================*/
$(".tablas").on("click", ".btnEliminarPromocion", function()
{

	var idPromocion = $(this).attr("idPromocion");
	var fotoPromocion = $(this).attr("fotoPromocion");
	var nombrePromo = $(this).attr("nombrePromocion");
  
	swal({
	  title: '¿Está seguro de borrar la promoción?',
	  text: "¡Si no lo está puede cancelar la accíón!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Si, borrar usuario!'
	}).then(function(result){
  
	  if(result.value){
  
		window.location = "index.php?ruta=promocion&idPromocion="+idPromocion+"&fotoPromocion="+fotoPromocion+"&nombrePromocion="+nombrePromo;
  
	  }
  
	})
  
  })

  
/*=============================================
EDITAR PROMOCION
=============================================*/
$(".tablas").on("click", ".btnEditarPromocion", function(){

	var idPromocion = $(this).attr("idPromocion");
	//alert(idUsuario);
	var datos = new FormData();
	datos.append("idPromocion", idPromocion);

	$.ajax({

		url:"ajax/promocion.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
			// alert(respuesta);
			$("#editPromocion").val(respuesta["id_promocion"]);
			$("#editPosicion").val(respuesta["posicion_promocion"]);
			$("#posicionActual").val(respuesta["posicion_promocion"]);
			$("#editNombre").val(respuesta["nombre_promocion"]);
			$("#EditEnlace").val(respuesta["enlace_promocion"]);
			$("#EditFoto").val(respuesta["imagen_promocion"]);
			$("#fotoActualPromo").val(respuesta["imagen_promocion"]);


			if(respuesta["imagen_promocion"] != ""){

				$(".mostrarFotoEdit").attr("src", respuesta["imagen_promocion"]);

			}else{

				$(".mostrarFotoEdit").attr("src", "vistas/img/usuarios/default/anonymous.png");

			}

		}

	});

})


// /*=============================================
// REVISAR SI LA POSICION YA ESTÁ REGISTRADA
// =============================================*/

// $("#nuevoPosicion").change(function(){

// 	$(".alert").remove();
	

// 	var promocion = $(this).val();

// 	var datos = new FormData();
// 	datos.append("validarPromo", promocion);

// 	 $.ajax({
// 	    url:"ajax/promocion.ajax.php",
// 	    method:"POST",
// 	    data: datos,
// 	    cache: false,
// 	    contentType: false,
// 	    processData: false,
// 	    dataType: "json",
// 	    success:function(respuesta){
// 			// alert(respuesta)
	    	
// 	    	if(respuesta){

// 	    		$("#nuevoPosicion").parent().after('<div class="alert alert-warning">Esta posicion ya existe en la base de datos</div>');

// 	    		$("#nuevoPosicion").val("");

// 	    	}

// 	    }

// 	})
// })

// /*=============================================
// REVISAR SI LA POSICION YA ESTÁ REGISTRADA EDITAR
// =============================================*/

// $("#editPosicion").change(function(){

// 	$(".alert").remove();
	
// 	var posicionActual = $("#posicionActual").val();
// 	var promocion = $(this).val();
	
// 	var bool = (posicionActual - promocion == 0) ? true : false;
	



// 	if(!bool)
// 	{

// 		// var promocion = $(this).val();

// 		var datos = new FormData();
// 		datos.append("validarPromo", promocion);

// 		$.ajax({
// 			url:"ajax/promocion.ajax.php",
// 			method:"POST",
// 			data: datos,
// 			cache: false,
// 			contentType: false,
// 			processData: false,
// 			dataType: "json",
// 			success:function(respuesta){
// 				// alert(respuesta)
				
// 				if(respuesta){

// 					$("#editPosicion").parent().after('<div class="alert alert-warning">Esta posicion ya existe en la base de datos</div>');

// 					$("#editPosicion").val("");

// 				}

// 			}

// 		})
// 	}
// })