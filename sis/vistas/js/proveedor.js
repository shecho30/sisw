/*=============================================
EDITAR USUARIO
=============================================*/
$(".tablas").on("click", ".btnEditarProveedor", function(){

	var idUsuario = $(this).attr("idProveedor");
	//alert(idUsuario);
	var datos = new FormData();
	datos.append("id_proveedor", idUsuario);

	$.ajax({

		url:"ajax/proveedores.ajax.php",
		method: "POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			
		//alert(respuesta);
			$("#idProveedor").val(respuesta["id_proveedor"]);
			$("#nuevoTiDocuEdit").val(respuesta["id_tipo_documento"]);
			$("#nuevoNuDocuEdit").val(respuesta["documento_proveedor"]);
			$("#nuevoNombreEmpEdit").val(respuesta["empresa_proveedor"]);
			$("#nuevoNombreEdit").val(respuesta["nombre_repre_proveedor"]);
			$("#nuevoApellidoEdit").val(respuesta["apellido_repre_proveedor"]);
			$("#nuevoCorreoEdit").val(respuesta["correo_proveedor"]);
			$("#nuevoTelefonoEdit").val(respuesta["telefono_proveedor"]);
			$("#nuevoDirEdit").val(respuesta["direccion_proveedor"]);
			$("#nuevoCiudadEdit").val(respuesta["id_ciudad"]);
			$("#nuevoProEdit").val(respuesta["id_producto"]);
			$("#nuevaFotoEdit").val(respuesta["imagen_proveedor"]);

			
			// $("#editarPassword").val(respuesta["contrasena_usuario"]);

			// if(respuesta["foto"] != ""){

			// 	$(".previsualizar").attr("src", respuesta["foto"]);

			// }

		}

	});

})


/*=============================================
ELIMINAR USUARIO
=============================================*/
$(".tablas").on("click", ".btnEliminarProveedor", function(){

    var id_proveedor = $(this).attr("idProveedor");
    // var fotoUsuario = $(this).attr("fotoUsuario");
    // var usuario = $(this).attr("usuario");
  
    swal({
      title: '¿Está seguro de borrar el usuario?',
      text: "¡Si no lo está puede cancelar la accíón!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar usuario!'
    }).then(function(result){
  
      if(result.value){
  
        window.location = "index.php?ruta=proveedores&idProveedor="+id_proveedor;/*+"&usuario="+usuario+"&fotoUsuario="+fotoUsuario;*/
  
      }
  
    })
  
  })
  
  