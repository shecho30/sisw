$(document).ready(function($) {
  
  /* EDITAR ESTADO PRODUCTO */
  $('.tablaEProductos tbody').on('click', '.btnEditarProducto',function(){
    var id = $(this).data('id');
    $.ajax({
      url: 'ajax/estado-productos.ajax.php',
      type: 'POST',
      data: {'idEstadoProducto' : id},
    })
    .done(function(data) {
      let res = $.parseJSON(data);
      $("#idEProducto").val(`${res.id_estado_producto}`);
      $("#nuevoEstadoProducto").val(`${res.nombre_estado_producto}`);
    })
    .fail(function() {
      console.log("error");
    })
  });

  /* ELIMINAR ESTADO PRODUCTO */
    $('.tablaEProductos tbody').on('click', '.btnEliminarProducto',function(e){
      e.preventDefault();
    var id = $(this).data('id');
     swal({
        title: '¿Está seguro de borrar el estado?',
        text: "¡Si no lo está puede cancelar la acción!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar estado!'
       }).then(function(result){
        if(result.value){

           $.ajax({
                url: 'ajax/estado-productos.ajax.php',
                type: 'POST',
                data: {'idEstadoProductoEliminar' : id},
              })
              .done(function(data) {
                
                let res = $.parseJSON(data);
                

                if(res == "ok"){
                  swal({
                    type: "success",
                    title: "¡El estado ha sido eliminado!",
                    showConfirmButton: true,
                    confirmButtonText: "Cerrar"
                    }).then(function(result){
                    if (result.value) {
                    window.location = "estado-producto";
                    }
                  })
                  }else if(res == "error"){
                    swal({
                      type: "info",
                      title: "¡Es posible que exista una relacion con este estado, verifique!",
                      showConfirmButton: true,
                      confirmButtonText: "Cerrar"
                      }).then(function(result){
                      if (result.value) {
                      window.location = "estado-producto";
                      }
                    })
                  }
                }).
              fail(function() {
                console.log("error");
              }); 
      
      }
      });
     });
   
});

