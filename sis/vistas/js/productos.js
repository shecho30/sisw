/*=============================================
CARGAR LA TABLA DINÁMICA DE PRODUCTOS
=============================================*/

 /*$.ajax({

	url: "ajax/datatable-productos.ajax.php",
	success:function(respuesta){
		
		console.log("respuesta", respuesta);

	}

 })*/

var tablep = $('.listProductos').DataTable({
  //"ajax": "ajax/datatable-productos.ajax.php",
  "deferRender": true,
	"retrieve": true,
	"processing": true,
	 "language": {

			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}

	}
  });



/*=============================================
CAPTURANDO LA CATEGORIA PARA ASIGNAR CÓDIGO
=============================================*/
/*$("#nuevaCategoria").change(function(){

	var idCategoria = $(this).val();

	var datos = new FormData();
  	datos.append("idCategoria", idCategoria);

  	$.ajax({

      url:"ajax/productos.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(respuesta){

      	if(!respuesta){

      		var nuevoCodigo = idCategoria+"01";
      		$("#nuevoCodigo").val(nuevoCodigo);

      	}else{

      		var nuevoCodigo = Number(respuesta["codigo"]) + 1;
          	$("#nuevoCodigo").val(nuevoCodigo);

      	}
                
      }

  	})

})*/
/*=============================================
AJAX PRODUCTO
=============================================*/

var validarusuario;

 $.get('ajax/productos.ajax.php/ajaxGetProductos',{'loadProducto' : true}, function(res) {
    let data = $.parseJSON(res);
        tablep.clear().draw();
        for (var i = 0; i < data.length; i++) {
     
          var rowNode = tablep.row.add([
                data[i].id_producto,
                data[i].nombre_producto,
                data[i].codigo_barra_producto,
                (data[i].cantidad_producto <= 10 ? `<button class='btn btn-danger'>${data[i].cantidad_producto}</button>` :
                    (data[i].cantidad_producto > 11 && data[i].cantidad_producto <= 15 ? `<button class='btn btn-warning'>${data[i].cantidad_producto}</button>` : 
                      `<button class='btn btn-success'>${data[i].cantidad_producto}</button>`)),
                // data[i].precio_compra_producto,
                data[i].precio_salida_producto,
                // data[i].unidad_medicion_producto,
                // data[i].presentacion_producto,
                // data[i].fecha_creacion_producto,
                (data[i].imagen_producto == "" || data[i].imagen_producto === null ?  `<img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail" width="40px">` : `<img src="${data[i].imagen_producto}" class="img-thumbnail" width="40px">`),
                // data[i].description_producto,
                data[i].nombre_categoria,
                data[i].nombre_estado_producto,
                `<div class='btn-group'>
                  <button class='btn btn-warning btnEditarProducto' idProducto='${data[i].id_producto}' data-toggle='modal' data-target='#modalEditarProducto'><i class='fa fa-pencil'></i></button>
                  <button class='btn btn-danger btnEliminarProducto' idProducto='${data[i].id_producto}' codigo='${data[i].codigo_barra_producto}' imagen='${data[i].imagen_producto}'><i class='fa fa-times'></i></button>
                  </div>`
                ]).draw().node();
        }
    });



/*=============================================
AGREGANDO PRECIO DE VENTA
=============================================*/
$("#nuevoPrecioCompra, #editar_precioCompra").change(function(){

	if($(".porcentaje").prop("checked")){

		var valorPorcentaje = $(".nuevoPorcentaje").val();
		
		var porcentaje = Number(($("#nuevoPrecioCompra").val()*valorPorcentaje/100))+Number($("#nuevoPrecioCompra").val());

		var editarPorcentaje = Number(($("#editar_precioCompra").val()*valorPorcentaje/100))+Number($("#editar_precioCompra").val());

		$("#nuevoPrecioVenta").val(porcentaje);
		$("#nuevoPrecioVenta").prop("readonly",true);

		$("#editar_precioVenta").val(editarPorcentaje);
		$("#editar_precioVenta").prop("readonly",true);

	}

})

/*=============================================
CAMBIO DE PORCENTAJE
=============================================*/
$(".nuevoPorcentaje").change(function(e){
  e.preventDefault();

	if($(".porcentaje").prop("checked")){

		var valorPorcentaje = $(this).val();
		
		var porcentaje = Number(($("#nuevoPrecioCompra").val()*valorPorcentaje/100))+Number($("#nuevoPrecioCompra").val());

		var editarPorcentaje = Number(($("#editarPrecioCompra").val()*valorPorcentaje/100))+Number($("#editarPrecioCompra").val());

		$("#nuevoPrecioVenta").val(porcentaje);
		$("#nuevoPrecioVenta").prop("readonly",true);

		$("#editarPrecioVenta").val(editarPorcentaje);
		$("#editarPrecioVenta").prop("readonly",true);

	}

})

$(".porcentaje").on("ifUnchecked",function(){

	$("#nuevoPrecioVenta").prop("readonly",false);
	$("#editarPrecioVenta").prop("readonly",false);

})

$(".porcentaje").on("ifChecked",function(){

	$("#nuevoPrecioVenta").prop("readonly",true);
	$("#editarPrecioVenta").prop("readonly",true);

})

/*=============================================
SUBIENDO LA FOTO DEL PRODUCTO
=============================================*/

$(".nuevaImagen").change(function(e){
  e.preventDefault();

	var imagen = this.files[0];
	
	/*=============================================
  	VALIDAMOS EL FORMATO DE LA IMAGEN SEA JPG O PNG
  	=============================================*/

  	if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){

  		$(".nuevaImagen").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen debe estar en formato JPG o PNG!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else if(imagen["size"] > 2000000){

  		$(".nuevaImagen").val("");

  		 swal({
		      title: "Error al subir la imagen",
		      text: "¡La imagen no debe pesar más de 2MB!",
		      type: "error",
		      confirmButtonText: "¡Cerrar!"
		    });

  	}else{

  		var datosImagen = new FileReader;
  		datosImagen.readAsDataURL(imagen);

  		$(datosImagen).on("load", function(event){

  			var rutaImagen = event.target.result;

  			$(".previsualizar").attr("src", rutaImagen);

  		})

  	}
})

/*=============================================
EDITAR PRODUCTO
=============================================*/

$(".listProductos tbody").on("click", "button.btnEditarProducto", function(e){
  e.preventDefault();
	var idProducto = $(this).attr("idProducto");
	
	var datos = new FormData();
    datos.append("idProducto", idProducto);

     $.ajax({

      url:"ajax/productos.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(respuesta){
          $("#id_producto").val(respuesta.id_producto);
          $("#editar_nombre").val(respuesta.nombre_producto);
          $("#editar_codigo").val(respuesta.codigo_barra_producto);          
          $("#editar_stock").val(respuesta.cantidad_producto);
          $("#editar_precioCompra").val(respuesta.precio_compra_producto);
          $("#editar_precioVenta").val(respuesta.precio_salida_producto);
          $("#editar_unidadMedicion").val(respuesta.unidad_medicion_producto);
          $("#editar_presentacion").val(respuesta.presentacion_producto);
          $("#editar_descripcion").val(respuesta.description_producto);
          $("#editarCategoria").val(respuesta.id_categoria);
          $("#editarCategoria").html(respuesta.nombre_categoria);   
          $("#editarEstado").val(respuesta.id_estado_producto);
          $("#editarEstado").html(respuesta.nombre_estado_producto);        

          if(respuesta.imagen_producto != ""){
           	$("#imagenActual").val(respuesta.imagen_producto);
           	$(".previsualizar").attr("src",  respuesta.imagen_producto);
          }
      }

  })

})

/*=============================================
ELIMINAR PRODUCTO
=============================================*/

$(".listProductos tbody").on("click", "button.btnEliminarProducto", function(e){
  e.preventDefault();


	var idProducto = $(this).attr("idProducto");
	var codigo = $(this).attr("codigo");
	var imagen = $(this).attr("imagen");
	
	swal({

		title: '¿Está seguro de borrar el producto?',
		text: "¡Si no lo está puede cancelar la accíón!",
		type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar producto!'
        }).then(function(result){
        if (result.value) {

        	window.location = "index.php?ruta=productos&idProducto="+idProducto+"&imagen="+imagen+"&codigo="+codigo;

        }


	})

})
	

/*=============================================
VALIDACION DE CREACION DE PRODUCTOS
=============================================*/

/*=============================================
VALIDACION DE CARACTERES NOMBRE
=============================================*/
$("#nuevoNombre").keypress(function(){
	
	var nuevoNombre = $(this).val();
	if (nuevoNombre.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoNombre").change(function(){
	$(".alert").remove();
	var nuevoNombre = $(this).val();
	if (nuevoNombre.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})



/*=============================================
VALIDACION DE CARACTERES CODIGO
=============================================*/
$("#nuevoCodigo").keypress(function(){
	
	var nuevoCodigo = $(this).val();
	if (nuevoCodigo.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El codigo no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoCodigo").change(function(){
	$(".alert").remove();
	var nuevoCodigo = $(this).val();
	if (nuevoCodigo.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El codigo no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})


/*=============================================
VALIDACION DE CARACTERES UNIDAD MEDIDA
=============================================*/
$("#nuevoUnidad").keypress(function(){
	
	var nuevoUnidad = $(this).val();
	if (nuevoUnidad.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La unidad no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoUnidad").change(function(){
	$(".alert").remove();
	var nuevoUnidad = $(this).val();
	if (nuevoUnidad.length > 49){
		$(this).parent().after('<div class="alert alert-warning">La unidad no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES PRESENTACION
=============================================*/
$("#nuevoPresentacion").keypress(function(){
	
	var nuevoPresentacion = $(this).val();
	if (nuevoPresentacion.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La presentacion no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoPresentacion").change(function(){
	$(".alert").remove();
	var nuevoPresentacion = $(this).val();
	if (nuevoPresentacion.length > 49){
		$(this).parent().after('<div class="alert alert-warning">La presentacion no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})


/*=============================================
VALIDACION DE CARACTERES DESCRIPCION
=============================================*/
$("#nuevoDescripcion").keypress(function(){
	
	var nuevoDescripcion = $(this).val();
	if (nuevoDescripcion.length > 250){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La descripcion no puede llevar mas de 250 caracteres</div>');

		$(this).val("");
	}
})

$("#nuevoDescripcion").change(function(){
	$(".alert").remove();
	var nuevoDescripcion = $(this).val();
	if (nuevoDescripcion.length > 250){
		$(this).parent().after('<div class="alert alert-warning">La descripcion no puede llevar mas de 250 caracteres</div>');

		$(this).val("");
	}
})



/*=============================================
VALIDACION DE EDICION DE PRODUCTOS
=============================================*/

/*=============================================
VALIDACION DE CARACTERES NOMBRE
=============================================*/
$("#editar_nombre").keypress(function(){
	
	var editar_nombre = $(this).val();
	if (editar_nombre.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editar_nombre").change(function(){
	$(".alert").remove();
	var editar_nombre = $(this).val();
	if (editar_nombre.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})



/*=============================================
VALIDACION DE CARACTERES UNIDAD MEDIDA
=============================================*/
$("#editar_unidadMedicion").keypress(function(){
	
	var editar_unidadMedicion = $(this).val();
	if (editar_unidadMedicion.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La unidad no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editar_unidadMedicion").change(function(){
	$(".alert").remove();
	var editar_unidadMedicion = $(this).val();
	if (editar_unidadMedicion.length > 49){
		$(this).parent().after('<div class="alert alert-warning">La unidad no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES PRESENTACION
=============================================*/
$("#editar_presentacion").keypress(function(){
	
	var editar_presentacion = $(this).val();
	if (editar_presentacion.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La presentacion no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editar_presentacion").change(function(){
	$(".alert").remove();
	var editar_presentacion = $(this).val();
	if (editar_presentacion.length > 49){
		$(this).parent().after('<div class="alert alert-warning">La presentacion no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})


/*=============================================
VALIDACION DE CARACTERES DESCRIPCION
=============================================*/
$("#editar_descripcion").keypress(function(){
	
	var editar_descripcion = $(this).val();
	if (editar_descripcion.length > 250){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La descripcion no puede llevar mas de 250 caracteres</div>');

		$(this).val("");
	}
})

$("#editar_descripcion").change(function(){
	$(".alert").remove();
	var editar_descripcion = $(this).val();
	if (editar_descripcion.length > 250){
		$(this).parent().after('<div class="alert alert-warning">La descripcion no puede llevar mas de 250 caracteres</div>');

		$(this).val("");
	}
})