/*=============================================
EDITAR CLIENTE
=============================================*/



$(".btnEditarCliente").click(function(){

  var idCliente   = $(this).attr("idCliente");
  //alert(idCliente);

  console.log("id Cliente",idCliente);

	var datos = new FormData();
  datos.append("idCliente", idCliente);

    $.ajax({

      url:"ajax/clientes.ajax.php",
      method: "POST",
      data: datos,
      cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success:function(respuesta){

        console.log(respuesta);
      
       $("#id_Cliente").val(respuesta["id_cliente"]);
    	 $("#editDocument").val(respuesta["document_cliente"]);
       $("#editTypeDocument").html(respuesta["nombre_tipo_documento"]);
       $("#editTypeDocument").val(respuesta["id_tipo_documento"]);
       $("#editCompanyCustomer").val(respuesta["empresa_cliente"]);
       $("#editName").val(respuesta["nombre_repre_cliente"]);
       $("#editLastName").val(respuesta["apellido_repre_cliente"]);
       $("#editPhone").val(respuesta["telefono_cliente"]);
       $("#editAddress").val(respuesta["direccion_cliente"]);
       $("#editEmail").val(respuesta["correo_cliente"]);
       $("#passwordActual").val(respuesta["contrasena_cliente"]);
       $("#editState").html(respuesta["nombre_estado_usuario"]);
       $("#editState").val(respuesta["id_estado_usuario"]);
       $("#editCity").html(respuesta["nombre_ciudad"]);
       $("#editCity").val(respuesta["id_ciudad"]);
	  }

  	})

})

/*=============================================
ELIMINAR CLIENTE
=============================================*/
$(".tablas").on("click", ".btnEliminarCliente", function(){

	var idCliente = $(this).attr("idCliente");
	
	swal({
        title: '¿Está seguro de borrar el cliente?',
        text: "¡Si no lo está puede cancelar la acción!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, borrar cliente!'
      }).then(function(result){
        if (result.value) {
          
            window.location = "index.php?ruta=clientes&idCliente="+idCliente;
        }

  })

})

/*=============================================
ACTIVAR CLIENTE
=============================================*/
$(".tablas").on("click", ".btnNew", function(){


  var id_cliente     = $(this).attr("id_cliente");
  var estado_cliente = $(this).attr("estado_cliente");

  var datos = new FormData();
  datos.append('activarId', id_cliente);
  datos.append('activarCliente', estado_cliente);

  $.ajax({
    url: 'ajax/clientes.ajax.php',
    type: 'POST',
    data: datos,
    cache: false,
    contentType: false,
    processData: false
    
  })
  .done(function(respuesta) {
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  });
  
  if (estado_cliente == 2) {
    $(this).removeClass('btn-success');
    $(this).addClass('btn-danger');
    $(this).html('inactivo');
    $(this).attr('estado_cliente', 3);
  }else if(estado_cliente == 3){
    $(this).removeClass('btn-danger');
    $(this).addClass('btn-warning');
    $(this).html('bloqueado');
    $(this).attr('estado_cliente', 1);
  }else if(estado_cliente == 1){
    $(this).removeClass('btn-warning');
    $(this).addClass('btn-success');
    $(this).html('activo');
    $(this).attr('estado_cliente', 2);
    
  }


});


/*=============================================
REVISAR SI EL CORREO YA ESTÁ REGISTRADO
=============================================*/

$("#newEmail").change(function(){

	$(".alert").remove();

	var correoU = $(this).val();

	var datoscU = new FormData();
	datoscU.append("validarCorreoU", correoU);

	 $.ajax({
	    url:"ajax/clientes.ajax.php",
	    method:"POST",
	    data: datoscU,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "json",
	    success:function(respuestaCu){
	    	
	    	if(respuestaCu){

	    		$("#newEmail").parent().after('<div class="alert alert-warning">Este Correo ya existe en la base de datos</div>');

	    		$("#newEmail").val("");

	    	}

	    }

	})
})


/*=============================================
REVISAR SI EL DOCUMENTO YA ESTÁ REGISTRADO
=============================================*/

$("#newDocument").change(function(){

	$(".alert").remove();

	var doc = $(this).val();

	var datosd = new FormData();
	datosd.append("validarDocumento", doc);

	 $.ajax({
	    url:"ajax/clientes.ajax.php",
	    method:"POST",
	    data: datosd,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "json",
	    success:function(respuestad){
	    	
	    	if(respuestad){

	    		$("#newDocument").parent().after('<div class="alert alert-warning">Este Documento ya existe en la base de datos</div>');

	    		$("#newDocument").val("");

	    	}

	    }

	})
})

/*=============================================
VALIDACION DE CREACION DE CLIENTES
=============================================*/

/*=============================================
VALIDACION DE CARACTERES DEL DOCUMENTO
=============================================*/
$("#newDocument").keypress(function(){
	
	var newDocument = $(this).val();
	if (newDocument.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El documento no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#newDocument").change(function(){
	$(".alert").remove();
	var newDocument = $(this).val();
	if (newDocument.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El documento no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})



/*=============================================
VALIDACION DE CARACTERES NOMBRE EMPRESA
=============================================*/
$("#newCompanyCustomer").keypress(function(){
	
	var newCompanyCustomer = $(this).val();
	if (newCompanyCustomer.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El nombre de la empresa no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#newCompanyCustomer").change(function(){
	$(".alert").remove();
	var newCompanyCustomer = $(this).val();
	if (newCompanyCustomer.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El nombre de la empresa no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})


/*=============================================
VALIDACION DE CARACTERES NOMBRE
=============================================*/
$("#newName").keypress(function(){
	
	var newName = $(this).val();
	if (newName.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#newName").change(function(){
	$(".alert").remove();
	var newName = $(this).val();
	if (newName.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES APELLIDO
=============================================*/
$("#newLastName").keypress(function(){
	
	var newLastName = $(this).val();
	if (newLastName.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El apellido no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#newLastName").change(function(){
	$(".alert").remove();
	var newLastName = $(this).val();
	if (newLastName.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El apellido no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES TELEFONO
=============================================*/
$("#newPhone").keypress(function(){

	var newPhone = $(this).val();
	if (newPhone.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El Telefono no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#newPhone").change(function(){
	$(".alert").remove();
	var newPhone = $(this).val();
	if (newPhone.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El Telefono no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES DIRECCION
=============================================*/
$("#newAddress").keypress(function(){

	var newAddress = $(this).val();
	if (newAddress.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La direccion no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#newAddress").change(function(){
	$(".alert").remove();
	var newAddress = $(this).val();
	if (newAddress.length > 49){
		$(this).parent().after('<div class="alert alert-warning">La direccion no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES CORREO
=============================================*/
$("#newEmail").keypress(function(){
	
	var newEmail = $(this).val();
	if (newEmail.length > 70){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El correo no puede llevar mas de 70 caracteres</div>');

		$(this).val("");
	}
})

$("#newEmail").change(function(){
	$(".alert").remove();
	var newEmail = $(this).val();
	if (newEmail.length > 70){
		$(this).parent().after('<div class="alert alert-warning">El correo no puede llevar mas de 70 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES CONTRASEÑA
=============================================*/
$("#newPassword").keypress(function(){
	
	var newPassword = $(this).val();
	if (newPassword.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La contraseña no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#newPassword").change(function(){
	$(".alert").remove();
	var newPassword = $(this).val();
	if (newPassword.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El contraseña no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})


/*=============================================
VALIDACION DE EDICION DE CLIENTES
=============================================*/

/*=============================================
VALIDACION DE CARACTERES DEL DOCUMENTO
=============================================*/
$("#editDocument").keypress(function(){
	
	var editDocument = $(this).val();
	if (editDocument.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El documento no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editDocument").change(function(){
	$(".alert").remove();
	var editDocument = $(this).val();
	if (editDocument.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El documento no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})



/*=============================================
VALIDACION DE CARACTERES NOMBRE EMPRESA
=============================================*/
$("#editCompanyCustomer").keypress(function(){
	
	var editCompanyCustomer = $(this).val();
	if (editCompanyCustomer.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El nombre de la empresa no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editCompanyCustomer").change(function(){
	$(".alert").remove();
	var editCompanyCustomer = $(this).val();
	if (editCompanyCustomer.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El nombre de la empresa no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})


/*=============================================
VALIDACION DE CARACTERES NOMBRE
=============================================*/
$("#editName").keypress(function(){
	
	var editName = $(this).val();
	if (editName.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editName").change(function(){
	$(".alert").remove();
	var editName = $(this).val();
	if (editName.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El nombre no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES APELLIDO
=============================================*/
$("#editLastName").keypress(function(){
	
	var editLastName = $(this).val();
	if (editLastName.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El apellido no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editLastName").change(function(){
	$(".alert").remove();
	var editLastName = $(this).val();
	if (editLastName.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El apellido no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES TELEFONO
=============================================*/
$("#editPhone").keypress(function(){

	var editPhone = $(this).val();
	if (editPhone.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El Telefono no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editPhone").change(function(){
	$(".alert").remove();
	var editPhone = $(this).val();
	if (editPhone.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El Telefono no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES DIRECCION
=============================================*/
$("#editAddress").keypress(function(){

	var editAddress = $(this).val();
	if (editAddress.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La direccion no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editAddress").change(function(){
	$(".alert").remove();
	var editAddress = $(this).val();
	if (editAddress.length > 49){
		$(this).parent().after('<div class="alert alert-warning">La direccion no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

/*=============================================
VALIDACION DE CARACTERES CORREO
=============================================*/
$("#editEmail").keypress(function(){
	
	var editEmail = $(this).val();
	if (editEmail.length > 70){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">El correo no puede llevar mas de 70 caracteres</div>');

		$(this).val("");
	}
})

$("#editEmail").change(function(){
	$(".alert").remove();
	var editEmail = $(this).val();
	if (editEmail.length > 70){
		$(this).parent().after('<div class="alert alert-warning">El correo no puede llevar mas de 70 caracteres</div>');

		$(this).val("");
	}
})
/*=============================================
VALIDACION DE CARACTERES CONTRASEÑA
=============================================*/
$("#editPassword").keypress(function(){
	
	var editPassword = $(this).val();
	if (editPassword.length > 49){
		$(".alert").remove();
		$(this).parent().after('<div class="alert alert-warning">La contraseña no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})

$("#editPassword").change(function(){
	$(".alert").remove();
	var editPassword = $(this).val();
	if (editPassword.length > 49){
		$(this).parent().after('<div class="alert alert-warning">El contraseña no puede llevar mas de 50 caracteres</div>');

		$(this).val("");
	}
})