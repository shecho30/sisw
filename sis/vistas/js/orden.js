$(function(){

    $("#tabla_producto").DataTable();
    // $("#table_view").DataTable();


    $(".tablas").on("click", ".btnAgregarProductos", function(){
        var idOrden = $(this).attr("idOrden");
        $("#id_orden_insert").val(idOrden);

    });


    $("#tabla_producto").on("click", ".insert_pro", function()
    {
        var id_producto = $(this).attr("id_producto");
        $("#id_orden_producto_insert").val(id_producto);

        var id_orden =  $("#id_orden_insert").val();
        $("#id_orden_compra_insert").val(id_orden);

    });

    $(".tablas").on("click",".view_orden",function(){
        var idOrden = $(this).attr("idOrden");

        var datos = new FormData();
        datos.append("id_orden", idOrden);
        $("#tabla_orden_view").empty();
        $("#nombreOrden").empty();
        $("#nombreProv").empty();
        $("#TotalPagar").empty();


        $.ajax({
            url: "ajax/orden.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response)
            {

                if(response.length > 0)
                {
                    var total = 0;
                    $(".ocultarPdf").show();

                    //alert(response);
                    $("#idOrdenPdf").val(response[0]["id_orden"]);
                    $("#nombreOrden").append("Nombre Orden: <u>"+response[0]["nombre_orden"]+"</u>");
                    $("#nombreProv").append("Nombre Proveedor: <u>"+response[0]["nombre_proveedor"]+"</u>");

                         $.each(response, function (index, value) {
                             $("#tabla_orden_view").append(
                                 "<tr>"+
                                     "<td> "+ value["nombre_producto"] +" </td>" +
                                     "<td> "+ "$"+new Intl.NumberFormat().format(value["precio"]) +" </td>" +
                                     "<td> "+ value["cantidad"] +" </td>" +
                                     "<td> "+ "$"+new Intl.NumberFormat().format(value["total"]) +" </td>" +
                                     "<td><div class='btn-group'>"+
                                        "<button class='btn btn-warning _editar' id='" + value["id"] + "' data-toggle='modal' data-target='#modal_editar'><i class='fa fa-edit'></i></button>"+
                                        "<button class='btn btn-danger _eliminar' id='" + value["id"] + "'><i class='fa fa-times'></i></button>"+
                                     "</div></td>"
                             +"</tr>");
                             total = total + parseInt(value["total"]);
                         });

                         $("#TotalPagar").append("Monto a Pagar: <u>$"+new Intl.NumberFormat().format(total)+"</u>");
                }
                else
                {
                    $("#nombreOrden").append("<p style='color:red;'>Debe agregar Productos a la orden</p>");
                    $(".ocultarPdf").hide();

                }

            }
        })

    });


    $(".tablas").on("click",".btnEliminarOrden",function(){
        var id_orden = $(this).attr("idOrden");

        swal({
            title: '¿Está seguro de borrar la Orden?',
            text: "¡Si no lo está puede cancelar la accíón!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si, borrar Orden!'
          }).then(function(result){

            if(result.value){

              window.location = "index.php?ruta=orden&idOrden="+id_orden;/*+"&usuario="+usuario+"&fotoUsuario="+fotoUsuario;*/

            }

          })

    });

    $(".tablas").on("click",".btnEditarOrden",function(){
        var id_orden = $(this).attr("idOrden");

        var datos = new FormData();
        datos.append("id_edit_orden", id_orden);
        // $("#tabla_orden_view").empty();
        // $("#nombreOrden").empty();
        // $("#nombreProv").empty();
        // $("#TotalPagar").empty();


        $.ajax({
            url: "ajax/orden.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response)
            {
                // alert(response);
                $("#nuevo_id_edit").val(response["id_orden_compra"]);
                $("#nueva_orden_edit").val(response["nombre_orden_compra"]);
                $("#nuevo_proveedor_edit").val(response["id_proveedor"]);
                $("#edit_nuevo_detalle").val(response["detalle_orden"]);

            }
        })
    });

    $("#table_view").on("click" ,"._eliminar",function(){
        var id = $(this).attr("id");

        swal({
            title: '¿Está seguro de borrar el producto?',
            text: "¡Si no lo está puede cancelar la accíón!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Si, borrar Orden!'
          }).then(function(result){

            if(result.value){

              window.location = "index.php?ruta=orden&id_prod="+id;/*+"&usuario="+usuario+"&fotoUsuario="+fotoUsuario;*/

            }

          })
    });

    $("#descargarPdf").click(function(){
      var id_orden = $("#idOrdenPdf").val();
      var datos = new FormData();
      datos.append("idOrden", id_orden);

      window.open("extensiones/tcpdf/pdf/ordenCompra.php?idOrden="+id_orden);




    });


    $("#table_view").on("click" ,"._editar",function(){
        var id = $(this).attr("id");

        var datos = new FormData();
        datos.append("id_editar", id);
        // $("#tabla_orden_view").empty();
        // $("#nombreOrden").empty();
        // $("#nombreProv").empty();
        // $("#TotalPagar").empty();


        $.ajax({
            url: "ajax/orden.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (response)
            {
                //alert(response);
                 $("#_id").val(response["id_orden_producto"]);
                 $("#_nuevo_precio").val(response["precio_unidad"]);
                 $("#_nuevo_cantidad").val(response["cantidad_producto"]);

            }
        })
    });
});
