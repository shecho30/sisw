/*=============================================
EDITAR CATEGORIA
=============================================*/
$(".tablaCategorias").on("click", ".btnEditarCategoria", function(){

  var idCategoria = $(this).attr("idCategoria");

  //alert (idCategoria);
  var datos = new FormData();
  datos.append("idCategoria", idCategoria);

  $.ajax({
    url: "ajax/categorias.ajax.php",
    method: "POST",
        data: datos,
        cache: false,
      contentType: false,
      processData: false,
      dataType:"json",
      success: function(respuesta){
        $("#editarCategoria").val(respuesta["nombre_categoria"]);
        $("#editarDescipcion").val(respuesta["descripcion_categoria"]);
        $("#idCategoria").val(respuesta["id_categoria"]);
      }
  })
})

/*=============================================
ELIMINAR CATEGORIA
=============================================*/
$(".tablaCategorias").on("click", ".btnEliminarCategoria", function(){

   var idCategoria = $(this).attr("idCategoria");

   var datos = new FormData();
   datos.append("id", idCategoria);
  //alert(idCategoria);
   swal({
    title: '¿Está seguro de borrar la categoría?',
    text: "¡Si no lo está puede cancelar la acción!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: 'Cancelar',
    confirmButtonText: 'Si, borrar categoría!'
   }).then(function(result){

    if(result.value){

      
      $.ajax({
          url: "ajax/categorias.ajax.php",
          method: "POST",
          data: datos,
          cache: false,
          contentType: false,
          processData: false,
          dataType:"json",
        success: function(respuesta)
        {
          // alert(respuesta);
          if( respuesta)
          {
            swal({
						  type: "error",
						  title: "¡La categoria no se puede borrar por que tienen productos asociados¡",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "categorias";

							}
						})
          }else
          {
            window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;
          }
        }
      });

      //  window.location = "index.php?ruta=categorias&idCategoria="+idCategoria;

    }

   })

})