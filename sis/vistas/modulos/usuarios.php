<?php
if ($_SESSION["perfil"] != 1 || $_SESSION["perfil"] == "Vendedor") {
    echo '<script>
    window.location = "inicio";
  </script>';
    return;
}
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar usuarios</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar usuarios</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">
                    Agregar usuario
                </button>
            </div>

            <div class="box-body">
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:6px">Codigo</th>
                            <th>Imagen</th>
                            <th>Nombre</th>
                            <th>Usuario</th>
                            <th>Correo</th>
                            <th>Estado</th>
                            <th>Tipo Usuario</th>
                            <th>Acciones</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        $item = null;
                        $valor = null;
                        $usuarios = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);
                        //print_r($usuarios);
                        foreach ($usuarios as $valor) {
                        echo "<tr>";
                            echo "<td>".$valor["id_usuario"]."</td>";
                            if($valor["imagen_usuario"] != ""){

                                echo '<td><img src="'.$valor["imagen_usuario"].'" class="img-thumbnail" width="40px"></td>';
            
                              }else{
            
                                echo '<td><img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="40px"></td>';
            
                              }
                            echo "<td>".$valor["nombre_usuario"]."</td>";
                            echo "<td>".$valor["nick_user"]."</td>";
                            echo "<td>".$valor["correo_usuario"]."</td>";
                            /* Estado */
                            if($valor["id_estado_usuario"] == 1)
                                echo "<td>Activo</td>";
                            elseif($valor["id_estado_usuario"] == 2)
                                echo "<td>Inactivo</td>";
                            elseif($valor["id_estado_usuario"] == 3)
                                echo "<td>Bloqueado</td>";
                            //echo "<td>".$valor["id_tipo_usuario"]."</td>";
                            /*Rol*/
                            if($valor["id_tipo_usuario"] == 1)
                            echo "<td>Admin</td>";
                            elseif($valor["id_tipo_usuario"] == 2)
                                echo "<td>Analista</td>";
                            elseif($valor["id_tipo_usuario"] == 3)
                                echo "<td>Usuario</td>";
                                echo '<td>';

                                if ($_SESSION["perfil"] == 1 || $_SESSION["perfil"] == "Vendedor") {
                                    echo '
                                    
                                    <div class="btn-group">
                                        <button class="btn btn-warning btnEditarUsuario" idUsuario="' . $valor["id_usuario"] . '" data-toggle="modal" data-target="#modalEditarUsuario"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger btnEliminarUsuario" idUsuario="' . $valor["id_usuario"] . '" fotoUsuario="' . $valor["imagen_usuario"] . '" usuario="' . $valor["nick_user"] . '"><i class="fa fa-times"></i></button>
                                    </div> 
                                    
                                    
                                </td>';
                        echo "</tr>";       
                        }
                    }
                        ?> 
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- AGREGAR USUARIO-->

<div id="modalAgregarUsuario" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar usuario</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <!-- NOMBRE -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre" id="nuevoNombre" required>
                            </div>
                        </div>

                        <!--Apellido-->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span> 
                                <input type="text" class="form-control input-lg " name="nuevoApellido" placeholder="Ingresar Apellido" id="nuevoApellido" required>
                            </div>
                        </div>

                        <!-- USUARIO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingresar usuario" id="nuevoUsuario" required>
                            </div>
                        </div>

                         <!-- Correo -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoCorreo" placeholder="Ingresar Correo" id="nuevoCorreo" required>
                            </div>
                        </div>

                        <!-- CONTRASEÑA -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span> 
                                <input type="password" class="form-control input-lg" name="nuevoPassword" placeholder="Ingresar contraseña" id="nuevoPassword" required>
                            </div>
                        </div>

                          <!-- SELECCIONAR SU ESTADO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span> 
                                <select class="form-control input-lg" name="nuevoEstado" required>
                                    <option value="" disabled selected>Selecionar Estado</option>
                                    <option value="1">Activo</option>
                                    <option value="2">Inactivo</option>
                                    <option value="3">Bloqueado</option>
                                </select>
                            </div>
                        </div>

                        <!-- SELECCIONAR SU PERFIL -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-drivers-license"></i></span> 
                                <select class="form-control input-lg" name="nuevoPerfil" required>
                                    <option value="" disabled selected>Selecionar perfil</option>
                                    <option value="1">Administrador</option>
                                    <option value="2">Especial</option>
                                    <option value="3">Vendedor</option>
                                </select>
                            </div>
                        </div>

                        <!-- SUBIR FOTO -->

                        <div class="form-group">
                            <div class="panel">SUBIR FOTO</div>
                            <input type="file" class="nuevaFoto" name="nuevaFoto">
                            <p class="help-block">Peso máximo de la foto 2MB</p>
                            <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar usuario</button>
                </div>

                <?php
                $crearUsuario = new ControladorUsuarios();
                $crearUsuario->ctrCrearUsuario();
                ?>
            </form>
        </div>
    </div>
</div>

<!-- EDITAR USUARIO -->

<div id="modalEditarUsuario" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar usuario</h4>
                </div>


                <div class="modal-body">
                    <div class="box-body">
                        
                          <!-- id_usuario -->
                          <div class="form-group">
                            <div class="input-group">
                                <input type="hidden" class="form-control input-lg" id="id_usuario" name="id_usuario" value="" required>
                            </div>
                        </div>

                        <!-- NOMBRE -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" id="editarNombre" name="editarNombre" value="" required>
                            </div>
                        </div>

                          <!--Apellido-->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" name="editarApellido" id="editarApellido" placeholder="Ingresar Apellido" required>
                            </div>
                        </div>

                        <!-- USUARIO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span> 
                                <input type="text" class="form-control input-lg" id="editarUsuario" name="editarUsuario" value="" readonly>
                            </div>
                        </div>

                        <!-- Correo -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="email" class="form-control input-lg" name="editarCorreo" id="editarCorreo" placeholder="Ingresar Correo" required>
                            </div>
                        </div>

                        <!-- CONTRASEÑA -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span> 
                                <input type="password" class="form-control input-lg" name="editarPassword" id="editarPassword" placeholder="Escriba la nueva contraseña" required>
                                <input type="hidden" id="passwordActual" name="passwordActual">
                            </div>
                        </div>

                        <!-- SELECCIONAR SU ESTADO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span> 
                                <select class="form-control input-lg" name="editarEstado" id="editarEstado">
                                    <option value="">Selecionar Estado</option>
                                    <option value="1">Activo</option>
                                    <option value="2">Inactivo</option>
                                    <option value="3">Bloqueado</option>
                                </select>
                            </div>
                        </div>


                        <!-- SELECCIONAR SU PERFIL -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span> 
                                <select class="form-control input-lg" name="editarPerfil" id="editarPerfil">
                                    <option value="">Selecione Perfil</option>
                                    <option value="1">Administrador</option>
                                    <option value="2">Analista</option>
                                    <option value="3">Usuario</option>
                                </select>
                            </div>
                        </div>

                        <!-- SUBIR FOTO -->

                        <div class="form-group">
                            <div class="panel">SUBIR FOTO</div>
                            <input type="file" class="nuevaFoto" name="editarFoto">

                            <p class="help-block">Peso máximo de la foto 2MB</p>

                            <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previsualizarEditar" width="100px">

                            <input type="hidden" name="fotoActual" id="fotoActual">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Modificar usuario</button>
                </div>

                <?php
                $editarUsuario = new ControladorUsuarios();
                $editarUsuario->ctrEditarUsuario();
                ?> 

            </form>
        </div>
    </div>
</div>

<?php
$borrarUsuario = new ControladorUsuarios();
$borrarUsuario->ctrBorrarUsuario();
?> 


