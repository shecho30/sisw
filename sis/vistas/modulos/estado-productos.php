<div class="content-wrapper">
   <section class="content-header">
      <h1>
         Administrar Estados Producto
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
         <li class="active">Administrar Estados Producto</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="box">
         <div class="box-header with-border">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalCrearEstadoProducto">
            Crear Estado Producto
            </button>
         </div>
         <div class="box-body">
            <table class="table table-bordered table-striped dt-responsive tablaEProductos" width="100%">
               <thead>
                  <tr>
                     <th style="width:10px">#</th>
                     <th>Nombre Estado Producto</th>
                     <th>Accion</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     $item = null;
                     
                     $valor = null;
                     
                     $eproductos = ControladorEstadoProducto::ctrMostrarEstadoProducto($item, $valor);                    
                        foreach($eproductos as $data) {  
                           echo "<tr>
                                <td>{$data->id_estado_producto}</td>
                                <td>{$data->nombre_estado_producto}</td>
                                <td>
                                  <div class='btn-group'>                                           
                                    <button class='btn btn-warning btnEditarProducto' data-id='$data->id_estado_producto' data-toggle='modal' data-target='#modalEditarEstadoProducto'><i class='fa fa-pencil'></i></button>
                                    <button class='btn btn-danger btnEliminarProducto' data-id='$data->id_estado_producto' data-toggle='modal' data-target='#modalEliminarEstadoProducto'><i class='fa fa-times'></i></button>                          
                                  </div>                             
                                </td>                          
                              </tr>";                     
                        }                                                             
                  ?>
               </tbody>
            </table>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!--=====================================
   MODAL AGREGAR ESTADO PRODUCTO
   ======================================-->
<div id="modalCrearEstadoProducto" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <form role="form" method="post" enctype="multipart/form-data">
            <!--=====================================
               CABEZA DEL MODAL
               ======================================-->
            <div class="modal-header" style="background:#3c8dbc; color:white">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Crear Estado Producto</h4>
            </div>
            <!--=====================================
               CUERPO DEL MODAL
               ======================================-->
            <div class="modal-body">
               <div class="box-body">
                  <!-- ENTRADA PARA EL CÓDIGO -->
                  <div class="form-group">
                     <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-cubes"></i></span> 
                        <input type="text" class="form-control input-lg" id="nuevoEstado" name="nuevoEstado" placeholder="Ingresar un Nuevo Estado" required>
                     </div>
                  </div>
               </div>
            </div>
            <!--=====================================
               PIE DEL MODAL
               ======================================-->
            <div class="modal-footer">
               <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
               <button type="submit" class="btn btn-primary">Guardar Estado</button>
            </div>
         </form>
         <?php
            $crearProducto = new ControladorEstadoProducto();
            $crearProducto -> ctrCrearEstadoProducto();            
          ?>  
      </div>
   </div>
</div>

<!--=====================================
   MODAL EDITAR ESTADO PRODUCTO
   ======================================-->
<div id="modalEditarEstadoProducto" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <form role="form" method="post" enctype="multipart/form-data">
            <!--=====================================
               CABEZA DEL MODAL
               ======================================-->
            <div class="modal-header" style="background:#3c8dbc; color:white">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Editar Estado Producto</h4>
            </div>
            <!--=====================================
               CUERPO DEL MODAL
               ======================================-->
            <div class="modal-body">
               <div class="box-body">
                  <!-- ENTRADA PARA EL CÓDIGO -->
                  <input type="hidden" name="idEProducto" id="idEProducto">
                  <div class="form-group">
                     <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-cubes"></i></span> 
                        <input type="text" class="form-control input-lg" id="nuevoEstadoProducto" name="nuevoEstadoProducto"  placeholder="Ingresar un Nuevo Estado" required>
                     </div>
                  </div>
               </div>
            </div>
            <!--=====================================
               PIE DEL MODAL
               ======================================-->
            <div class="modal-footer">
               <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
               <button type="submit" class="btn btn-primary">Guardar Estado</button>
            </div>
         </form>
          <?php
            $crearProducto = new ControladorEstadoProducto();
            $crearProducto -> ctrActualizarEstadoProducto();                    
          ?>  
      </div>
   </div>
</div>