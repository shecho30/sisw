<?php
if ($_SESSION["perfil"] != 1 || $_SESSION["perfil"] == "Vendedor") {
    echo '<script>
    window.location = "inicio";
  </script>';
    return;
}
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar Promoción</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar Promoción</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarPromocion">
                    Agregar Promocion
                </button>
            </div>

            <div class="box-body">
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px">Codigo</th>
                            <th>imagen</th>
                            <th>nombre</th>
                            <th>enlace</th>
                            <th>posicion</th>
                            <th>Acciones</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        $item = null;
                        $valor = null;
                        $promocion = ControladorPromocion::ctrMostrarPromocion($item, $valor);
                        // print_r($promocion);
                        foreach ($promocion as $valor) 
                        {
                            echo "<tr>";
                                echo "<td>".$valor["id_promocion"]."</td>";
                                if($valor["imagen_promocion"] != "")
                                {

                                    echo '<td><img src="'.$valor["imagen_promocion"].'" class="img-thumbnail" width="40px"></td>';
                
                                }
                                else
                                {
                
                                    echo '<td><img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail" width="40px"></td>';
                
                                }
                                    echo "<td>".$valor["nombre_promocion"]."</td>";
                                    echo "<td>".$valor["enlace_promocion"]."</td>";
                                    echo "<td>".$valor["posicion_promocion"]."</td>";
                                if ($_SESSION["perfil"] == 1 || $_SESSION["perfil"] == "Vendedor") 
                                {
                                        echo '
                                        <td>
                                            <div class="btn-group">
                                            <button class="btn btn-warning btnEditarPromocion" idPromocion="' . $valor["id_promocion"] . '" data-toggle="modal" data-target="#modalEditarPromocion"><i class="fa fa-pencil"></i></button>
                                            <button class="btn btn-danger btnEliminarPromocion" nombrePromocion="'.$valor["nombre_promocion"].'" idPromocion="' . $valor["id_promocion"] . '" fotoPromocion ="' . $valor["imagen_promocion"].'"><i class="fa fa-times"></i></button>
                                            </div> 
                                        </td>';
                                }
                            echo "</tr>";           
                        }
                        ?> 
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>


<!-- AGREGAR PROMOCION-->

<div id="modalAgregarPromocion" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Promocion</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                        <!-- Posicion -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span> 
                                <select class="form-control input-lg" name="nuevoPosicion" id="nuevoPosicion">
                                    <option value="1">Posicion 1</option>
                                    <option value="2">Posicion 2</option>
                                    <option value="3">Posicion 3</option>
                                </select>
                                <!-- <input type="number" class="form-control input-lg" name="nuevoPosicion" id="nuevoPosicion" placeholder="Ingresar Posicion" required> -->
                            </div>
                        </div>

                        <!--Nombre-->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span> 
                                <input type="text" class="form-control input-lg " name="nuevoNombre" placeholder="Ingresar Nombre" required>
                            </div>
                        </div>

                        <!-- Enlace -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoEnlace" placeholder="Ingresar Enlace" id="nuevoEnlace" required>
                            </div>
                        </div>


                        <!-- foto -->

                        <div class="form-group">
                            <div class="panel">SUBIR FOTO</div>
                            <input type="file" class="fotoPro" name="fotoPro">
                            <p class="help-block">Peso máximo de la foto 2MB</p>
                            <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail mostrarFoto" width="100px">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Promocion</button>
                </div>

                 <?php
                $crearPromocion = new ControladorPromocion();
                $crearPromocion->ctrCrearPromocion();
                ?> 
            </form>
        </div>
    </div>
</div>

<!-- EDITAR PROMOCION-->

<div id="modalEditarPromocion" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar Promocion</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                    <!-- id promocion-->
                    <div class="form-group">
                            <div class="input-group">
                                <input type="hidden" class="form-control input-lg" id="editPromocion" name="editPromocion" placeholder="Ingresar Posicion" required>
                            </div>
                        </div>

                        <!-- Posicion -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span> 
                                <input type="number" class="form-control input-lg" id="editPosicion" name="editPosicion" min="1" max="3" placeholder="Ingresar Posicion" required>
                                <input type="hidden" id="posicionActual">
                            </div>
                        </div>

                        <!--Nombre-->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span> 
                                <input type="text" class="form-control input-lg " name="editNombre" id="editNombre" placeholder="Ingresar Nombre" required>
                            </div>
                        </div>

                        <!-- Enlace -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" name="EditEnlace" placeholder="Ingresar Enlace" id="EditEnlace" required>
                            </div>
                        </div>


                        <!-- foto -->

                        <div class="form-group">
                            <div class="panel">SUBIR FOTO</div>
                            <input type="file" class="fotoPro" name="EditFoto">
                            <p class="help-block">Peso máximo de la foto 2MB</p>
                            <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail mostrarFotoEdit" width="100px">
                            <input type="hidden" name="fotoActualPromo" id="fotoActualPromo">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Promocion</button>
                </div>

                 <?php
                $edit = new ControladorPromocion();
                $edit->ctrEditarPromocion();
                ?> 
            </form>
        </div>
    </div>
</div>



<?php
 $crearPromocion = new ControladorPromocion();
 $crearPromocion->ctrBorrarPromocion();
?>


