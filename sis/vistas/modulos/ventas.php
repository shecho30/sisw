<?php
if ($_SESSION["perfil"] == "Especial") {
    echo '<script>
    window.location = "inicio";
  </script>';
    return;
}
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar ventas</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar ventas</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <a href="crear-venta">
                    <button class="btn btn-primary">
                        Agregar venta
                    </button>
                </a>

                <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                    <span>
                        <i class="fa fa-calendar"></i> Rango de fecha
                    </span>
                    <i class="fa fa-caret-down"></i>
                </button>
            </div>

            <div class="box-body">
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px">#</th>
                            <th>Código factura</th>
                            <th>Cliente</th>
                            <th>Vendedor</th>
                            <th>Forma de pago</th>
                            <th>Neto</th>
                            <th>Total</th> 
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr> 
                    </thead>
                    <tbody>

                        <?php
                        if (isset($_GET["fechaInicial"])) {
                            $fechaInicial = $_GET["fechaInicial"];
                            $fechaFinal = $_GET["fechaFinal"];
                        } else {
                            $fechaInicial = null;
                            $fechaFinal = null;
                        }
                        $respuesta = ControladorVentas::ctrRangoFechasVentas($fechaInicial, $fechaFinal);
                        foreach ($respuesta as $key => $value) {
                            echo '<tr>
                  <td>' . ($key + 1) . '</td>
                  <td>' . $value["id_dian_factura"] . '</td>';
                            $itemCliente = "id_cliente";
                            $valorCliente = $value["id_cliente"];
                            $respuestaCliente = ControladorClientes::ctrMostrarClientesV($itemCliente, $valorCliente);
                            echo '<td>' . $respuestaCliente["nombre_repre_cliente"] . '</td>';
                            $itemUsuario = "id_usuario";
                            $valorUsuario = $value["id_usuario"];
                            $respuestaUsuario = ControladorUsuarios::ctrMostrarUsuariosV($itemUsuario, $valorUsuario);
                            echo '<td>' . $respuestaUsuario["nombre_usuario"] . '</td>
                  <td>' . $value["id_forma_pago"] . '</td>
                  <td>$ ' . number_format($value["neto_factura"], 2) . '</td>
                  <td>$ ' . number_format($value["total_factura"], 2) . '</td>
                  <td>' . $value["fecha_creacion_factura"] . '</td>
                  <td>
                    <div class="btn-group">                       
                      <button class="btn btn-info btnImprimirFactura" codigoVenta="' . base64_encode($value["id_factura"]) . '">
                        <i class="fa fa-print"></i>
                      </button>';
                            
                            echo '</div>  
                  </td>
                </tr>';
                        }
                        ?>

                    </tbody>
                </table>
                <?php
                // $eliminarVenta = new ControladorVentas();
                // $eliminarVenta->ctrEliminarVenta();
                ?>
            </div>
        </div>
    </section>
</div>