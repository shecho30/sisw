<?php
if ($_SESSION ["reenvio"] == "2" ) {
    $_SESSION["reenvio"] = "1";
      echo '<script>
      window.location = "clientes";
    </script>';
  
    return;
      
  }
if ($_SESSION["perfil"] == "Especial") {
    echo '<script>
    window.location = "inicio";
  </script>';
    return;
}
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar clientes</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar clientes</li>
        </ol>

    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCliente">
                    Agregar cliente
                </button>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px">#</th>
                            <th>Documento</th>
                            <!-- <th>Tipo Documento</th> -->
                            <th>Empresa</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Direccion</th> 
                            <!-- <th>Telefono</th> -->
                            <th>E-mail</th>
                            <!-- <th>Fecha</th> -->
                            <th>Estado</th>
                            <th>Ciudad</th>
                            <th>Acciones</th>
                        </tr> 
                    </thead>

                    <tbody>
                        <?php
                        $item = null;
                        $valor = null;
                        $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);
                        foreach ($clientes as $key => $value) {
                            echo 
                '<tr>
                    <td>' .$value["id_cliente"]. '</td>
                    <td>' .$value["document_cliente"]. '</td>
                    
                    <td>' .$value["empresa_cliente"]. '</td>
                    <td>' .$value["nombre_repre_cliente"]. '</td>
                    <td>' .$value["apellido_repre_cliente"]. '</td>';
                    //<td>' .$value["telefono_cliente"]. '</td>            
                    echo '<td>' .$value["direccion_cliente"]. '</td>
                    <td>' .$value["correo_cliente"]. '</td>';
                    // <td>' .$value["fecha_creacion_cliente"]. '</td>';

                    if ($value["nombre_estado_usuario"] == 'activo') {
                        echo '<td><button class="btn btn-success btnNew" id_cliente="'.$value["id_cliente"].'" estado_cliente="2">' .$value["nombre_estado_usuario"]. '</button></td>';
                    }else if ($value["nombre_estado_usuario"] == 'inactivo') {
                        echo '<td><button class="btn btn-danger btnNew" id_cliente="'.$value["id_cliente"].'" estado_cliente="3">' .$value["nombre_estado_usuario"]. '</button></td>';
                    }else{
                        echo '<td><button class="btn btn-warning btnNew" id_cliente="'.$value["id_cliente"].'" estado_cliente="1">' .$value["nombre_estado_usuario"]. '</button></td>';
                    }
                   
                    echo '<td>'.$value["nombre_ciudad"].'</td>
                         
                    <td>
                      <div class="btn-group">
                        <button class="btn btn-warning btnEditarCliente"  idCliente="'.$value["id_cliente"].'" data-toggle="modal" data-target="#modalEditarCliente"><i class="fa fa-pencil"></i></button>';
                            if ($_SESSION["perfil"] == "1" || $_SESSION["perfil"] == "Admin") {
                                echo '<button class="btn btn-danger btnEliminarCliente" idCliente="' . $value["id_cliente"] . '"><i class="fa fa-times"></i></button>';
                            }
                            echo '</div>  
                    </td>
                </tr>';
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- AGREGAR CLIENTE -->

<div id="modalAgregarCliente" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post">

                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar cliente</h4>
                </div>


                <div class="modal-body">
                    <div class="box-body">

                         <!-- DOCUMENTO ID -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span> 
                                <input type="number" min="" class="form-control input-lg" name="newDocument" placeholder="Ingresar numero documento" id="newDocument" required>
                            </div>
                        </div>

                        <!-- Tipo de documento -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card"></i></span> 
                                <select class="form-control input-lg" name="newTypeDocument" required>
                                    <option disabled selected value = "">Seleccione tipo de documento</option>
                                    <?php

                                        $item = null;
                                        $valor = null;
                                        $clientes = ControladorClientes::ctrMostrarDocumento($item, $valor);
                                        foreach ($clientes as $key => $value) {
                                            echo '<option value="'.$value["id_tipo_documento"].'">'.$value["nombre_tipo_documento"].'</option>';
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Empresa Cliente -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                <input type="text" class="form-control input-lg" name="newCompanyCustomer" placeholder="Ingrese nombre de la empresa" id="newCompanyCustomer">
                            </div>
                        </div>

                        <!-- NOMBRE -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" name="newName" placeholder="Ingresar nombre" id="newName" required>
                            </div>
                        </div>

                        <!-- Apellido -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" name="newLastName" placeholder="Ingresar apellido" id="newLastName" required>
                            </div>
                        </div>


                        <!-- TELÉFONO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 
                                <input type="number" class="form-control input-lg" name="newPhone" placeholder="Ingresar teléfono" id="newPhone" required>
                            </div>
                        </div>

                        <!-- DIRECCIÓN -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 
                                <input type="text" class="form-control input-lg" name="newAddress" placeholder="Ingresar dirección" id="newAddress" required>
                            </div>
                        </div>

                        <!-- CORREO ELECTRONICO -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="email" name="newEmail" class="form-control input-lg" placeholder="Ingrese Email" id="newEmail" required>
                            </div>
                        </div>

                        <!-- CONTRASEÑA -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                                <input type="password" name="newPassword" class="form-control input-lg" placeholder="Ingrese contraseña" id="newPassword" required>
                            </div>
                        </div>
                        
                        <!-- Estado Usuario -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bell-o"></i></span>
                                <select class="form-control input-lg" name="newState" required>
                                    <option disabled selected value="">Seleccione el estado del Cliente</option>
                                    <?php
                                    $item  = null;
                                    $valor = null;
                                    $ciudad = ControladorClientes::ctrEstadoUsuario($item, $valor);

                                    foreach ($ciudad as $key => $value) {
                                        echo '<option value="'.$value["id_estado_usuario"].'">'.$value["nombre_estado_usuario"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Fecha -->
                        <?php 

                            date_default_timezone_set('America/Bogota');

                            $fechaActual = date("Y-m-d H:i:s");

                        ?>
                        <div>
                            <div>
                                <input type="hidden" name="newDate" class="form-control input-lg" value="<?= $fechaActual?>">
                            </div>
                        </div>


                         <!-- Ciudad -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bell-o"></i></span>
                                <select class="form-control input-lg" name="newCity" required>
                                    <option disabled selected value = "">Seleccione la ciudad</option>
                                    <?php
                                    $item  = null;
                                    $valor = null;
                                    $ciudad = ControladorClientes::ctrMostrarCiudad($item, $valor);

                                    foreach ($ciudad as $key => $value) {
                                        echo '<option value="'.$value["id_ciudad"].'">'.$value["nombre_ciudad"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar cliente</button>
                </div>
            </form>

            <?php
            $crearCliente = new ControladorClientes();
            $crearCliente->ctrCrearCliente();
            ?>
        </div>
    </div>
</div>

<!-- EDITAR CLIENTE-->
<div id="modalEditarCliente" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post">

                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar cliente</h4>
                </div>


                <div class="modal-body">
                    <div class="box-body">

                         <!-- DOCUMENTO ID -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="hidden" id="id_Cliente" name="id_Cliente" value=""> 
                                <input type="number" min="" class="form-control input-lg" id="editDocument" name="editDocument" placeholder="Ingresar numero documento" value="" required>
                            </div>
                        </div>



                        <!-- Tipo de documento -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card"></i></span> 
                                <select class="form-control input-lg" name="editTypeDocument">
                                    <option id="editTypeDocument" value=""></option>
                                    <?php

                                        $item = null;
                                        $valor = null;
                                        $clientes = ControladorClientes::ctrMostrarDocumento($item, $valor);
                                        foreach ($clientes as $key => $value) {
                                            echo '<option value="'.$value["id_tipo_documento"].'">'.$value["nombre_tipo_documento"].'</option>';
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Empresa Cliente -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                <input type="text" class="form-control input-lg" id="editCompanyCustomer" name="editCompanyCustomer" placeholder="Ingrese nombre de la empresa" value="">
                            </div>
                        </div>

                        <!-- NOMBRE -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" id="editName" name="editName" placeholder="Ingresar nombre" value="" required>
                            </div>
                        </div>

                        <!-- Apellido -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                                <input type="text" class="form-control input-lg" id="editLastName" name="editLastName" placeholder="Ingresar apellido" value="" required>
                            </div>
                        </div>


                        <!-- TELÉFONO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 
                                <input type="number" class="form-control input-lg" id="editPhone" name="editPhone" placeholder="Ingresar teléfono" value="" required>
                            </div>
                        </div>

                        <!-- DIRECCIÓN -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 
                                <input type="text" class="form-control input-lg" id="editAddress" name="editAddress" placeholder="Ingresar dirección" value="" required>
                            </div>
                        </div>

                        <!-- CORREO ELECTRONICO -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="email" name="editEmail" id="editEmail" class="form-control input-lg" placeholder="Ingrese Email" value="" required>
                            </div>
                        </div>

                        <!-- CONTRASEÑA -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                                <input type="password" name="editPassword" class="form-control input-lg" placeholder="Ingrese nueva contraseña" id="editPassword" required>
                                <input type="hidden" name="passwordActual" id="passwordActual" value="">
                            </div>
                        </div>
                        
                        <!-- Estado Usuario -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bell-o"></i></span>
                                <select class="form-control input-lg" name="editState">
                                    <option id="editState" value=""></option>
                                    <?php
                                    $item  = null;
                                    $valor = null;
                                    $ciudad = ControladorClientes::ctrEstadoUsuario($item, $valor);

                                    foreach ($ciudad as $key => $value) {
                                        echo '<option value="'.$value["id_estado_usuario"].'">'.$value["nombre_estado_usuario"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                         <!-- Ciudad -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bell-o"></i></span>
                                <select class="form-control input-lg" name="editCity">
                                    <option id="editCity" value=""></option>
                                    <?php
                                    $item  = null;
                                    $valor = null;
                                    $ciudad = ControladorClientes::ctrMostrarCiudad($item, $valor);

                                    foreach ($ciudad as $key => $value) {
                                        echo '<option value="'.$value["id_ciudad"].'">'.$value["nombre_ciudad"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Editar cliente</button>
                </div>
            </form>

           <?php
            $editarCliente = new ControladorClientes();
            $editarCliente->ctrEditarCliente();
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
  $("#customers").addClass("active");
</script>
<?php
$eliminarCliente = new ControladorClientes();
$eliminarCliente->ctrEliminarCliente();
?>
