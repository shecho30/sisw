<?php
if ($_SESSION ["reenvio"] == "2" ) {
  $_SESSION["reenvio"] = "1";
    echo '<script>
    window.location = "crear-venta";
  </script>';

  return;
    
}
?>
<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Crear venta
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Crear venta</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="row">

      <!--=====================================
      EL FORMULARIO
      ======================================-->
      
      <div class="col-lg-5 col-xs-12">
        
        <div class="box box-success">
          
          <div class="box-header with-border"></div>

          <form role="form" method="post" class="formularioVenta">

            <div class="box-body">
  
              <div class="box">

                <!--=====================================
                ENTRADA DEL VENDEDOR
                ======================================-->
            
                <div class="form-group">
                
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-user"></i></span> 

                    <input type="text" class="form-control" id="nuevoVendedor" value="<?php echo $_SESSION["nombre"]; ?>" readonly>

                    <input type="hidden" name="idVendedor" value="<?php echo $_SESSION["id"]; ?>">

                  </div>

                </div> 

                <!--=====================================
                ENTRADA DEL CÓDIGO
                ======================================--> 

                <div class="form-group">
                  
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>

                    <?php

                    $item = null;
                    $valor = null;

                    $ventas = ControladorVentas::ctrMostrarVentas($item, $valor);

                    if(!$ventas){

                      echo '<input type="text" class="form-control" id="nuevaVenta" name="nuevaVenta" value="10001" readonly>';
                  

                    }else{

                      foreach ($ventas as $key => $value) {
                        
                        
                      
                      }

                      $codigo = $value["id_dian_factura"] + 1;



                      echo '<input type="text" class="form-control" id="nuevaVenta" name="nuevaVenta" value="'.$codigo.'" readonly>';
                  

                    }

                    ?>
                    
                    
                  </div>
                
                </div>

                <!--=====================================
                ENTRADA DEL CLIENTE
                ======================================--> 

                <div class="form-group">
                  
                  <div class="input-group">
                    
                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                    
                    <select class="form-control nuevouserc" id="seleccionarCliente" name="seleccionarCliente" required>

                    <option value="">Seleccionar cliente</option>

                    <?php

                      $item = null;
                      $valor = null;

                      $categorias = ControladorClientes::ctrMostrarClientes($item, $valor);

                       foreach ($categorias as $key => $value) {

                         echo '<option value="'.$value["id_cliente"].'">'.$value["nombre_repre_cliente"]. " " .$value["apellido_repre_cliente"].'</option>';

                       }

                    ?>

                    </select>
                    
                    <span class="input-group-addon"><button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalAgregarCliente" data-dismiss="modal">Agregar cliente</button></span>
                  
                  </div>
                
                </div>

                <!--=====================================
                ENTRADA PARA AGREGAR PRODUCTO
                ======================================--> 

                <div class="form-group row nuevoProducto">

                

                </div>

                <input type="hidden" id="listaProductos" name="listaProductos">

                <!--=====================================
                BOTÓN PARA AGREGAR PRODUCTO
                ======================================-->

                <button type="button" class="btn btn-default hidden-lg btnAgregarProducto">Agregar producto</button>

                <hr>

                <div class="row">

                  <!--=====================================
                  ENTRADA IMPUESTOS Y TOTAL
                  ======================================-->
                  
                  <div class="col-xs-8 pull-right">
                    
                    <table class="table">

                      <thead>

                        <tr>
                          <th>Impuesto</th>
                          <th>Total</th>      
                        </tr>

                      </thead>

                      <tbody>
                      
                        <tr>
                          
                          <td style="width: 50%">
                            
                            <div class="input-group">
                           
                              <input type="number" class="form-control input-lg" min="0" id="nuevoImpuestoVenta" name="nuevoImpuestoVenta" placeholder="19" value="19" required>

                               <input type="hidden" name="nuevoPrecioImpuesto" id="nuevoPrecioImpuesto" required>

                               <input type="hidden" name="nuevoPrecioNeto" id="nuevoPrecioNeto" required>

                              <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                        
                            </div>

                          </td>

                           <td style="width: 50%">
                            
                            <div class="input-group">
                           
                              <span class="input-group-addon"><i class="ion ion-social-usd"></i></span>

                              <input type="text" class="form-control input-lg" id="nuevoTotalVenta" name="nuevoTotalVenta" total="" placeholder="00000" readonly required>

                              <input type="hidden" name="totalVenta" id="totalVenta">
                              
                        
                            </div>

                          </td>

                        </tr>

                      </tbody>

                    </table>

                  </div>

                </div>

                <hr>

                <!--=====================================
                ENTRADA MÉTODO DE PAGO
                ======================================-->

                <div class="form-group row">
                  
                  <div class="col-xs-6" style="padding-right:0px">
                    
                     <div class="input-group">

                      <select class="form-control" id="nuevoMetodoPago" name="nuevoMetodoPago" required>

                        <option value="">Seleccionar Metodo de pago</option>

                        <?php

                          $item = null;
                          $valor = null;

                          $metodos = ControladorVentas::ctrMostrarFormapago($item, $valor);

                            foreach ($metodos as $key => $values) {

                            echo '<option value="'.$values["id_forma_pago"].'">'.$values["nombre_forma_pago"].'</option>';

                            }

                        ?>                
                      </select>    

                    </div>

                  </div>

                  <div class="cajasMetodoPago"></div>

                  <input type="hidden" id="listaMetodoPago" name="listaMetodoPago">

                  <input type="hidden" id="validarPago" name="validarPago">

                </div>

                <br>
      
              </div>

          </div>

          <div class="box-footer">

            <button type="submit" class="btn btn-primary pull-right">Guardar venta</button>

          </div>

        </form>

        <?php

          $guardarVenta = new ControladorVentas();
          $guardarVenta -> ctrCrearVenta();
          
        ?>

        </div>
            
      </div>

      <!--=====================================
      LA TABLA DE PRODUCTOS
      ======================================-->

      <div class="col-lg-7 hidden-md hidden-sm hidden-xs">
        
        <div class="box box-warning">

          <div class="box-header with-border"></div>

          <div class="box-body">
            
            <table class="table table-bordered table-striped dt-responsive tablaVentas">
              
               <thead>

                 <tr>
                  <th style="width: 10px">#</th>
                  <th>Imagen</th>
                  <th>Código</th>
                  <th>Descripcion</th>
                  <th>Stock</th>
                  <th>Acciones</th>
                </tr>

              </thead>

            </table>

          </div>

        </div>


      </div>

    </div>
   
  </section>

</div>

<!--=====================================
MODAL AGREGAR CLIENTE
======================================-->

<div id="modalAgregarCliente" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

<div class="modal-header" style="background:#3c8dbc; color:white">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Agregar cliente</h4>
</div>


<div class="modal-body">
    <div class="box-body">

         <!-- DOCUMENTO ID -->

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key"></i></span> 
                <input type="number" min="" class="form-control input-lg" name="newDocument" placeholder="Ingresar numero documento" id="newDocument" required>
            </div>
        </div>

        <!-- Tipo de documento -->
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-address-card"></i></span> 
                <select class="form-control input-lg" name="newTypeDocument" required>
                    <option disabled selected value = "">Seleccione tipo de documento</option>
                    <?php

                        $item = null;
                        $valor = null;
                        $clientes = ControladorClientes::ctrMostrarDocumento($item, $valor);
                        foreach ($clientes as $key => $value) {
                            echo '<option value="'.$value["id_tipo_documento"].'">'.$value["nombre_tipo_documento"].'</option>';
                        }

                    ?>
                </select>
            </div>
        </div>

        <!-- Empresa Cliente -->
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-university"></i></span>
                <input type="text" class="form-control input-lg" name="newCompanyCustomer" placeholder="Ingrese nombre de la empresa">
            </div>
        </div>

        <!-- NOMBRE -->

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                <input type="text" class="form-control input-lg" name="newName" placeholder="Ingresar nombre" required>
            </div>
        </div>

        <!-- Apellido -->

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span> 
                <input type="text" class="form-control input-lg" name="newLastName" placeholder="Ingresar apellido" required>
            </div>
        </div>


        <!-- TELÉFONO -->

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone"></i></span> 
                <input type="number" class="form-control input-lg" name="newPhone" placeholder="Ingresar teléfono" required>
            </div>
        </div>

        <!-- DIRECCIÓN -->
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 
                <input type="text" class="form-control input-lg" name="newAddress" placeholder="Ingresar dirección" required>
            </div>
        </div>

        <!-- CORREO ELECTRONICO -->
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                <input type="email" name="newEmail" class="form-control input-lg" placeholder="Ingrese Email" id="newEmail" required>
            </div>
        </div>

        <!-- CONTRASEÑA -->

        <div class="form-group">
            <div class="input-group">
                
                <input type="hidden" name="newPassword" class="form-control input-lg" value="123456" required>
            </div>
        </div>
        
        <!-- Estado Usuario -->

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bell-o"></i></span>
                <select class="form-control input-lg" name="newState" required>
                    <option disabled selected value="">Seleccione el estado del Cliente</option>
                    <?php
                    $item  = null;
                    $valor = null;
                    $ciudad = ControladorClientes::ctrEstadoUsuario($item, $valor);

                    foreach ($ciudad as $key => $value) {
                        echo '<option value="'.$value["id_estado_usuario"].'">'.$value["nombre_estado_usuario"].'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>

        <!-- Fecha -->
        <?php 

            date_default_timezone_set('America/Bogota');

            $fechaActual = date("Y-m-d H:i:s");

        ?>
        <div>
            <div>
                <input type="hidden" name="newDate" class="form-control input-lg" value="<?= $fechaActual?>">
            </div>
        </div>


         <!-- Ciudad -->

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-bell-o"></i></span>
                <select class="form-control input-lg" name="newCity" required>
                    <option disabled selected value = "">Seleccione la ciudad</option>
                    <?php
                    $item  = null;
                    $valor = null;
                    $ciudad = ControladorClientes::ctrMostrarCiudad($item, $valor);

                    foreach ($ciudad as $key => $value) {
                        echo '<option value="'.$value["id_ciudad"].'">'.$value["nombre_ciudad"].'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>

    </div>
</div>


<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
    <button type="submit" class="btn btn-primary">Guardar cliente</button>
</div>
</form>

      <?php

        $crearCliente = new ControladorClientes();
        $crearCliente -> ctrCrearCliente();

      ?>

    </div>

  </div>

</div>
