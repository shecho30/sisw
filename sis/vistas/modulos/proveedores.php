<?php
if ($_SESSION["perfil"] != 1 || $_SESSION["perfil"] == "Vendedor") {
    echo '<script>
    window.location = "inicio";
  </script>';
    return;
}
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar Proveedores</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li class="active">Administrar Proveedores</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarProveedor">
                    Agregar Proveedor
                </button>
            </div>

            <div class="box-body">
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px">Codigo</th>
                            <th>Documento</th>
                            <th>Empresa</th>
                            <th>Nombre</th>
                            <th>Correo</th>
                            <th>Acciones</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        $item = null;
                        $valor = null;
                        $proveedores = ControladorProveedor::ctrMostrarProveedores($item, $valor);
                        //print_r($proveedores);
                        foreach ($proveedores as $valor) {
                        echo "<tr>";
                            echo "<td>".$valor["id_proveedor"]."</td>";
                            echo "<td>".$valor["documento_proveedor"]."</td>";
                            echo "<td>".$valor["empresa_proveedor"]."</td>";
                            echo "<td>".$valor["nombre_repre_proveedor"]."</td>";
                            echo "<td>".$valor["correo_proveedor"]."</td>";
                                echo '<td>
                                    <div class="btn-group">
                                    <button class="btn btn-warning btnEditarProveedor" idProveedor="' . $valor["id_proveedor"] . '" data-toggle="modal" data-target="#modalEditarProveedores"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger btnEliminarProveedor" idProveedor="' . $valor["id_proveedor"] . '"><i class="fa fa-times"></i></button>
                                    </div>  
                                </td>';
                        echo "</tr>";       
                        }
                        ?> 
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>



<!-- AGREGAR PROVEEDORES-->

<div id="modalAgregarProveedor" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Proveedor</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                          <!-- TIPO DOCUMENTO -->

                         <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span> 
                                <select class="form-control input-lg" name="nuevoTiDocu" required>
                                    <option value="" selected disabled>Selecionar Tipo Documento</option>
                                    <option value="1">Cedula Ciudadania</option>
                                    <option value="2">Cedula Extranjeria</option>
                                    <option value="3">Pasaporte</option>
                                </select>
                            </div>
                        </div>

                         <!-- NUMERO DOCUMENTO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoNuDocu" placeholder="Ingresar numero Documento" required>
                            </div>
                        </div>


                        <!-- NOMBRE EMPRESA -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoNombreEmp" placeholder="Ingresar nombre Empresa" required>
                            </div>
                        </div>

                         <!-- NOMBRE REPRESENTANTE -->
                         <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre Representante" required>
                            </div>
                        </div>

                        <!--APELLIDO-->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span> 
                                <input type="text" class="form-control input-lg " name="nuevoApellido" placeholder="Ingresar Apellido Representante" required>
                            </div>
                        </div>

                         <!-- CORREO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span> 
                                <input type="email" class="form-control input-lg" name="nuevoCorreo" placeholder="Ingresar Correo" required>
                            </div>
                        </div>

                        <!-- TELEFONO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoTelefono" placeholder="Ingresar Telefono" required>
                            </div>
                        </div>

                        <!-- DIRECCION -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoDir" placeholder="Ingresar Direccion" required>
                            </div>
                        </div>

                       
                        <!-- SELECCIONAR SU CIUDAD -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span> 
                                <select class="form-control input-lg" name="nuevoCiudad" required>
                                    <option value="" selected disabled>Selecionar Ciudad</option>
                                    <option value="1">Cali</option>
                                    <option value="2">Bogota</option>
                                    <option value="3">Medellin</option>
                                </select>
                            </div>
                        </div>

                        <!-- SELECCIONAR SU producto -->

                        <!-- <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-drivers-license"></i></span> 
                                <select class="form-control input-lg" name="nuevoPro" required>
                                    <option value="" selected disabled>Selecionar Producto</option> -->
                                    <?php
                                        //  $item = null;
                                        //  $valor = null;
                                        //  $productos = ControladorProductos::ctrMostrarProductosOrden($item,$valor);
                                        // foreach($productos as $value)
                                        // {
                                        //     echo '<option value="'.$value["id_producto"].'">'.$value["nombre_producto"].'</option>';
                                        // }
                                         
                                    ?>
                                    <!-- <option value="1">USB</option>
                                    <option value="2">Pc</option>
                                    <option value="3">Cd</option> -->
                                <!-- </select>
                            </div>
                        </div> -->

                        <!-- SUBIR FOTO -->

                        <div class="form-group">
                            <div class="panel">SUBIR FOTO</div>
                            <input type="file" class="nuevaFoto" name="nuevaFoto">
                            <p class="help-block">Peso máximo de la foto 2MB</p>
                            <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Proveedor</button>
                </div>

                <?php
                $crearProveedor = new ControladorProveedor();
                $crearProveedor->ctrCrearProveedor();
                ?>
            </form>
        </div>
    </div>
</div>

<!-- EDITAR PROVEEDORES-->

<div id="modalEditarProveedores" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar Proveedor</h4>
                </div>


                <div class="modal-body">
                    <div class="box-body">
                        
                        <!-- ID PROVEEDOR -->

                        <div class="form-group">
                            <div class="input-group">
                                <input type="hidden"  name="idProveedor" id="idProveedor"  required>
                            </div>
                        </div>


                        <!-- Tipo Documento -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span> 
                                <select class="form-control input-lg" name="nuevoTiDocuEdit" id="nuevoTiDocuEdit">
                                    <option value="">Selecionar Tipo Documento</option>
                                    <option value="1">Cedula Ciudadania</option>
                                    <option value="2">Cedula Extranjeria</option>
                                    <option value="3">Pasaporte</option>
                                </select>
                            </div>
                        </div>

                         <!-- NUMERO DOCUMENTO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoNuDocuEdit" id="nuevoNuDocuEdit" placeholder="Ingresar numero Documento" required>
                            </div>
                        </div>


                        <!-- NOMBRE EMPRESA -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoNombreEmpEdit" id="nuevoNombreEmpEdit" placeholder="Ingresar nombre Empresa" required>
                            </div>
                        </div>

                         <!-- NOMBRE REPRESENTANTE -->
                         <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoNombreEdit" id="nuevoNombreEdit" placeholder="Ingresar nombre Representante" required>
                            </div>
                        </div>

                        <!--APELLIDO-->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span> 
                                <input type="text" class="form-control input-lg " name="nuevoApellidoEdit" id="nuevoApellidoEdit" placeholder="Ingresar Apellido Representante" required>
                            </div>
                        </div>

                         <!-- CORREO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span> 
                                <input type="email" class="form-control input-lg" name="nuevoCorreoEdit" id="nuevoCorreoEdit" placeholder="Ingresar Correo" required>
                            </div>
                        </div>

                        <!-- TELEFONO -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoTelefonoEdit" id="nuevoTelefonoEdit" placeholder="Ingresar Tlefono" required>
                            </div>
                        </div>

                        <!-- DIRECCION -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevoDirEdit" id="nuevoDirEdit" placeholder="Ingresar Direccion" required>
                            </div>
                        </div>

                       
                        <!-- SELECCIONAR SU CIUDAD -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span> 
                                <select class="form-control input-lg" name="nuevoCiudadEdit" id="nuevoCiudadEdit"> 
                                    <option value="">Selecionar Ciudad</option>
                                    <option value="1">Cali</option>
                                    <option value="2">Bogota</option>
                                    <option value="3">Medellin</option>
                                </select>
                            </div>
                        </div>

                        <!-- SELECCIONAR SU PERFIL -->

                        <!-- <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-drivers-license"></i></span> 
                                <select class="form-control input-lg" name="nuevoProEdit" id="nuevoProEdit"> -->
                                    <!-- <option value="">Selecionar Producto</option> -->
                                    <?php
                                        //  $item = null;
                                        //  $valor = null;
                                        //  $productos = ControladorProductos::ctrMostrarProductosOrden($item,$valor);
                                        // foreach($productos as $value)
                                        // {
                                        //     echo '<option value="'.$value["id_producto"].'">'.$value["nombre_producto"].'</option>';
                                        // }
                                         
                                    ?>
                                <!-- </select>
                            </div>
                        </div> -->

                        <!-- SUBIR FOTO -->

                        <div class="form-group">
                            <div class="panel">SUBIR FOTO</div>
                            <input type="file" class="nuevaFoto" name="nuevaFotoEdit" id="nuevaFotoEdit">
                            <p class="help-block">Peso máximo de la foto 2MB</p>
                            <img src="vistas/img/usuarios/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Modificar Proveedor</button>
                </div>

                <?php
                 $editarProveedor = new ControladorProveedor();
                 $editarProveedor->ctrEditarProveedor();
                ?> 

            </form>
        </div>
    </div>
</div>




<?php
$borrar_proveedor = new ControladorProveedor();
$borrar_proveedor->ctrBorrarProveedor();
?> 
