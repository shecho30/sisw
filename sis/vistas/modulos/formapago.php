<?php
if ($_SESSION["perfil"] == "Vendedor") {
    echo '<script>
    window.location = "inicio";
  </script>';
    return;
}
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar forma de pago</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>      
            <li class="active">Administrar Forma de Pago</li>    
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">  
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarFormaPago">          
                    Agregar Forma de Pago
                </button>
            </div>

            <div class="box-body">        
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px">Codigo</th>
                            <th>Nombre</th>                         
                            <th style="width:100px" >Accion</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        $item = null;
                        $valor = null;
                        $formapago = ControladorFormaPago::ctrMostrarFormaPago($item, $valor);
                        //print_r($categorias);
                        foreach ($formapago as $valor) 
                        {
                            echo '<tr>';
                            echo '<td>'.$valor["id_forma_pago"].'</td>';
                            echo '<td>'.$valor["nombre_forma_pago"].'</td>';
                            echo '<td> 
                                    <div class="btn-group"> 
                                        <button class="btn btn-warning btnEditarFormaPago" idFormaPago="' . $valor["id_forma_pago"] . '" data-toggle="modal" data-target="#modalEditarFormaPago"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger btnEliminarFormaPago" idFormaPago="' . $valor["id_forma_pago"] . '"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- AGREGAR CATEGORÍA -->

<div id="modalAgregarFormaPago" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Forma de Pago
                    </h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <!--  NOMBRE -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevaFormaPago" placeholder="Ingresar forma de pago" required>
                            </div>
                        </div>
                        
                     <!--  <div class="row">
                            <div col-12>
                                <textarea name="descripcionCategoria" id="" class="form-control input-lg" placeholder="Ingrese Descipcion"></textarea>
                            </div>
                        </div>-->


                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Forma de Pago</button>
                </div>
                <?php
                $crearFormaPago = new ControladorFormaPago();
                $crearFormaPago->ctrCrearFormaPago();
                ?>
            </form>
        </div>
    </div>
</div>

<!-- EDITAR CATEGORÍA -->
<div id="modalEditarFormaPago" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar Forma de Pago</h4>
                </div>


                <div class="modal-body">
                    <div class="box-body">
                        <!-- NOMBRE -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                                <input type="text" class="form-control input-lg" name="editarFormaPago" id="editarFormaPago" required>
                                <input type="hidden"  name="idFormaPago" id="idFormaPago" required>
                            </div>
                        </div>

                         <!--  <div class="row">
                            <div col-12>
                                <textarea name="descripcionCategoria" id="descipcionCategoria" class="form-control input-lg" placeholder=""></textarea>
                            </div>
                        </div>-->

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>

                <?php
                $editarFormaPago= new ControladorFormaPago();
                $editarFormaPago->ctrEditarFormaPago();
                ?> 
            </form>
        </div>
    </div>
</div>


<?php
 $borrarFormaPago = new ControladorFormaPago();
 $borrarFormaPago->ctrBorrarFormaPago();
?> */