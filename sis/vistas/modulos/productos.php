<div class="content-wrapper">
  <section class="content-header">    
    <h1>      
      Administrar productos    
    </h1>

    <ol class="breadcrumb">      
      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>      
      <li class="active">Administrar productos</li>    
    </ol>
  </section>
  
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarProducto">          
            Agregar producto
          </button>
      </div>

      <div class="box-body">        
       <table class="table table-bordered table-striped dt-responsive listProductos" width="100%">         
        <thead>         
          <tr>           
           <th style="width:10px">#</th>
           <th>Nombre</th>
           <th>Codigo</th>
           <th>Cantidad</th>
           <!-- <th>Precio de compra</th> -->
           <th>Precio de salida</th>
           <!-- <th>Unidad de medicion</th> -->
           <!-- <th>Presentacion</th> -->
           <!-- <th>Fecha</th> -->
           <th>Imagen</th>
           <!-- <th>Descripcion</th> -->
           <th>categoria</th>
           <th>estado</th>
           <th>Acciones</th>        
         </tr> 
        </thead>

      <tbody>

       

                 
        </tbody>

       </table>

      </div>

    </div>
  </section>
</div>

<!--=====================================
MODAL AGREGAR PRODUCTO
======================================-->

<div id="modalAgregarProducto" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar producto</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">
            <!-- ENTRADA PARA LA NOMBRE -->
            <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-edit"></i></span> 
                <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre" id="nuevoNombre" required>
              </div>
            </div>

            <!-- ENTRADA PARA EL CÓDIGO -->            
            <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-code"></i></span> 
                <input type="text" class="form-control input-lg" id="nuevoCodigo" name="nuevoCodigo" placeholder="Ingresar código"  required>
              </div>
            </div>


            <!-- ENTRADA PARA PRECIO COMPRA -->
            <div class="form-group row">
                <div class="col-xs-12 col-sm-6">                
                  <div class="input-group">                  
                    <span class="input-group-addon"><i class="fa fa-arrow-up"></i></span>
                    <input type="number" class="form-control input-lg" id="nuevoPrecioCompra" name="nuevoPrecioCompra" min="0" step="any" placeholder="Precio de compra" required>
                  </div>
                </div>
  
                <!-- ENTRADA PARA PRECIO VENTA -->
                <div class="col-xs-12 col-sm-6">                
                  <div class="input-group">                  
                    <span class="input-group-addon"><i class="fa fa-arrow-down"></i></span>
                    <input type="number" class="form-control input-lg" id="nuevoPrecioVenta" name="nuevoPrecioVenta" min="0" step="any" placeholder="Precio de venta" required>
                  </div>                
                  <br>

                  <!-- CHECKBOX PARA PORCENTAJE -->
                  <div class="col-xs-6">                    
                    <div class="form-group">                      
                      <label>                        
                        <input type="checkbox" class="minimal porcentaje" checked>
                        Utilizar procentaje
                      </label>
                    </div>
                  </div>

                  <!-- ENTRADA PARA PORCENTAJE -->
                  <div class="col-xs-6" style="padding:0">                    
                    <div class="input-group">                      
                      <input type="number" class="form-control input-lg nuevoPorcentaje" min="0" max="100" value="40" required>
                      <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                    </div>
                  </div>
                </div>
            </div>

            <!-- ENTRADA PARA UNIDAD DE MEDICION -->
            <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoUnidad" min="0" placeholder="Ingregar Unida de Medición" id="nuevoUnidad" required>
              </div>
            </div>

            <!-- ENTRADA PARA PRESENTACION -->
            <div class="form-group">              
              <div class="input-group">             
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoPresentacion" min="0" placeholder="Ingregar presentacion" id="nuevoPresentacion" required>
              </div>
            </div>

            <!-- ENTRADA PARA SUBIR FOTO -->
            <div class="form-group">              
              <div class="panel">SUBIR IMAGEN</div>
              <input type="file" class="nuevaImagen" name="nuevaImagen">
              <p class="help-block">Peso máximo de la imagen 2MB</p>
              <img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">
            </div>

            <!-- ENTRADA PARA LA DESCRIPCIÓN -->
            <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-product-hunt"></i></span>
                <input type="text" class="form-control input-lg" name="nuevoDescripcion" placeholder="Ingresar descripción" id="nuevoDescripcion" required>
              </div>
            </div>

            <!-- ENTRADA PARA SELECCIONAR CATEGORÍA -->          
            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                <select class="form-control input-lg" id="nuevoCategoria" name="nuevoCategoria" required>
                  
                  <option value="">Selecionar categoría</option>

                  <?php
                    $item = null;
                    $valor = null;
                    $categorias = ControladorCategorias::ctrMostrarCategorias($item, $valor);
                      foreach ($categorias as $value) {
                        echo '<option value="'.$value["id_categoria"].'">'.$value["nombre_categoria"].'</option>';
                      }                     
                  ?>
  
                </select>

              </div>

            </div>

            <!-- ENTRADA PARA SELECCIONAR ESTADOS -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-th"></i></span> 

                <select class="form-control input-lg" id="nuevoEstado" name="nuevoEstado" required>
                  
                  <option value="">Selecionar Estado</option>

                  <?php
                    $item = null;
                    $valor = null;
                    $estados = ControladorEstadoProducto::ctrMostrarEstadoProducto($item, $valor);
                      foreach ($estados as $estado) {
                        echo '<option value="'.$estado->id_estado_producto.'">'.$estado->nombre_estado_producto.'</option>';
                      }                   
                  ?>
  
                </select>

              </div>

            </div> 
          </div>      
        </div>
      

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar producto</button>

        </div>


        <?php

          $crearProducto = new ControladorProductos();
          $crearProducto -> ctrCrearProducto();

        ?> 

      </form>
 

    </div>

  </div>

</div>

<!--=====================================
MODAL EDITAR PRODUCTO
======================================-->

<div id="modalEditarProducto" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar producto</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA LA NOMBRE -->

            <div class="form-group">           
            <label>Nombre Producto:</label>   
              <div class="input-group">         
                <span class="input-group-addon"><i class="fa fa-edit"></i> </span>
                <input type="text" class="form-control input-lg" id="editar_nombre" name="editar_nombre" placeholder="Ingresar nombre" required>
                <input type="hidden" id="id_producto" name="id_producto">
              </div>
            </div>

            <!-- ENTRADA PARA EL CÓDIGO -->
            <label>Codigo:</label>   
            <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-code"></i></span> 
                <input type="text" class="form-control input-lg" id="editar_codigo" name="editar_codigo" readonly required>
              </div>
            </div>        


             <!-- ENTRADA PARA PRECIO COMPRA -->
            <div class="form-group row">
                <div class="col-xs-6">                
                  <div class="input-group">                  
                    <span class="input-group-addon"><i class="fa fa-arrow-up"></i></span> 
                    <input type="number" class="form-control input-lg" id="editar_precioCompra" name="editar_precioCompra" step="any" min="0" required>
                  </div>
                </div>

                <!-- ENTRADA PARA PRECIO VENTA -->

                <div class="col-xs-6">                
                  <div class="input-group">                  
                    <span class="input-group-addon"><i class="fa fa-arrow-down"></i></span> 
                    <input type="number" class="form-control input-lg" id="editar_precioVenta" name="editar_precioVenta" step="any" min="0" readonly required>
                  </div>
                
                  <br>

                  <!-- CHECKBOX PARA PORCENTAJE -->
                  <div class="col-xs-6">
                    
                    <div class="form-group">
                      
                      <label>
                        
                        <input type="checkbox" class="minimal porcentaje" checked>
                        Utilizar procentaje
                      </label>

                    </div>
                  </div>

                  <!-- ENTRADA PARA PORCENTAJE -->
                  <div class="col-xs-6" style="padding:0">
                    
                    <div class="input-group">
                      
                      <input type="number" class="form-control input-lg nuevoPorcentaje" min="0" max="100" value="40" required>

                      <span class="input-group-addon"><i class="fa fa-percent"></i></span>

                    </div>
                  </div>
                </div>
            </div>
  

            <!-- ENTRADA PARA MEDICION -->
             <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-check"></i></span> 
                <input type="text" class="form-control input-lg" id="editar_unidadMedicion" name="editar_unidadMedicion" min="0" placeholder="Ingregar Unida de Medición" required>
              </div>
            </div>

            <!-- ENTRADA PARA PRESENTACION -->
             <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-check"></i></span> 
                <input type="text" class="form-control input-lg" id="editar_presentacion" name="editar_presentacion" min="0" placeholder="Ingregar presentacion" required>
              </div>
            </div>


            <!-- ENTRADA PARA LA DESCRIPCIÓN -->
            <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-product-hunt"></i></span> 
                <input type="text" class="form-control input-lg" id="editar_descripcion" name="editar_descripcion" required>
              </div>
            </div>

            <!-- ENTRADA PARA SELECCIONAR CATEGORÍA -->
            <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                <select class="form-control input-lg"  name="editarCategoria" readonly required>                  
                  <option id="editarCategoria"></option>
                </select>
              </div>
            </div>

          
            <!-- ENTRADA PARA SUBIR FOTO -->
            <div class="form-group">              
              <div class="panel">SUBIR IMAGEN</div>
              <input type="file" class="nuevaImagen" name="editarImagen">
              <p class="help-block">Peso máximo de la imagen 2MB</p>
              <img src="vistas/img/productos/default/anonymous.png" class="img-thumbnail previsualizar" width="100px">
              <input type="hidden" name="imagenActual" id="imagenActual">
            </div>

            <!-- ENTRADA PARA EDITAR ESTADO -->
            <div class="form-group">              
              <div class="input-group">              
                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                <select class="form-control input-lg"  name="editarEstado" required>                  
                  <option id="editarEstado"></option>
                  <option value="1">Activo</option>
                  <option value="2">Inactivo</option>
                  <option value="3">Inventario</option>
                </select>
              </div>
            </div>

          </div>

        </div>


        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

                <?php

          $editarProducto = new ControladorProductos();
          $editarProducto -> ctrlEditarProducto();

        ?>   

      </form>

   
  
    </div>


  </div>

</div>

<?php

  $eliminarProducto = new ControladorProductos();
  $eliminarProducto -> ctrEliminarProducto();

?>      



