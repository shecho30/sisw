<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar Proveedores</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li class="active">Administrar Ordenes de Compra</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarOrden">
                    Agregar Orden Compra
                </button>
            </div>

            <div class="box-body">
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px">ver</th>
                            <th style="width:10px">codigo</th>
                            <th>nombre</th>
                            <th>fecha creacion</th>
                            <th>Acciones</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        $item = null;
                        $valor = null;
                        $ordenes = ControladorOrden::ctrMostrarOrden($item, $valor);
                        //print_r($proveedores);
                        foreach ($ordenes as $valor) {
                        echo "<tr>";
                            echo "<td><i class='fa fa-eye' aria-hidden='true'></i></td>";
                            echo "<td>".$valor["id_orden_compra"]."</td>";
                            echo "<td>".$valor["nombre_orden_compra"]."</td>";
                            echo "<td>".$valor["fecha_creacion_orden_compra"]."</td>";
                                echo '<td>
                                    <div class="btn-group">
                                    <button class="btn btn-warning btnEditarProveedor" idProveedor="' . $valor["id_proveedor"] . '" data-toggle="modal" data-target="#modalEditarProveedores"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger btnEliminarProveedor" idProveedor="' . $valor["id_proveedor"] . '"><i class="fa fa-times"></i></button>
                                    </div>  
                                </td>';
                        echo "</tr>";       
                        }
                        ?> 
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- AGREGAR ORDEN-->

<div id="modalAgregarOrden" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Proveedor</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                          <!-- Nombre Orden Compra -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span> 
                                <input type="text" class="form-control input-lg" name="nueva_orden" placeholder="Ingresar Nombre Orden" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                                <select class="form-control input-lg" id="nuevo_proveedor" name="nuevo_proveedor" required>
                                    <option value="">Selecionar Proveedor</option>
                                    <?php
                                    $item = null;
                                    $valor = null;

                                    $proveedores = ControladorProveedor::ctrMostrarProveedores($item, $valor);

                                    foreach ($proveedores as $pro) {

                                        echo '<option value="' . $pro["id_proveedor"] . '">' . $pro["nombre_repre_proveedor"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <div class="input-group">
                                    <?php
                                        // $respuesta = false;
                                        // if(!$respuesta)
                                        // {
                                        //     echo "<p>Agrege producto <a href='' data-toggle='modal' data-target='#modal_producto'><i class='fa fa-plus'></i></a></p>";
                                        // }
                                    ?>
                            </div>                
                        </div> -->
                    
                      
                    </div>
                </div>  

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Orden</button>
                </div>
  

                <?php
                 $crearOrden = new ControladorOrden();
                 $crearOrden->ctrCrearOrden();
                ?>
            </form>
        </div>
    </div>
</div>


<!-- Modal Producto-->
<div id="modal_producto" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Proveedor</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                <form method="POST">
                      <table class="table table-bordered table-striped dt-responsive" id="tabla_producto" width="100%">
                        <thead>
                            <tr>
                                <th>Selecionar</th>
                                <th>Pc</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php
                                $productos = serviceProducto::mostrar_productos();
                                foreach($productos as $pro)
                                {
                                    echo "<tr>";
                                        echo "<td><input type='checkbox' name='productos[]' value ='".$pro['id_producto']."'></td>";
                                        echo "<td>".$pro['nombre_producto']."</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Orden</button>
                </div>
            </form>
  
                <?php
                //  $obj_producto = new ControladorOrden();
                //  $obj_producto->insertarProductos();
                ?>
            </form>
        </div>
    </div>
</div>
