<?php
if ($_SESSION["perfil"] == "Especial") {
    echo '<script>
    window.location = "inicio";
  </script>';
    return;
}
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar ingreso de stock</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Administrar pedido</li>
        </ol>

    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarPedido">
                    Agregar pedido
                </button>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px">#</th>
                            <th>Numero pedido</th>
                            <th>Nombre cliente</th>
                            <th>Nombre producto</th>
                            <th>Fecha creación pedido</th>
                            <th>Cantidad producto</th>
                            <th>Acciones</th>
                        </tr> 
                    </thead>

                    <tbody>
                        <?php
                        $item = null;
                        $valor = null;
                        $pedidos = ControladorPedidos::ctrMostrarPedidos($item, $valor);
	                        foreach ($pedidos as $key => $value) {
	                            echo 
				                '<tr>
				                    <td>' .$value["id_pedido"]. '</td>
				                    <td>' .$value["numero_pedido"]. '</td>
				                    <td>' .$value["nombre_repre_cliente"]. '</td>
				                    <td>' .$value["nombre_producto"]. '</td>
				                    <td>' .$value["fecha_creacion_pedido"]. '</td>
				                    <td>' .$value["cantidad_pedido"]. '</td>';
                                echo 
                                '<td>
                                  <div class="btn-group">
                                    <button class="btn btn-warning btnEditarPedido"  idPedido="'.$value["id_pedido"].'" data-toggle="modal" data-target="#modalEditarPedido"><i class="fa fa-pencil"></i></button>';
                                        if ($_SESSION["perfil"] == "1" || $_SESSION["perfil"] == "Admin") {
                                            echo '<button class="btn btn-danger" id="btnEliminarPedido" idPedido="' . $value["id_pedido"] . '"><i class="fa fa-times"></i></button>';
                                        }
                                        echo '</div>
                                </tr>';
	                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
<!-- AGREGAR PEDIDO -->

<div id="modalAgregarPedido" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post">

                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar pedido</h4>
                </div>


                <div class="modal-body">
                    <div class="box-body">

                         <!-- DOCUMENTO ID -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                                <select class="form-control input-lg" name="newPedido">
                                <option>
                                	Seleccione numero de orden
                                </option>
                                    <?php
                                        $tabla = "tbl_orden_compra";
                                        $item = null;
                                        $valor = null;

                                        $respuesta = ModeloOrden::mdlMostrarOrden($tabla, $item, $valor);
                                        foreach ($respuesta as $key => $value) {
                                            echo '<option value="'.$value["id_orden_compra"].'">'.$value["id_orden_compra"]." ".$value["nombre_orden_compra"].'</option>';
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Cliente -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span> 
                                <select class="form-control input-lg" name="newCliente">
                                <option>
                                	Seleccione proveedor
                                </option>
                                    <?php

                                        $tabla = "tbl_proveedor";
                                        $item = null;
                                        $valor = null;
                                        $respuestap = ModeloProveedor::mdlMostrarProveedor($tabla, $item, $valor);
                                        foreach ($respuestap as $key => $value) {
                                            echo '<option value="'.$value["id_proveedor"].'">'.$value["empresa_proveedor"].'</option>';
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>


                         <!-- Producto -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bell-o"></i></span>
                                <select class="form-control input-lg" name="newProducto">
                                    <option>Seleccione producto</option>
                                    <?php
                                    $item  = null;
                                    $valor = null;
                                    $producto = ControladorPedidos::ctrMostrarProductos($item, $valor);

                                    foreach ($producto as $key => $value) {
                                        echo '<option value="'.$value["id_producto"].'">'.$value["nombre_producto"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Date -->
		               <?php 

                        date_default_timezone_set('America/Bogota');

                        $fechaActual = date("Y-m-d H:i:s");

                        ?>
                        <div class="form-group">
                        	<div class="input-group">
                        		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                                <input type="text" name="newDate" class="form-control input-lg" value="<?= $fechaActual?>" readonly>
                            </div>
                        </div>

                        <!-- Cantidad producto -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 
                                <input type="number" class="form-control input-lg" id="newCantidad" name="newCantidad" placeholder="Ingrese la cantidad de productos" value="" required>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Pedido</button>
                </div>
            </form>

            <?php
            $crearPedido = new ControladorPedidos();
            $crearPedido->ctrCrearPedido();
            ?>
        </div>
    </div>
</div>

<!-- EDITAR PEDIDO -->

<div id="modalEditarPedido" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post">

                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar pedido</h4>
                </div>


                <div class="modal-body">
                    <div class="box-body">

                         <!-- INGRESAR EL NUMERO DE PEDIDO-->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                                <input type="hidden" name="id_pedido" id="id_pedido" value="">
                                <input type="number" class="form-control input-lg" name="editPedido" id="editPedido" placeholder="Ingresar numero pedido" value="" required>
                            </div>
                        </div>

                        <!-- Cliente -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span> 
                                <select class="form-control input-lg" name="editCliente">
                                    <option id="editCliente" value=""></option>
                                    <?php
                                        $item = null;
                                        $valor = null;
                                        $pedidos = ControladorPedidos::ctrMostrarClientes($item, $valor);
                                        foreach ($pedidos as $key => $value) {
                                            echo '<option value="'.$value["id_cliente"].'">'.$value["nombre_repre_cliente"].'</option>';
                                        }

                                    ?>
                                </select>
                            </div>
                        </div>


                         <!-- Producto -->

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bell-o"></i></span>
                                <select class="form-control input-lg" name="editProducto">
                                    <option id="editProducto" value=""></option>
                                    <?php
                                    $item  = null;
                                    $valor = null;
                                    $producto = ControladorPedidos::ctrMostrarProductos($item, $valor);

                                    foreach ($producto as $key => $value) {
                                        echo '<option value="'.$value["id_producto"].'">'.$value["nombre_producto"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Cantidad producto -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span> 
                                <input type="number" class="form-control input-lg" id="editCantidad" name="editCantidad" placeholder="Ingrese la cantidad de productos" value="" required>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Editar Pedido</button>
                </div>
            </form>

            <?php
            $editarPedido = new ControladorPedidos();
            $editarPedido->ctrEditarPedido();
            ?>
        </div>
    </div>
</div>
<?php
$eliminarPedido = new ControladorPedidos();
$eliminarPedido->ctrEliminarPedido();
?>
