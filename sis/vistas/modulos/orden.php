<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar Orden Compra</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li class="active">Administrar Ordenes de Compra</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarOrden">
                    Agregar Orden Compra
                </button>
            </div>

            <div class="box-body">
                <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
                    <thead>
                        <tr>
                            <th style="width:10px">ver</th>
                            <th style="width:10px">codigo</th>
                            <th>nombre</th>
                            <th>fecha creacion</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $item = null; $valor = null;
                        $ordenes = ControladorOrden::ctrMostrarOrden($item, $valor);
                        foreach ($ordenes as $valor)
                        {
                        echo "<tr>";
                            echo "<td><button class='btn btn-default view_orden' idOrden='".$valor["id_orden_compra"]."' data-toggle='modal' data-target='#modal_view'><i class='fa fa-eye' aria-hidden='true'></i></button></td>";
                            echo "<td>".$valor["id_orden_compra"]."</td>";
                            echo "<td>".$valor["nombre_orden_compra"]."</td>";
                            echo "<td>".$valor["fecha_creacion_orden_compra"]."</td>";
                                echo '<td>
                                    <div class="btn-group">
                                    <button class="btn btn-primary btnAgregarProductos" idOrden="' . $valor["id_orden_compra"] . '" data-toggle="modal" data-target="#modal_producto"><i class="fa fa-plus"></i></button>
                                    <button class="btn btn-warning btnEditarOrden" idOrden="' . $valor["id_orden_compra"] . '" data-toggle="modal" data-target="#modalEditarOrden"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-danger btnEliminarOrden" idOrden="' . $valor["id_orden_compra"] . '"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>';
                        echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<!-- AGREGAR ORDEN-->

<div id="modalAgregarOrden" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Orden de Compra</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                          <!-- Nombre Orden Compra -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="text" class="form-control input-lg" name="nueva_orden" placeholder="Ingresar Nombre Orden" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                <select class="form-control input-lg" id="nuevo_proveedor" name="nuevo_proveedor" required>
                                    <option value="">Seleccionar Proveedor</option>
                                    <?php
                                    $item = null;
                                    $valor = null;

                                    $proveedores = ControladorProveedor::ctrMostrarProveedores($item, $valor);

                                    foreach ($proveedores as $pro) {

                                        echo '<option value="' . $pro["id_proveedor"] . '">' . $pro["nombre_repre_proveedor"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <!-- id_orden Detalle  -->
                        <div class="form-group">
                           <div class="input-group">
                             <textarea name="nuevo_detalle" id="nuevo_nuevo_detalle" rows="8" cols="80" class="form-control input-lg" placeholder="Ingresar el detalle de la orden de compra" required></textarea>
                           </div>
                       </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Orden</button>
                </div>


                <?php
                 $crearOrden = new ControladorOrden();
                 $crearOrden->ctrCrearOrden();
                ?>
            </form>
        </div>
    </div>
</div>


<!-- Modal Producto-->
<div id="modal_producto" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Proveedor</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                <form method="POST">
                    <input type="hidden" id="id_orden_insert" name="nuevo_id_orden">
                      <table class="table table-bordered table-striped dt-responsive" id="tabla_producto" width="100%">
                        <thead>
                            <tr>
                                <th>Selecionar</th>
                                <th>Pc</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $item = null;
                            $valor = null;
                            $productos = ControladorProductos::ctrMostrarProductosOrden($item,$valor);
                            // print_r($productos);
                                // $productos = serviceProducto::mostrar_productos();
                                 foreach($productos as $pro)
                                 {
                                     echo "<tr>";
                                         echo '<td><a class="btn btn-success insert_pro" nombre_producto="'.$pro["nombre_producto"].'" id_producto="' . $pro["id_producto"] . '" data-toggle="modal" data-target="#modal_orden_producto"><i class="fa fa-plus"></i></a></td>';
                                         echo "<td>".$pro['nombre_producto']."</td>";
                                     echo "</tr>";
                                 }
                            ?>
                        </tbody>
                    </table>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Orden</button>
                </div>
            </form>

                <?php
                //  $obj_producto = new ControladorOrden();
                //  $obj_producto->insertarProductos();
                ?>
            </form>
        </div>
    </div>
</div>


<!-- Modal Orden Producto-->
<div id="modal_orden_producto" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Producto</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                    <!-- id_orden Compra  -->
                     <!-- id_orden Producto  -->
                    <div class="form-group">
                        <div class="input-group">
                            <input type="hidden" class="form-control input-lg" name="insert_id_producto" id="id_orden_producto_insert" placeholder="Ingresar Nombre Orden" required>
                            <input type="hidden" class="form-control input-lg" name="insert_id_orden" id="id_orden_compra_insert" placeholder="Ingresar Nombre Orden" required>
                        </div>
                    </div>

                    <!-- id_orden Precio  -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="number" class="form-control input-lg" name="nuevo_precio" id="nuevo_precio" placeholder="Ingresar precio Producto" required>
                        </div>
                    </div>

                     <!-- id_orden Cantidad  -->
                     <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="number" class="form-control input-lg" name="nuevo_cantidad" id="nuevo_nuevo_cantidad" placeholder="Ingresar cantidad Producto" required>
                        </div>
                    </div>

                    <!-- id_orden Detalle  -->
                    <!--<div class="form-group">
                       <div class="input-group">
                         <textarea name="nuevo_detalle" id="nuevo_nuevo_detalle" rows="8" cols="80" class="form-control input-lg" placeholder="Ingresar el detalle de la orden de compra" required></textarea>
                       </div>
                   </div>-->

                </div>
            </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Orden</button>
                </div>

                <?php
                 $obj_producto = new ControladorOrden();
                  $obj_producto->ctrCrearOrdenProducto();
                ?>
            </form>
        </div>
    </div>
</div>



<!-- Modal View Orden-->
<div id="modal_view" class="modal fade bs-example-modal-lg" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- <form role="form" method="post" enctype="multipart/form-data"> -->


                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Orden de Compra</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">


                      <input type="hidden" id="idOrdenPdf" name="idOrdenPdf"></input>
                     <!-- Nombre Orden Compra -->
                        <div class="form-group">
                            <div class="input-group">
                                <label for="name" id="nombreOrden"></label>
                            </div>
                        </div>

                    <!-- Nombre Proveedor Compra -->
                         <div class="form-group">
                            <div class="input-group">
                                <label for="name" id="nombreProv"></label>
                            </div>
                        </div>

                    <!--Tabla productos-->
                        <!-- <div class="form-group">
                            <div class="input-group"> -->
                               <table class="table table-bordered" id="table_view" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>
                                                Nombre
                                            </th>
                                            <th>
                                                precio
                                            </th>
                                            <th>
                                                cantidad
                                            </th>
                                            <th>
                                                total
                                            </th>
                                            <th>
                                                Acciones
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="tabla_orden_view">
                                    </tbody>
                               </table>
                            <!-- </div>
                        </div> -->
                        <br>
                        <div class="row">
                            <label for="name" class="col-md-offset-8 col-md-4"id="TotalPagar"></label>
                        </div>


                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <!--<a href="extensiones/tcpdf/pdf/ordenCompra.php" class="btn btn-primary ">Descargar</a>-->
                    <button type="button" id="descargarPdf" class="btn btn-primary ocultarPdf" name="button" target="_blank">Descargar</button>
                </div>

                <?php
                $obj=new ControladorOrden();
                $obj->ctrEliminarOrdenProducto();
                ?>
            <!-- </form> -->
        </div>
    </div>
</div>



<!-- EDITAR ORDEN-->

<div id="modalEditarOrden" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Orden de Compra</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">


                        <!-- Nombre Orden Compra -->
                        <div class="form-group">
                                <input type="hidden" id="nuevo_id_edit" name="nuevo_id_edit">
                        </diV>

                          <!-- Nombre Orden Compra -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="text" class="form-control input-lg" name="nueva_orden_edit" id="nueva_orden_edit" placeholder="Ingresar Nombre Orden" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                <select class="form-control input-lg" id="nuevo_proveedor_edit" name="nuevo_proveedor_edit" required>
                                    <!-- <option value="">Selecionar Proveedor</option> -->
                                    <?php
                                    $item = null;
                                    $valor = null;

                                    $proveedores = ControladorProveedor::ctrMostrarProveedores($item, $valor);

                                    foreach ($proveedores as $pro) {

                                        echo '<option value="' . $pro["id_proveedor"] . '">' . $pro["nombre_repre_proveedor"] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                          <!-- id_orden Detalle  -->
                          <div class="form-group">
                           <div class="input-group">
                             <textarea name="edit_nuevo_detalle" id="edit_nuevo_detalle" rows="8" cols="80" class="form-control input-lg" placeholder="Ingresar el detalle de la orden de compra" required></textarea>
                           </div>
                       </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Orden</button>
                </div>


                <?php
                 $EditarOrden = new ControladorOrden();
                 $EditarOrden->ctrEditarOrden();
                ?>
            </form>
        </div>
    </div>
</div>



<!-- Modal Editar producto-->
<div id="modal_editar" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" enctype="multipart/form-data">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Producto</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">

                    <!-- id_orden Compra  -->
                     <!-- id_orden Producto  -->
                    <div class="form-group">
                        <div class="input-group">
                            <input type="hidden" class="form-control input-lg" name="_id" id="_id" placeholder="Ingresar Nombre Orden" required>
                        </div>
                    </div>

                    <!-- id_orden Precio  -->
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="number" class="form-control input-lg" name="_nuevo_precio" id="_nuevo_precio" placeholder="Ingresar precio Producto" required>
                        </div>
                    </div>

                     <!-- id_orden Cantidad  -->
                     <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input type="number" class="form-control input-lg" name="_nuevo_cantidad" id="_nuevo_cantidad" placeholder="Ingresar cantidad Producto" required>
                        </div>
                    </div>

                </div>
            </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar Orden</button>
                </div>

                <?php
                   $obj_producto = new ControladorOrden();
                   $obj_producto->ctrCrearOrdenProEditar();
                ?>
            </form>
        </div>
    </div>
</div>

<?php
    $borrar_proveedor = new ControladorOrden();
    $borrar_proveedor->ctrEliminarOrdenCompra();
?>
