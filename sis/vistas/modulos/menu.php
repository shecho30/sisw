<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?php
            if ($_SESSION["perfil"] == 1) {
                echo '<li class="active">
				<a href="inicio">
					<i class="fa fa-home"></i>
					<span>Inicio</span>
				</a>
			</li>
			<li>
				<a href="usuarios">
					<i class="fa fa-user"></i>
					<span>Usuarios</span>
				</a>

			</li>';
            
            
                echo '<li>
				<a href="categorias">
					<i class="fa fa-th"></i>
					<span>Categorías</span>
				</a>
			</li>';

			echo '<li class="treeview">
					<a href="#">
						<i class="fa fa-product-hunt"></i>
						<span>Productos</span>
						<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
					</a>
				 
						<ul class="treeview-menu">
							<li>
								<a href="productos">
									<i class="fa fa-circle-o"></i>
									<span>Productos</span>
								</a>
								
							</li>
							<li>
								<a href="promocion">
									<i class="fa fa-circle-o"></i>
									<span>Promocion</span>
								</a>
								
							</li>

						
			</ul>
			</li>';}
			echo '<li class="treeview">
			<a href="#">
				<i class="fa fa-users"></i>
				<span>Perfiles</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>

			<ul class="treeview-menu">
				<li>
					<a href="clientes">
						<i class="fa fa-circle-o"></i>
						<span>Clientes</span>
					</a>
				</li>';

				if ($_SESSION["perfil"] == 1 || $_SESSION["perfil"] == "Especial") {				
					echo'
				<li>
					<a href="proveedores">
						<i class="fa fa-circle-o"></i>
						<span>Proveedores</span>
					</a>
				</li>';
			}

			echo '</ul>
		

            
                <li class="treeview">
				<a href="#">
					<i class="fa fa-list-ul"></i>
					<span>Ventas</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				
				<ul class="treeview-menu">
					<li>
						<a href="ventas">
							<i class="fa fa-circle-o"></i>
							<span>Administrar ventas</span>
						</a>
					</li>
					<li>
						<a href="crear-venta">
							<i class="fa fa-circle-o"></i>
							<span>Crear venta</span>
						</a>
					</li>';
			if ($_SESSION["perfil"] == 1 || $_SESSION["perfil"] == "Vendedor") {
			echo	'<li>
						<a href="formapago">
							<i class="fa fa-circle-o"></i>
							<span>Forma Pago</span>
						</a>
					</li>
					<li>
						<a href="crear-pedido">
							<i class="fa fa-circle-o"></i>
							<span>Gestionar orden</span>
						</a>
					</li>';

					if($_SESSION["perfil"] ==1 )
					{
					echo '<li>
							<a href="orden">
								<i class="fa fa-circle-o"></i>
								<span>Orden de Compra</span>
							</a>
						  </li>';
					}

                if ($_SESSION["perfil"] == 1) {
                    echo '<li>
						<a href="reportes">
							<i class="fa fa-circle-o"></i>
							<span>Reporte de ventas</span>
						</a>
					</li>';
                }

                echo '</ul>
			</li>';
            }
            ?>
        </ul>
    </section>
</aside>