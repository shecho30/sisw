<?php
if ($_SESSION["perfil"] == "Vendedor") {
    echo '<script>
    window.location = "inicio";
  </script>';
    return;
}
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>Administrar categorías</h1>

        <ol class="breadcrumb">
            <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>      
            <li class="active">Administrar categorías</li>    
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">  
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarCategoria">          
                    Agregar categoría
                </button>
            </div>

            <div class="box-body">        
                <table class="table table-bordered table-striped dt-responsive tablaCategorias" width="100%">
                    <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <?php
                        $item = null;
                        $valor = null;
                        $categorias = ControladorCategorias::ctrMostrarCategorias($item, $valor);
                        //print_r($categorias);
                        foreach ($categorias as $valor) {
                            echo '<tr>';
                            echo '<td>'.$valor["id_categoria"].'</td>';
                            echo '<td>'.$valor["nombre_categoria"].'</td>';
                            echo '<td>'.$valor["fecha_creacion_categoria"].'</td>';
                            echo '<td>
                            <div class="btn-group">
                              <button class="btn btn-warning btnEditarCategoria" idCategoria="' . $valor["id_categoria"] . '" data-toggle="modal" data-target="#modalEditarCategoria"><i class="fa fa-pencil"></i></button>
                              <button class="btn btn-danger btnEliminarCategoria" idCategoria="' . $valor["id_categoria"] .  '"><i class="fa fa-times"></i></button>
                            </div>  
                          </td>';
                            echo '</tr>';
                     
                        }
                        ?>
                    </tbody>
                </table>    
            </div>
        </div>
    </section>
</div>

<!-- AGREGAR CATEGORÍA -->

<div id="modalAgregarCategoria" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar categoría</h4>
                </div>

                <div class="modal-body">
                    <div class="box-body">
                        <!--  NOMBRE -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                                <input type="text" class="form-control input-lg" name="nuevaCategoria" placeholder="Ingresar categoría" required>
                            </div>
                        </div>

                        <!--Descripcion-->
                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="des_categoria" class="form-control input-lg" ></textarea>
                            </div>
                        </div>


                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar categoría</button>
                </div>
                <?php
                $crearCategoria = new ControladorCategorias();
                $crearCategoria->ctrCrearCategoria();
                ?>
            </form>
        </div>
    </div>
</div>

<!-- EDITAR CATEGORÍA -->

<div id="modalEditarCategoria" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Editar categoría</h4>
                </div>


                <div class="modal-body">
                    <div class="box-body">
                        <!-- NOMBRE -->
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-th"></i></span> 
                                <input type="text" class="form-control input-lg" name="editarCategoria" id="editarCategoria" required>
                                <input type="hidden"  name="idCategoria" id="idCategoria" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <textarea name="editarDescipcion" id="editarDescipcion" class="form-control input-lg" ></textarea>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">Guardar cambios</button>
                </div>

<?php
$editarCategoria = new ControladorCategorias();
$editarCategoria->ctrEditarCategoria();
?> 
            </form>
        </div>
    </div>
</div>

<?php
$borrarCategoria = new ControladorCategorias();
$borrarCategoria->ctrBorrarCategoria();
?>