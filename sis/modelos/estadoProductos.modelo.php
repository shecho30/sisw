<?php

require_once "conexion.php";

class ModeloEstadosProductos{

  /*=============================================
  MOSTRAR PRODUCTOS
  =============================================*/
  static public function mdlMostrarEstadoProductos($tabla, $item, $valor){

    if($item != null){

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$valor ");

      $stmt -> bindParam(":".$valor, $valor, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetch(PDO::FETCH_OBJ);

    }else{

      $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

      $stmt -> execute();

      return $stmt -> fetchAll(PDO::FETCH_OBJ);

    }

    $stmt -> close();

    $stmt = null;
  }

  /*=============================================
  ACTUALIZAR ESTADO PRODUCTO
  =============================================*/
  static public function mdlActualizarEstadoProductos($tabla, $datos){
    $sql = "UPDATE $tabla SET nombre_estado_producto = :nombre_estado_producto WHERE id_estado_producto = :id_estado_producto";
    $stmt = Conexion::conectar()->prepare($sql);
    $stmt->bindParam(':id_estado_producto',$datos['id_estado_producto'], PDO::PARAM_INT);
    $stmt->bindParam(':nombre_estado_producto',$datos['nombre_estado_producto'], PDO::PARAM_STR);
    $result = $stmt->execute();
    return $result;
  }


  /*=============================================
  REGISTRO DE PRODUCTO
  =============================================*/
  static public function mdlIngresarEstadoProducto($tabla, $datos){

    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (nombre_estado_producto) VALUES (:nombre_estado_producto)");

    
    $stmt->bindParam(":nombre_estado_producto", $datos, PDO::PARAM_STR);
    

    if($stmt->execute()){

      return "ok";

    }else{

      return "error";
    
    }

    $stmt->close();
    $stmt = null;
  }

  /*=============================================
  BORRAR ESTADO PRODUCTO
  =============================================*/

  static public function mdlEliminarEstadoProducto($tabla, $datos){

    $stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_estado_producto = :id");

    $stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

    if($stmt -> execute()){

      return "ok";
    
    }else{

      return "error"; 

    }

    $stmt -> close();

    $stmt = null;
  }

}