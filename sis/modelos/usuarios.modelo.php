<?php

require_once "conexion.php";

class ModeloUsuarios
{

	/*=============================================
	MOSTRAR USUARIOS
	=============================================*/

	static public function mdlMostrarUsuarios($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	static public function mdlIngresarUsuario($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre_usuario, apellido_usuario, nick_user, correo_usuario, contrasena_usuario,fecha_creacion_usuario,imagen_usuario,id_estado_usuario,id_tipo_usuario) VALUES (:nombre_usuario, :apellido_usuario, :nick_user, :correo_usuario, :contrasena_usuario,sysdate(),:imagen_usuario,:id_estado_usuario,:id_tipo_usuario)");

		$stmt->bindParam(":nombre_usuario", $datos["nombre_usuario"], PDO::PARAM_STR);
		$stmt->bindParam(":apellido_usuario", $datos["apellido_usuario"], PDO::PARAM_STR);
		$stmt->bindParam(":nick_user", $datos["nick_user"], PDO::PARAM_STR);
		$stmt->bindParam(":correo_usuario", $datos["correo_usuario"], PDO::PARAM_STR);
		$stmt->bindParam(":contrasena_usuario", $datos["contrasena_usuario"], PDO::PARAM_STR);
		$stmt->bindParam(":imagen_usuario", $datos["imagen_usuario"], PDO::PARAM_STR);
		$stmt->bindParam(":id_estado_usuario", $datos["id_estado_usuario"], PDO::PARAM_STR);
		$stmt->bindParam(":id_tipo_usuario", $datos["id_tipo_usuario"], PDO::PARAM_STR);
		
		if($stmt->execute()){

			return "ok";	

		}else{

			return "error";
		
		}

		$stmt->close();
		
		$stmt = null;

	}

	/*=============================================
	EDITAR USUARIO
	=============================================*/

	static public function mdlEditarUsuario($tabla, $datos){
	
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre_usuario = :nombre_usuario,
																 apellido_usuario = :apellido_usuario,
																 nick_user = :nick_user, 
																 imagen_usuario = :imagen_usuario,
																 correo_usuario = :correo_usuario,
																 contrasena_usuario = :contrasena_usuario,
																 id_estado_usuario = :id_estado_usuario,
																 id_tipo_usuario = :id_tipo_usuario
																 WHERE id_usuario = :id_usuario");

		$stmt -> bindParam(":nombre_usuario", $datos["nombre_usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido_usuario", $datos["apellido_usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":nick_user", $datos["nick_user"], PDO::PARAM_STR);
		$stmt -> bindParam(":imagen_usuario", $datos["imagen_usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":correo_usuario", $datos["correo_usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":contrasena_usuario", $datos["contrasena_usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_estado_usuario", $datos["id_estado_usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_tipo_usuario", $datos["id_tipo_usuario"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_usuario", $datos["id_usuario"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR USUARIO
	=============================================*/

	static public function mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	BORRAR USUARIO
	=============================================*/

	static public function mdlBorrarUsuario($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_usuario = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;


	}

}