<?php

 class Conexion
{
    public function conectar()
    {
        $driver    = "mysql";
        $host      = "localhost";
        $user      = "root";
        $pass      = "";
        $database  = "db_sisprova_qa";
        $charset   = "utf8";
        $key       = "150";

        try 
        {
            //echo "$this->driver:host=$this->host;dbname=$this->database;charset=$this->charset";
            $con = new PDO("$driver:host=$host;dbname=$database;charset=$charset",$user,$pass);
            $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $con;
        }
        catch(Exception $e) 
        {
            echo("Error ".$e->getMessage());
        }
    }  

}
    /*
    private $driver, $host, $user, $pass, $database, $charset;

        public function __construct() 
        {
            $db_cfg = require_once(__DIR__.'\..\config\dbConfig.php');

            $this->driver   =$db_cfg["driver"];
            $this->host     =$db_cfg["host"];
            $this->user     =$db_cfg["user"];
            $this->pass     =$db_cfg["pass"];
            $this->database =$db_cfg["database"];
            $this->charset  =$db_cfg["charset"];
            //echo "$this->driver $this->host $this->user $this->pass $this->database $this->charset";
            
            
        }

        public function get_Con()
        {
            try 
                {
                    //echo "$this->driver:host=$this->host;dbname=$this->database;charset=$this->charset";
                    $con = new PDO("$this->driver:host=$this->host;dbname=$this->database;charset=$this->charset",$this->user,$this->pass);
                    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    return $con;
                }
            catch(Exception $e) 
                {
                    echo("Error ".$e->getMessage());
                }
           
        }
 }

*/
 ?>
