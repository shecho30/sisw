<?php

require_once "conexion.php";

class ModeloClientes{

	/*=============================================
	CREAR CLIENTE
	=============================================*/

	static public function mdlIngresarCliente($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(document_cliente, id_tipo_documento, empresa_cliente, nombre_repre_cliente, apellido_repre_cliente, telefono_cliente,direccion_cliente,correo_cliente, contrasena_cliente, id_estado_usuario, fecha_creacion_cliente, id_ciudad) VALUES (:document_cliente, :id_tipo_documento, :empresa_cliente, :nombre_repre_cliente, :apellido_repre_cliente, :telefono_cliente, :direccion_cliente, :correo_cliente, :contrasena_cliente, :id_estado_usuario, :fecha_creacion_cliente, :id_ciudad)");

		$stmt->bindParam(":document_cliente",       $datos["document_cliente"],       PDO::PARAM_STR);
		$stmt->bindParam(":id_tipo_documento",      $datos["id_tipo_documento"],      PDO::PARAM_INT);
		$stmt->bindParam(":empresa_cliente",        $datos["empresa_cliente"],        PDO::PARAM_STR);
		$stmt->bindParam(":nombre_repre_cliente",   $datos["nombre_repre_cliente"],   PDO::PARAM_STR);
		$stmt->bindParam(":apellido_repre_cliente", $datos["apellido_repre_cliente"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono_cliente",       $datos["telefono_cliente"],       PDO::PARAM_STR);
		$stmt->bindParam(":direccion_cliente",      $datos["direccion_cliente"],      PDO::PARAM_STR);
		$stmt->bindParam(":correo_cliente",         $datos["correo_cliente"],         PDO::PARAM_STR);
		$stmt->bindParam(":contrasena_cliente",     $datos["contrasena_cliente"],     PDO::PARAM_STR);
		$stmt->bindParam(":id_estado_usuario",      $datos["id_estado_usuario"],      PDO::PARAM_STR);
		$stmt->bindParam(":fecha_creacion_cliente", $datos["fecha_creacion_cliente"], PDO::PARAM_STR);
		$stmt->bindParam(":id_ciudad",              $datos["id_ciudad"],              PDO::PARAM_STR);



		var_dump($datos);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	MOSTRAR CLIENTES
	=============================================*/


	static public function mdlMostrarClientes($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT t1.id_cliente, t1.id_tipo_documento, t1.document_cliente, t1.empresa_cliente, t1.nombre_repre_cliente, t1.apellido_repre_cliente, 
															t1.telefono_cliente,t1.direccion_cliente, t1.correo_cliente, t1.contrasena_cliente, t1.id_estado_usuario,
															t1.fecha_creacion_cliente,t2.nombre_ciudad, t2.id_ciudad, t3.nombre_tipo_documento, t4.nombre_estado_usuario
														 FROM $tabla t1 INNER JOIN tbl_ciudad t2 ON t1.id_ciudad = t2.id_ciudad 
														 INNER JOIN tbl_tipo_documento t3 ON t1.id_tipo_documento = t3.id_tipo_documento 
														 INNER JOIN tbl_estado_usuario t4 ON t1.id_estado_usuario = t4.id_estado_usuario 
														 WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			
			$stmt -> execute();

			$result = $stmt -> fetch(PDO::FETCH_OBJ);
			return $result;

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT t1.id_cliente, t1.id_tipo_documento, t1.document_cliente, t1.empresa_cliente, t1.nombre_repre_cliente, t1.apellido_repre_cliente, 
															t1.telefono_cliente,t1.direccion_cliente, t1.correo_cliente, t1.contrasena_cliente, t1.id_estado_usuario,
															t1.fecha_creacion_cliente,t2.nombre_ciudad, t2.id_ciudad, t3.nombre_tipo_documento, t4.nombre_estado_usuario
														 FROM $tabla t1 INNER JOIN tbl_ciudad t2 ON t1.id_ciudad = t2.id_ciudad 
														 INNER JOIN tbl_tipo_documento t3 ON t1.id_tipo_documento = t3.id_tipo_documento 
														 INNER JOIN tbl_estado_usuario t4 ON t1.id_estado_usuario = t4.id_estado_usuario");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR ids para poder almacenarlos
	=============================================*/
	static public function mdlMostrarDocumento($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR ids de los usuarios para poder almacenarlos
	=============================================*/
	static public function mdlEstadoUsuario($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}
	
	
	/*=============================================
	MOSTRAR Ciudades
	=============================================*/

	static public function mdlMostrarCiudad($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = ".$item);

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	EDITAR CLIENTE
	=============================================*/

	static public function mdlEditarCliente($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET document_cliente = :document_cliente, id_tipo_documento = :id_tipo_documento, empresa_cliente = :empresa_cliente, nombre_repre_cliente = :nombre_repre_cliente, apellido_repre_cliente = :apellido_repre_cliente, telefono_cliente = :telefono_cliente, direccion_cliente = :direccion_cliente, correo_cliente = :correo_cliente, contrasena_cliente = :contrasena_cliente, id_estado_usuario = :id_estado_usuario, id_ciudad = :id_ciudad WHERE  id_cliente = :id_cliente");

		$stmt->bindParam(":id_cliente",             $datos["id_cliente"],             PDO::PARAM_INT);
		$stmt->bindParam(":document_cliente",       $datos["document_cliente"],       PDO::PARAM_INT);
		$stmt->bindParam(":id_tipo_documento",      $datos["id_tipo_documento"],      PDO::PARAM_INT);
		$stmt->bindParam(":empresa_cliente",        $datos["empresa_cliente"],        PDO::PARAM_STR);
		$stmt->bindParam(":nombre_repre_cliente",   $datos["nombre_repre_cliente"],   PDO::PARAM_STR);
		$stmt->bindParam(":apellido_repre_cliente", $datos["apellido_repre_cliente"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono_cliente",       $datos["telefono_cliente"],       PDO::PARAM_INT);
		$stmt->bindParam(":direccion_cliente",      $datos["direccion_cliente"],      PDO::PARAM_STR);
		$stmt->bindParam(":correo_cliente",         $datos["correo_cliente"],         PDO::PARAM_STR);
		$stmt->bindParam(":contrasena_cliente",     $datos["contrasena_cliente"],     PDO::PARAM_STR);
		$stmt->bindParam(":id_estado_usuario",      $datos["id_estado_usuario"],      PDO::PARAM_INT);
		$stmt->bindParam(":id_ciudad",              $datos["id_ciudad"],              PDO::PARAM_INT);

		echo json_encode($datos);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	ELIMINAR CLIENTE
	=============================================*/

	static public function mdlEliminarCliente($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_cliente = :id_cliente");

		$stmt -> bindParam(":id_cliente", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR CLIENTE
	=============================================*/

	static public function mdlActualizarCliente($tabla, $item1, $valor1, $item2, $valor2){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$valor1 WHERE $item2 = :$valor2");

		$stmt -> bindParam(":".$valor1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$valor2, $valor2, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

	
	/*=============================================
	MOSTRAR CLIENTES PARA ACTUALIZAR VENTAS
	=============================================*/

	static public function mdlMostrarClientesV($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");


			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}


	/*=============================================
	ACTUALIZAR CLIENTE PARA LA VENTA
	=============================================*/

	static public function mdlActualizarClienteV($tabla, $item1, $valor1, $valor){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE id_cliente = :id");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

}

?>