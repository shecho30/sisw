<?php

require_once "conexion.php";

class ModeloProveedor
{

	/*=============================================
	MOSTRAR PROVEEDORES
	=============================================*/

    static public function mdlMostrarProveedor($tabla, $item, $valor)
    {

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		

		$stmt -> close();

		$stmt = null;

	}
	
		/*=============================================
	REGISTRO DE USUARIO
	=============================================*/

	static public function mdlIngresarProveedor($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(documento_proveedor,id_tipo_documento,empresa_proveedor,nombre_repre_proveedor,
																  apellido_repre_proveedor,telefono_proveedor,direccion_proveedor,correo_proveedor,
																  imagen_proveedor,fecha_creacion_proveedor,id_ciudad,id_producto) 
														  VALUES (:documento_proveedor,:id_tipo_documento,:empresa_proveedor,:nombre_repre_proveedor,
																  :apellido_repre_proveedor,:telefono_proveedor,:direccion_proveedor,:correo_proveedor,
																  NULL,SYSDATE(),:id_ciudad,:id_producto)");

		$stmt->bindParam(":documento_proveedor", $datos["numero_documento"], PDO::PARAM_STR);
		$stmt->bindParam(":id_tipo_documento",   $datos["tipo_documento"], PDO::PARAM_INT);
		$stmt->bindParam(":empresa_proveedor",   $datos["nombre_empresa"], PDO::PARAM_STR);
		$stmt->bindParam(":nombre_repre_proveedor",   $datos["nombre_repre"], PDO::PARAM_STR);
		$stmt->bindParam(":apellido_repre_proveedor", $datos["apellido_repre"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono_proveedor",  $datos["telefono_repre"], PDO::PARAM_STR);
		$stmt->bindParam(":direccion_proveedor", $datos["direccion_empresa"], PDO::PARAM_STR);
		$stmt->bindParam(":correo_proveedor",  $datos["correo_repre"], PDO::PARAM_STR);
		$stmt->bindParam(":id_ciudad",   $datos["ciudad_proveedor"], PDO::PARAM_STR);
		$stmt->bindParam(":id_producto", $datos["producto_proveedor"], PDO::PARAM_STR);

		//print_r($stmt);
		
		if($stmt->execute())
		{
			return "ok";	
		}
		else
		{
			return "error";
		}

		$stmt->close();
		
		$stmt = null;

	}

	/*=============================================
	BORRAR PROVEEDOR
	=============================================*/

	static public function mdlBorrarProveedor($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_proveedor = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;


	}


	/*=============================================
	EDITAR PROVEEDOR
	=============================================*/

	static public function mdlEditarProveedor($tabla, $datos){
	
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET id_tipo_documento = :id_tipo_documento,
																 documento_proveedor = :documento_proveedor,
																 empresa_proveedor = :empresa_proveedor, 
																 nombre_repre_proveedor = :nombre_repre_proveedor,
																 apellido_repre_proveedor = :apellido_repre_proveedor,
																 correo_proveedor = :correo_proveedor,
																 telefono_proveedor = :telefono_proveedor,
																 direccion_proveedor = :direccion_proveedor,
																 id_ciudad = :id_ciudad,
																 id_producto = :id_producto,
																 imagen_proveedor = :imagen_proveedor
																 WHERE id_proveedor = :id_proveedor");

		$stmt -> bindParam(":id_tipo_documento", $datos["nuevoTiDocuEdit"], PDO::PARAM_INT);
		$stmt -> bindParam(":documento_proveedor", $datos["nuevoNuDocuEdit"], PDO::PARAM_STR);
		$stmt -> bindParam(":empresa_proveedor", $datos["nuevoNombreEmpEdit"], PDO::PARAM_STR);
		$stmt -> bindParam(":nombre_repre_proveedor", $datos["nuevoNombreEdit"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido_repre_proveedor", $datos["nuevoApellidoEdit"], PDO::PARAM_STR);
		$stmt -> bindParam(":correo_proveedor", $datos["nuevoCorreoEdit"], PDO::PARAM_STR);
		$stmt -> bindParam(":telefono_proveedor", $datos["nuevoTelefonoEdit"], PDO::PARAM_STR);
		$stmt -> bindParam(":direccion_proveedor", $datos["nuevoDirEdit"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_ciudad", $datos["nuevoCiudadEdit"], PDO::PARAM_INT);
		$stmt -> bindParam(":id_producto", $datos["nuevoProEdit"], PDO::PARAM_INT);
		$stmt -> bindParam(":imagen_proveedor", $datos["nuevaFotoEdit"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_proveedor", $datos["idProveedor"], PDO::PARAM_INT);

		//print_r($stmt);
		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}
}
?>