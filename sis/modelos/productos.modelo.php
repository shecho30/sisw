<?php

require_once "conexion.php";

class ModeloProductos{

	/*=============================================
	MOSTRAR PRODUCTOS
	=============================================*/

	static public function mdlMostrarProductos($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla INNER JOIN tbl_estado_producto ON tbl_producto.id_estado_producto = tbl_estado_producto.id_estado_producto INNER JOIN tbl_categoria ON tbl_producto.id_categoria = tbl_categoria.id_categoria WHERE $item = :$item ORDER BY id_producto DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_OBJ);

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla INNER JOIN tbl_estado_producto ON tbl_producto.id_estado_producto = tbl_estado_producto.id_estado_producto INNER JOIN tbl_categoria ON tbl_producto.id_categoria = tbl_categoria.id_categoria");


			$stmt -> execute();

			return $stmt -> fetchAll(PDO::FETCH_OBJ);

		}

		$stmt -> close();

		$stmt = null;

	}
	/*=============================================
	MOSTRAR PRODUCTOS ORDEN
	=============================================*/

	static public function mdlMostrarProductosOrden($tabla){

		

			$stmt = Conexion::conectar()->prepare("SELECT id_producto,nombre_producto FROM $tabla" );

			// $stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();


		$stmt -> close();

		$stmt = null;

	}

	static public function mdlIngresarProductos($tabla, $datos){
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre_producto, codigo_barra_producto, cantidad_producto, precio_compra_producto, precio_salida_producto, unidad_medicion_producto, presentacion_producto, imagen_producto, description_producto, id_categoria, id_estado_producto) VALUES (:nombre_producto, :codigo_barra_producto, :cantidad_producto, :precio_compra_producto, :precio_salida_producto, :unidad_medicion_producto, :presentacion_producto, :imagen_producto, :description_producto, :id_categoria, :id_estado_producto)");

		$stmt->bindParam(":nombre_producto",$datos["nombre_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":codigo_barra_producto",$datos["codigo_barra_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":cantidad_producto",$datos["cantidad_producto"], PDO::PARAM_INT);
		$stmt->bindParam(":precio_compra_producto",$datos["precio_compra_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":precio_salida_producto",$datos["precio_salida_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":unidad_medicion_producto",$datos["unidad_medicion_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":presentacion_producto",$datos["presentacion_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":imagen_producto",$datos["imagen_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":description_producto",$datos["description_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":id_categoria",$datos["id_categoria"], PDO::PARAM_INT);
		$stmt->bindParam(":id_estado_producto",$datos["id_estado_producto"], PDO::PARAM_INT);
		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		}

		$stmt->close();
		$stmt = null;

	}


	static public function modeloEditarProducto($tabla, $datos){
		$sql = "UPDATE $tabla SET nombre_producto=:nombre_producto,codigo_barra_producto= 
		:codigo_barra_producto, precio_compra_producto= 
		:precio_compra_producto, precio_salida_producto= :precio_salida_producto, unidad_medicion_producto= 
		:unidad_medicion_producto, presentacion_producto= :presentacion_producto,imagen_producto= 
		:imagen_producto,description_producto= :description_producto,id_categoria= :id_categoria, id_estado_producto= 
		:id_estado_producto WHERE id_producto = :id_producto";
		$stmt = Conexion::conectar()->prepare($sql);

		$stmt->bindParam(":id_producto",$datos["id_producto"], PDO::PARAM_INT);
		$stmt->bindParam(":nombre_producto",$datos["nombre_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":codigo_barra_producto",$datos["codigo_barra_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":precio_compra_producto",$datos["precio_compra_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":precio_salida_producto",$datos["precio_salida_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":unidad_medicion_producto",$datos["unidad_medicion_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":presentacion_producto",$datos["presentacion_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":imagen_producto",$datos["imagen_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":description_producto",$datos["description_producto"], PDO::PARAM_STR);
		$stmt->bindParam(":id_categoria",$datos["id_categoria"], PDO::PARAM_INT);
		$stmt->bindParam(":id_estado_producto",$datos["id_estado_producto"], PDO::PARAM_INT);
		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		}

		$stmt->close();
		$stmt = null;
	}

		static public function mdlEliminarProducto($tabla, $datos){
		$sql = "DELETE FROM $tabla WHERE id_producto = :id_producto";
		$stmt = Conexion::conectar()->prepare($sql);

		$stmt->bindParam(":id_producto",$datos, PDO::PARAM_INT);
		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		}

		$stmt->close();
		$stmt = null;
	}



	/*=============================================
	MOSTRAR PRODUCTOS PARA GUARDAR VENTA
	=============================================*/

	static public function mdlMostrarProductosV($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla INNER JOIN tbl_estado_producto ON tbl_producto.id_estado_producto = tbl_estado_producto.id_estado_producto INNER JOIN tbl_categoria ON tbl_producto.id_categoria = tbl_categoria.id_categoria WHERE $item = :$item ORDER BY id_producto DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla INNER JOIN tbl_estado_producto ON tbl_producto.id_estado_producto = tbl_estado_producto.id_estado_producto INNER JOIN tbl_categoria ON tbl_producto.id_categoria = tbl_categoria.id_categoria");


			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR STOCK PRODUCTOS PARA GUARDAR VENTA
	=============================================*/

	static public function mdlActualizarProductoV($tabla, $item1, $valor1, $valor){


			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE id_producto = :id");

			$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
			$stmt -> bindParam(":id", $valor, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR SUMA VENTAS
	=============================================*/	

	static public function mdlMostrarSumaVentas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT SUM(vendidos) as total FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;
	}


		/*=============================================
	MOSTRAR PRODUCTOS EN ORDEN PARA REPORTES
	=============================================*/

	static public function mdlMostrarProductosR($tabla, $item, $valor, $orden){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ORDER BY id_producto DESC");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY $orden DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;

	}
}
