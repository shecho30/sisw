<?php

require_once "conexion.php";

class ModeloPromocion
{

	/*=============================================
	MOSTRAR PROMOCION
	=============================================*/

    static public function mdlMostrarPromocion($tabla, $item, $valor)
    {

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}
		

		$stmt -> close();

		$stmt = null;

	}
	

	/*=============================================
	REGISTRO DE PROMOCION
	=============================================*/

	static public function mdlIngresarPromocion($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre_promocion, imagen_promocion, enlace_promocion, posicion_promocion) VALUES (:nombre_promocion, :imagen_promocion, :enlace_promocion, :posicion_promocion)");

		$stmt->bindParam(":nombre_promocion", $datos["nombre_promocion"], PDO::PARAM_STR);
		$stmt->bindParam(":imagen_promocion", $datos["imagen_promocion"], PDO::PARAM_STR);
		$stmt->bindParam(":enlace_promocion", $datos["enlace_promocion"], PDO::PARAM_STR);
		$stmt->bindParam(":posicion_promocion", $datos["posicion_promocion"], PDO::PARAM_INT);
		
		if($stmt->execute()){

			return "ok";	

		}else{

			return "error";
		
		}

		$stmt->close();
		
		$stmt = null;

	}

	/*=============================================
	BORRAR USUARIO
	=============================================*/

	static public function mdlBorrarPromocion($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_promocion = :id");

		$stmt -> bindParam(":id", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;


	}

	/*=============================================
	EDITAR PROMOCION
	=============================================*/

	static public function mdlEditarPromocion($tabla, $datos){
	
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre_promocion = :nombre_promocion,
																 imagen_promocion = :imagen_promocion,
																 enlace_promocion = :enlace_promocion, 
																 posicion_promocion = :posicion_promocion
																 WHERE id_promocion = :id_promocion");

		$stmt -> bindParam(":nombre_promocion", $datos["nombre_promocion"], PDO::PARAM_STR);
		$stmt -> bindParam(":imagen_promocion", $datos["imagen_promocion"], PDO::PARAM_STR);
		$stmt -> bindParam(":enlace_promocion", $datos["enlace_promocion"], PDO::PARAM_STR);
		$stmt -> bindParam(":posicion_promocion", $datos["posicion_promocion"], PDO::PARAM_INT);
		$stmt -> bindParam(":id_promocion", $datos["id_promocion"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}
}
    