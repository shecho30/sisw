<?php

require_once "conexion.php";

class ModeloOrden
{
    /*=============================================
	MOSTRAR ORDEN
	=============================================*/

	static public function mdlMostrarOrden($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}


		$stmt -> close();

		$stmt = null;

	}
	  /*=============================================
	MOSTRAR ORDEN
	=============================================*/

	/*=============================================
MOSTRAR ORDEN PARA PDF
=============================================*/

static public function mdlMostrarOrdenPdf($item,$tabla){

if ($tabla == "tbl_orden_compra") {

		if($item != null){


			$stmt = Conexion::conectar()->prepare("SELECT
																							oc.id_orden_compra,
																							oc.nombre_orden_compra,
																							oc.fecha_creacion_orden_compra,
																							oc.detalle_orden,
																							prov.documento_proveedor, prov.empresa_proveedor
																							FROM $tabla oc,
																							tbl_proveedor prov
																							WHERE oc.id_proveedor = prov.id_proveedor
																							AND oc.id_orden_compra = :idOrdenCompra;");

			$stmt -> bindParam(":idOrdenCompra",$item, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}

	else if ($tabla == "tbl_orden_producto"){
		if($item != null){


			$stmt = Conexion::conectar()->prepare("SELECT
																							op.precio_unidad,
																							op.cantidad_producto,
																							op.total_producto,
																							p.nombre_producto
																							FROM $tabla op, tbl_producto p
																							WHERE op.id_producto = p.id_producto
																							AND op.id_orden_compra = :idOrdenCompra;");

			$stmt -> bindParam(":idOrdenCompra",$item, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}
}
	/*=============================================
MOSTRAR ORDEN PARA PDF
=============================================*/

	static public function mdlMostrarOrdenProducto($tabla, $item, $valor){

		if($item != null){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

		}


		$stmt -> close();

		$stmt = null;

	}
  /*=============================================
	INSERTAR ORDEN
	=============================================*/

	static public function mdlIngresarOrden($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombre_orden_compra,fecha_creacion_orden_compra,id_proveedor,detalle_orden)
												     VALUES(:nombre_orden_compra,SYSDATE(),:id_proveedor,:detalle_orden)");

		$stmt->bindParam(":nombre_orden_compra", $datos["nueva_orden"], PDO::PARAM_STR);
		$stmt->bindParam(":id_proveedor",   $datos["nuevo_proveedor"], PDO::PARAM_INT);
		$stmt->bindParam(":detalle_orden",   $datos["nuevo_detalle"], PDO::PARAM_STR);

		print_r($stmt);
		if($stmt->execute())
		{
			return "ok";
		}
		else
		{
			return "error";
		}

		$stmt->close();

		$stmt = null;

	}

	/*=============================================
	INSERTAR ORDEN
	=============================================*/

	static public function mdlIngresarOrdenProducto($tabla, $datos)
	{
		// print_r($datos);
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_producto,id_orden_compra,precio_unidad,cantidad_producto,total_producto)
												  															VALUES(:id_producto, :id_orden_compra, :precio_unidad, :cantidad_producto, :total_producto)");



		$stmt->bindParam(":id_producto", 		$datos["insert_id_producto"], PDO::PARAM_INT);
		$stmt->bindParam(":id_orden_compra",    $datos["insert_id_orden"], PDO::PARAM_INT);
		$stmt->bindParam(":precio_unidad",      $datos["nuevo_precio"], PDO::PARAM_INT);
		$stmt->bindParam(":cantidad_producto",  $datos["nuevo_cantidad"], PDO::PARAM_INT);
		$stmt->bindParam(":total_producto",     $datos["total_final"], PDO::PARAM_INT);

		print_r($stmt);
		// print_r($datos);
		if($stmt->execute())
		{
			return "ok";
		}
		else
		{
			return "error";
		}

		$stmt->close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR ORDEN DETALLADA
	=============================================*/

	static public function mdlMostrarOrdenDetallada($valor){


			$stmt = Conexion::conectar()->prepare("SELECT
													op.id_orden_producto as id,
													oc.id_orden_compra as id_orden,
													oc.nombre_orden_compra as nombre_orden,
													prov.id_proveedor as id_proveedor,
													prov.nombre_repre_proveedor as nombre_proveedor,
													prod.id_producto as id_producto,
													prod.nombre_producto as nombre_producto,
													op.precio_unidad as precio,
													op.cantidad_producto as cantidad,
													op.total_producto as total
												  FROM tbl_orden_producto AS op
													INNER JOIN tbl_orden_compra AS oc
													ON oc.id_orden_compra = op.id_orden_compra
													INNER JOIN tbl_producto as prod
													ON prod.id_producto = op.id_producto
													INNER JOIN tbl_proveedor as prov
													ON prov.id_proveedor = oc.id_proveedor
													WHERE oc.id_orden_compra = :id_orden");

			$stmt->bindParam(":id_orden", $valor, PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll();


		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	Eliminar Orden Compra
	=============================================*/

	static public function mdlBorrarOrdenCompra($valor)
	{

		$stmt = Conexion::conectar()->prepare("DELETE op, oc FROM tbl_orden_producto AS op INNER JOIN tbl_orden_compra AS oc
												WHERE op.id_orden_compra = oc.id_orden_compra AND op.id_orden_compra = :id_orden_compra");

		$stmt -> bindParam(":id_orden_compra", $valor, PDO::PARAM_INT);

		$stmt->execute();

		if($stmt->rowCount() > 0)
		{
			return "ok";
		}
		else
		{
			$stmt = null;
			$stmt = Conexion::conectar()->prepare("DELETE FROM tbl_orden_compra WHERE id_orden_compra = :id_orden_compra");
			$stmt -> bindParam(":id_orden_compra", $valor, PDO::PARAM_INT);
			if($stmt -> execute()){
				return "ok";
			}else{

				return "error";
			}

		}

		$stmt -> close();

		$stmt = null;


	}

	/*=============================================
	Eliminar Orden Producto
	=============================================*/

	static public function mdlBorrarOrdenProducto($tabla,$valor)
	{

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_orden_producto = :id_orden_producto");

		$stmt -> bindParam(":id_orden_producto", $valor, PDO::PARAM_INT);

		$stmt->execute();

		if($stmt->rowCount() > 0)
		{
			return "ok";
		}
		else
		{
			return "error";

		}

		$stmt -> close();

		$stmt = null;


	}

		/*=============================================
	EDITAR ORDEN
	=============================================*/

	static public function mdlEditarOrdenCompra($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre_orden_compra = :nombre_orden_compra,
																 id_proveedor = :id_proveedor,
																 detalle_orden = :detalle_orden
																 WHERE id_orden_compra = :id_orden_compra");

		$stmt -> bindParam(":nombre_orden_compra", $datos["nombre_orden"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_proveedor", $datos["id_proveedor"], PDO::PARAM_INT);
		$stmt -> bindParam(":detalle_orden", $datos["detalle_orden"], PDO::PARAM_STR);
		$stmt -> bindParam(":id_orden_compra", $datos["id_orden"], PDO::PARAM_INT);


		//print_r($stmt);
		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";
		}

		$stmt -> close();

		$stmt = null;

	}

		/*=============================================
	EDITAR ORDEN PRODUCTO
	=============================================*/

	static public function mdlEditarOrdenProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET precio_unidad = :precio_unidad,
																 cantidad_producto = :cantidad_producto,
																 total_producto = :total_producto
																 WHERE id_orden_producto = :id_orden_producto");

		$stmt -> bindParam(":precio_unidad", 	 $datos["_nuevo_precio"], PDO::PARAM_INT);
		$stmt -> bindParam(":cantidad_producto", $datos["_nuevo_cantidad"], PDO::PARAM_INT);
		$stmt -> bindParam(":total_producto", 	 $datos["_nuevo_total"], PDO::PARAM_INT);
		$stmt -> bindParam(":id_orden_producto", $datos["_id"], PDO::PARAM_INT);


		//print_r($stmt);
		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";
		}

		$stmt -> close();

		$stmt = null;

	}



}
