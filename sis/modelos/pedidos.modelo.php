<?php 
require_once "conexion.php";

/**
 * 
 */
class ModeloPedidos
{
	
	public function mdlMostrarPedidos($tabla,$item,$valor){
		if ($item!= null) {
			$stmt = Conexion::conectar()->prepare("SELECT t1.id_pedido, t1.numero_pedido, t1.id_cliente, t1.id_producto, t1.fecha_creacion_pedido, t1.cantidad_pedido, t2.nombre_repre_cliente, t3.nombre_producto FROM $tabla t1 INNER JOIN tbl_cliente t2 ON t1.id_cliente = t2.id_cliente INNER JOIN tbl_producto t3 ON t1.id_producto = t3.id_producto WHERE $item = $item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			
			$stmt -> execute();

			$result = $stmt -> fetch(PDO::FETCH_OBJ);
			return $result;
		}else {
			$stmt = Conexion::conectar()->prepare("SELECT t1.id_pedido, t1.numero_pedido, t1.id_cliente, t1.id_producto, t1.fecha_creacion_pedido, t1.cantidad_pedido, t2.nombre_repre_cliente, t3.nombre_producto FROM $tabla t1 INNER JOIN tbl_cliente t2 ON t1.id_cliente = t2.id_cliente INNER JOIN tbl_producto t3 ON t1.id_producto = t3.id_producto");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			
			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}

	public function mdlMostrarClientes($tabla, $item, $valor){
		if ($item!= null) {
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = $item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			
			$stmt -> execute();

			$result = $stmt -> fetch(PDO::FETCH_OBJ);
			return $result;
		}else {
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			
			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}
	public function mdlMostrarProductos($tabla, $item, $valor){
		if ($item!= null) {
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = $item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			
			$stmt -> execute();

			$result = $stmt -> fetch(PDO::FETCH_OBJ);
			return $result;
		}else {
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			
			$stmt -> execute();

			return $stmt -> fetchAll();

		}

		$stmt -> close();

		$stmt = null;
	}

	public function mdlAgregarPedidos($tabla,$datos){
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (numero_pedido,id_cliente,id_producto,fecha_creacion_pedido,cantidad_pedido) VALUES (:numero_pedido,:id_cliente,:id_producto,:fecha_creacion_pedido,:cantidad_pedido)");

		$stmt->bindParam(":numero_pedido",         $datos["numero_pedido"],            PDO::PARAM_STR);
		$stmt->bindParam(":id_cliente",            $datos["id_cliente"],               PDO::PARAM_STR);
		$stmt->bindParam(":id_producto",           $datos["id_producto"],              PDO::PARAM_STR);
		$stmt->bindParam(":fecha_creacion_pedido", $datos["fecha_creacion_pedido"],    PDO::PARAM_STR);
		$stmt->bindParam(":cantidad_pedido",       $datos["cantidad_pedido"],          PDO::PARAM_STR);

		var_dump($datos);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;
	}
	public function mdlEditarPedidos($tabla,$datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET numero_pedido = :numero_pedido, id_cliente = :id_cliente, id_producto =:id_producto, cantidad_pedido =:cantidad_pedido WHERE id_pedido = :id_pedido");

		$stmt->bindParam(":id_pedido",              $datos["id_pedido"],       PDO::PARAM_INT); 
		$stmt->bindParam(":numero_pedido" ,         $datos["numero_pedido"],   PDO::PARAM_INT);
		$stmt->bindParam(":id_cliente" ,            $datos["id_cliente"],      PDO::PARAM_INT);
		$stmt->bindParam(":id_producto" ,           $datos["id_producto"],     PDO::PARAM_INT);
		$stmt->bindParam(":cantidad_pedido" ,       $datos["cantidad_pedido"], PDO::PARAM_INT);


		//echo json_encode($datos);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	static public function mdlEliminarPedido($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_pedido = :id_pedido");

		$stmt -> bindParam(":id_pedido", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;
	}
	
}
 ?>