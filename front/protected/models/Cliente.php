<?php

/**
 * This is the model class for table "tbl_cliente".
 *
 * The followings are the available columns in table 'tbl_cliente':
 * @property integer $id_cliente
 * @property string $document_cliente
 * @property integer $id_tipo_documento
 * @property string $empresa_cliente
 * @property string $nombre_repre_cliente
 * @property string $apellido_repre_cliente
 * @property string $telefono_cliente
 * @property string $direccion_cliente
 * @property string $correo_cliente
 * @property string $contrasena_cliente
 * @property integer $id_estado_usuario
 * @property string $fecha_creacion_cliente
 * @property integer $id_ciudad
 *
 * The followings are the available model relations:
 * @property TblCiudad $idCiudad
 * @property TblTipoDocumento $idTipoDocumento
 * @property TblFactura[] $tblFacturas
 * @property TblPedido[] $tblPedidos
 */
class Cliente extends CActiveRecord {

    public $repetir_contrasena;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TblCliente the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_cliente';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('document_cliente', 'required', 'message' => 'Digite numero de documento'),
            array('document_cliente', 'numerical', 'integerOnly' => true, 'message' => 'La identificaciín debe ser numérica'),
            array('id_tipo_documento', 'required', 'message' => 'Seleccione tipo de documento', 'on' => 'registro'),
            array('nombre_repre_cliente', 'required', 'message' => 'El nombre es requerido', 'on' => 'registro'),
            array('correo_cliente', 'unique', 'message' => 'El correo electronico ya se encuentra registrado', 'on' => 'registro'),
            array('apellido_repre_cliente, telefono_cliente, direccion_cliente, correo_cliente, contrasena_cliente, id_ciudad', 'required', 'on' => 'registro'),
//			array('nombre_repre_cliente, apellido_repre_cliente, telefono_cliente, direccion_cliente, correo_cliente, contrasena_cliente, id_estado_usuario, fecha_creacion_cliente, id_ciudad', 'required'),
//                      array('id_tipo_documento, id_estado_usuario, id_ciudad', 'numerical', 'integerOnly'=>true),
            array('document_cliente, empresa_cliente, nombre_repre_cliente, apellido_repre_cliente, telefono_cliente, direccion_cliente, correo_cliente', 'length', 'max' => 50, 'on' => 'registro'),
            array('contrasena_cliente', 'length', 'min' => 4, 'max' => 60),
            array('repetir_contrasena', 'length', 'max' => 60),
            array('repetir_contrasena', 'compare', 'compareAttribute' => 'contrasena_cliente', 'message' => "Las claves no coinciden.", "on" => "registro"),
            array('repetir_contrasena', 'compare', 'compareAttribute' => 'contrasena_cliente', 'message' => "Las claves no coinciden.", "on" => "pass"),
            array('correo_cliente', 'required', 'message' => 'Digite el correo registrado', 'on' => 'recordar'),
            array('correo_cliente', 'email', 'message' => 'No es un correo valido', 'on' => 'recordar'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id_cliente, document_cliente, id_tipo_documento, empresa_cliente, nombre_repre_cliente, apellido_repre_cliente, telefono_cliente, direccion_cliente, correo_cliente, contrasena_cliente, id_estado_usuario, fecha_creacion_cliente, id_ciudad', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idCiudad' => array(self::BELONGS_TO, 'Ciudad', 'id_ciudad'),
            'idTipoDocumento' => array(self::BELONGS_TO, 'TipoDocumento', 'id_tipo_documento'),
            'tblFacturas' => array(self::HAS_MANY, 'Factura', 'id_cliente'),
            'tblPedidos' => array(self::HAS_MANY, 'Pedido', 'id_cliente'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id_cliente' => 'Id Cliente',
            'document_cliente' => 'Documento',
            'id_tipo_documento' => 'Tipo de documento',
            'empresa_cliente' => 'Empresa Cliente',
            'nombre_repre_cliente' => 'Nombre',
            'apellido_repre_cliente' => 'Apellido',
            'telefono_cliente' => 'Telefono',
            'direccion_cliente' => 'Direccion ',
            'correo_cliente' => 'Email',
            'contrasena_cliente' => 'Contrasena',
            'repetir_contrasena' => 'Repetir contrasena',
            'id_estado_usuario' => 'Id Estado Usuario',
            'fecha_creacion_cliente' => 'Fecha Creacion Cliente',
            'id_ciudad' => 'Ciudad',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_cliente', $this->id_cliente);
        $criteria->compare('document_cliente', $this->document_cliente, true);
        $criteria->compare('id_tipo_documento', $this->id_tipo_documento);
        $criteria->compare('empresa_cliente', $this->empresa_cliente, true);
        $criteria->compare('nombre_repre_cliente', $this->nombre_repre_cliente, true);
        $criteria->compare('apellido_repre_cliente', $this->apellido_repre_cliente, true);
        $criteria->compare('telefono_cliente', $this->telefono_cliente, true);
        $criteria->compare('direccion_cliente', $this->direccion_cliente, true);
        $criteria->compare('correo_cliente', $this->correo_cliente, true);
        $criteria->compare('contrasena_cliente', $this->contrasena_cliente, true);
        $criteria->compare('id_estado_usuario', $this->id_estado_usuario);
        $criteria->compare('fecha_creacion_cliente', $this->fecha_creacion_cliente, true);
        $criteria->compare('id_ciudad', $this->id_ciudad);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
