<?php

/**
 * This is the model class for table "tbl_categoria".
 *
 * The followings are the available columns in table 'tbl_categoria':
 * @property integer $id_categoria
 * @property string $nombre_categoria
 * @property string $fecha_creacion_categoria
 * @property string $imagen_categoria
 * @property string $descripcion_categoria
 *
 * The followings are the available model relations:
 * @property TblProducto[] $tblProductos
 */
class Categoria extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Categoria the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_categoria';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre_categoria, fecha_creacion_categoria, descripcion_categoria', 'required'),
			array('nombre_categoria', 'length', 'max'=>50),
			array('imagen_categoria', 'length', 'max'=>255),
			array('descripcion_categoria', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_categoria, nombre_categoria, fecha_creacion_categoria, imagen_categoria, descripcion_categoria', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblProductos' => array(self::HAS_MANY, 'TblProducto', 'id_categoria'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_categoria' => 'Id Categoria',
			'nombre_categoria' => 'Nombre Categoria',
			'fecha_creacion_categoria' => 'Fecha Creacion Categoria',
			'imagen_categoria' => 'Imagen Categoria',
			'descripcion_categoria' => 'Descripcion Categoria',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_categoria',$this->id_categoria);
		$criteria->compare('nombre_categoria',$this->nombre_categoria,true);
		$criteria->compare('fecha_creacion_categoria',$this->fecha_creacion_categoria,true);
		$criteria->compare('imagen_categoria',$this->imagen_categoria,true);
		$criteria->compare('descripcion_categoria',$this->descripcion_categoria,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}