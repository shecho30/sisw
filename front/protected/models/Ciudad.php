<?php

/**
 * This is the model class for table "tbl_ciudad".
 *
 * The followings are the available columns in table 'tbl_ciudad':
 * @property integer $id_ciudad
 * @property string $nombre_ciudad
 * @property integer $id_departamento
 *
 * The followings are the available model relations:
 * @property TblDepartamento $idDepartamento
 * @property TblCliente[] $tblClientes
 */
class Ciudad extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ciudad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_ciudad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre_ciudad, id_departamento', 'required'),
			array('id_departamento', 'numerical', 'integerOnly'=>true),
			array('nombre_ciudad', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_ciudad, nombre_ciudad, id_departamento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDepartamento' => array(self::BELONGS_TO, 'TblDepartamento', 'id_departamento'),
			'tblClientes' => array(self::HAS_MANY, 'TblCliente', 'id_ciudad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_ciudad' => 'Id Ciudad',
			'nombre_ciudad' => 'Nombre Ciudad',
			'id_departamento' => 'Id Departamento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_ciudad',$this->id_ciudad);
		$criteria->compare('nombre_ciudad',$this->nombre_ciudad,true);
		$criteria->compare('id_departamento',$this->id_departamento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}