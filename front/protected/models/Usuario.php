<?php

/**
 * This is the model class for table "tbl_usuario".
 *
 * The followings are the available columns in table 'tbl_usuario':
 * @property integer $id_usuario
 * @property string $nombre_usuario
 * @property string $apellido_usuario
 * @property string $nick_user
 * @property string $correo_usuario
 * @property string $contrasena_usuario
 * @property string $fecha_creacion_usuario
 * @property string $imagen_usuario
 * @property integer $id_estado_usuario
 * @property integer $id_tipo_usuario
 *
 * The followings are the available model relations:
 * @property TblFactura[] $tblFacturas
 * @property TblOrdenCompra[] $tblOrdenCompras
 * @property TblEstadoUsuario $idEstadoUsuario
 * @property TblTipoUsuario $idTipoUsuario
 */
class Usuario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre_usuario, apellido_usuario, nick_user, correo_usuario, contrasena_usuario, fecha_creacion_usuario, id_estado_usuario, id_tipo_usuario', 'required'),
			array('id_estado_usuario, id_tipo_usuario', 'numerical', 'integerOnly'=>true),
			array('nombre_usuario, apellido_usuario, nick_user', 'length', 'max'=>50),
			array('correo_usuario', 'length', 'max'=>60),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			
			array('id_usuario, nombre_usuario, apellido_usuario, nick_user, correo_usuario, contrasena_usuario, fecha_creacion_usuario, imagen_usuario, id_estado_usuario, id_tipo_usuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblFacturas' => array(self::HAS_MANY, 'TblFactura', 'id_usuario'),
			'tblOrdenCompras' => array(self::HAS_MANY, 'TblOrdenCompra', 'id_usuario'),
			'idEstadoUsuario' => array(self::BELONGS_TO, 'TblEstadoUsuario', 'id_estado_usuario'),
			'idTipoUsuario' => array(self::BELONGS_TO, 'TblTipoUsuario', 'id_tipo_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_usuario' => 'Id Usuario',
			'nombre_usuario' => 'Nombre Usuario',
			'apellido_usuario' => 'Apellido Usuario',
			'nick_user' => 'Nick User',
			'correo_usuario' => 'Correo Usuario',
			'contrasena_usuario' => 'Contrasena Usuario',
			'fecha_creacion_usuario' => 'Fecha Creacion Usuario',
			'imagen_usuario' => 'Imagen Usuario',
			'id_estado_usuario' => 'Id Estado Usuario',
			'id_tipo_usuario' => 'Id Tipo Usuario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_usuario',$this->id_usuario);
		$criteria->compare('nombre_usuario',$this->nombre_usuario,true);
		$criteria->compare('apellido_usuario',$this->apellido_usuario,true);
		$criteria->compare('nick_user',$this->nick_user,true);
		$criteria->compare('correo_usuario',$this->correo_usuario,true);
		$criteria->compare('contrasena_usuario',$this->contrasena_usuario,true);
		$criteria->compare('fecha_creacion_usuario',$this->fecha_creacion_usuario,true);
		$criteria->compare('imagen_usuario',$this->imagen_usuario,true);
		$criteria->compare('id_estado_usuario',$this->id_estado_usuario);
		$criteria->compare('id_tipo_usuario',$this->id_tipo_usuario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}