<?php

/**
 * This is the model class for table "tbl_producto".
 *
 * The followings are the available columns in table 'tbl_producto':
 * @property integer $id_producto
 * @property string $nombre_producto
 * @property string $codigo_barra_producto
 * @property integer $cantidad_producto
 * @property double $precio_compra_producto
 * @property double $precio_salida_producto
 * @property string $unidad_medicion_producto
 * @property string $presentacion_producto
 * @property string $fecha_creacion_producto
 * @property string $imagen_producto
 * @property string $description_producto
 * @property integer $id_categoria
 * @property integer $id_estado_producto
 *
 * The followings are the available model relations:
 * @property TblDetalleFactura[] $tblDetalleFacturas
 * @property TblDetalleOc[] $tblDetalleOcs
 * @property TblPedido[] $tblPedidos
 * @property TblPedidoProveedor[] $tblPedidoProveedors
 * @property TblCategoria $idCategoria
 * @property TblEstadoProducto $idEstadoProducto
 * @property TblProveedor[] $tblProveedors
 */
class Producto extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Producto the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_producto';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre_producto, codigo_barra_producto, cantidad_producto, precio_compra_producto, precio_salida_producto, unidad_medicion_producto, presentacion_producto, fecha_creacion_producto, description_producto, id_categoria, id_estado_producto', 'required'),
            array('cantidad_producto, id_categoria, id_estado_producto', 'numerical', 'integerOnly' => true),
            array('precio_compra_producto, precio_salida_producto', 'numerical'),
            array('nombre_producto, codigo_barra_producto, presentacion_producto', 'length', 'max' => 50),
            array('unidad_medicion_producto, imagen_producto, description_producto', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id_producto, nombre_producto, codigo_barra_producto, cantidad_producto, precio_compra_producto, precio_salida_producto, unidad_medicion_producto, presentacion_producto, fecha_creacion_producto, imagen_producto, description_producto, id_categoria, id_estado_producto', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tblDetalleFacturas' => array(self::HAS_MANY, 'DetalleFactura', 'id_producto'),
            'tblDetalleOcs' => array(self::HAS_MANY, 'DetalleOc', 'id_producto'),
            'tblPedidos' => array(self::HAS_MANY, 'Pedido', 'id_producto'),
            'tblPedidoProveedors' => array(self::HAS_MANY, 'PedidoProveedor', 'id_producto'),
            'idCategoria' => array(self::BELONGS_TO, 'Categoria', 'id_categoria'),
            'idEstadoProducto' => array(self::BELONGS_TO, 'EstadoProducto', 'id_estado_producto'),
            'tblProveedors' => array(self::HAS_MANY, 'Proveedor', 'id_producto'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id_producto' => 'Id Producto',
            'nombre_producto' => 'Nombre Producto',
            'codigo_barra_producto' => 'Codigo Barra Producto',
            'cantidad_producto' => 'Cantidad Producto',
            'precio_compra_producto' => 'Precio Compra Producto',
            'precio_salida_producto' => 'Precio venta',
            'unidad_medicion_producto' => 'Unidad Medicion Producto',
            'presentacion_producto' => 'Presentacion Producto',
            'fecha_creacion_producto' => 'Fecha Creacion Producto',
            'imagen_producto' => 'Imagen Producto',
            'description_producto' => 'Description Producto',
            'id_categoria' => 'Id Categoria',
            'id_estado_producto' => 'Id Estado Producto',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_producto', $this->id_producto);
        $criteria->compare('nombre_producto', $this->nombre_producto, true);
        $criteria->compare('codigo_barra_producto', $this->codigo_barra_producto, true);
        $criteria->compare('cantidad_producto', $this->cantidad_producto);
        $criteria->compare('precio_compra_producto', $this->precio_compra_producto);
        $criteria->compare('precio_salida_producto', $this->precio_salida_producto);
        $criteria->compare('unidad_medicion_producto', $this->unidad_medicion_producto, true);
        $criteria->compare('presentacion_producto', $this->presentacion_producto, true);
        $criteria->compare('fecha_creacion_producto', $this->fecha_creacion_producto, true);
        $criteria->compare('imagen_producto', $this->imagen_producto, true);
        $criteria->compare('description_producto', $this->description_producto, true);
        $criteria->compare('id_categoria', $this->id_categoria);
        $criteria->compare('id_estado_producto', $this->id_estado_producto);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function mbuscador() {
        $criteria = new CDbCriteria ();
        
        $criteria->compare('nombre_producto', $this->nombre_producto, true);
        $criteria->compare('description_producto', $this->nombre_producto, true,'OR');
//        $criteria->compare('idZona', $this->idZona);
//        $criteria->compare('etiqueta', $this->etiqueta);
//        $criteria->compare('id_estado_producto', "1");

        $criteria->addCondition("id_estado_producto='1'");
//        dump($criteria);
        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria));
    }

}
