<?php

/**
 * This is the model class for table "tbl_factura".
 *
 * The followings are the available columns in table 'tbl_factura':
 * @property integer $id_factura
 * @property integer $id_dian_factura
 * @property integer $id_cliente
 * @property integer $id_usuario
 * @property string $detalle_factura
 * @property integer $id_forma_pago
 * @property double $iva_factura
 * @property double $neto_factura
 * @property double $total_factura
 * @property string $fecha_creacion_factura
 *
 * The followings are the available model relations:
 * @property DetalleFactura[] $detalleFacturas
 * @property FormaPago $idFormaPago
 * @property Usuario $idUsuario
 * @property Cliente $idCliente
 */
class Factura extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Factura the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tbl_factura';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_dian_factura, id_cliente, detalle_factura, id_forma_pago, iva_factura, neto_factura, total_factura', 'required'),
            array('id_dian_factura, id_cliente, id_usuario, id_forma_pago', 'numerical', 'integerOnly' => true),
            array('iva_factura, neto_factura, total_factura', 'numerical'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id_factura, id_dian_factura, id_cliente, id_usuario, detalle_factura, id_forma_pago, iva_factura, neto_factura, total_factura, fecha_creacion_factura', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'detalleFacturas' => array(self::HAS_MANY, 'DetalleFactura', 'id_factura'),
            'idFormaPago' => array(self::BELONGS_TO, 'FormaPago', 'id_forma_pago'),
            'idUsuario' => array(self::BELONGS_TO, 'Usuario', 'id_usuario'),
            'idCliente' => array(self::BELONGS_TO, 'Cliente', 'id_cliente'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id_factura' => 'Id Factura',
            'id_dian_factura' => 'Id Dian Factura',
            'id_cliente' => 'Id Cliente',
            'id_usuario' => 'Id Usuario',
            'detalle_factura' => 'Detalle Factura',
            'id_forma_pago' => 'Forma de Pago',
            'iva_factura' => 'IVA',
            'neto_factura' => 'Sub total',
            'total_factura' => 'Total',
            'fecha_creacion_factura' => 'Fecha Creacion Factura',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id_factura', $this->id_factura);
        $criteria->compare('id_dian_factura', $this->id_dian_factura);
        $criteria->compare('id_cliente', $this->id_cliente);
        $criteria->compare('id_usuario', $this->id_usuario);
        $criteria->compare('detalle_factura', $this->detalle_factura, true);
        $criteria->compare('id_forma_pago', $this->id_forma_pago);
        $criteria->compare('iva_factura', $this->iva_factura);
        $criteria->compare('neto_factura', $this->neto_factura);
        $criteria->compare('total_factura', $this->total_factura);
        $criteria->compare('fecha_creacion_factura', $this->fecha_creacion_factura, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
