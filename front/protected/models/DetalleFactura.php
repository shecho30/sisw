<?php

/**
 * This is the model class for table "tbl_detalle_factura".
 *
 * The followings are the available columns in table 'tbl_detalle_factura':
 * @property integer $id_detalle_factura
 * @property integer $id_producto
 * @property integer $id_factura
 * @property double $cantidad_detalle_factura
 * @property string $total_detalle_factura
 * @property string $fecha_creacion_detalle_factura
 *
 * The followings are the available model relations:
 * @property Factura $idFactura
 * @property Producto $idProducto
 * @property Devolucion[] $devolucions
 */
class DetalleFactura extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetalleFactura the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_detalle_factura';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_producto, id_factura, cantidad_detalle_factura, total_detalle_factura, fecha_creacion_detalle_factura', 'required'),
			array('id_producto, id_factura', 'numerical', 'integerOnly'=>true),
			array('cantidad_detalle_factura', 'numerical'),
			array('total_detalle_factura', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_detalle_factura, id_producto, id_factura, cantidad_detalle_factura, total_detalle_factura, fecha_creacion_detalle_factura', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idFactura' => array(self::BELONGS_TO, 'Factura', 'id_factura'),
			'idProducto' => array(self::BELONGS_TO, 'Producto', 'id_producto'),
			'devolucions' => array(self::HAS_MANY, 'Devolucion', 'id_detalle_factura'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_detalle_factura' => 'Id Detalle Factura',
			'id_producto' => 'Id Producto',
			'id_factura' => 'Id Factura',
			'cantidad_detalle_factura' => 'Cantidad Detalle Factura',
			'total_detalle_factura' => 'Total Detalle Factura',
			'fecha_creacion_detalle_factura' => 'Fecha Creacion Detalle Factura',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_detalle_factura',$this->id_detalle_factura);
		$criteria->compare('id_producto',$this->id_producto);
		$criteria->compare('id_factura',$this->id_factura);
		$criteria->compare('cantidad_detalle_factura',$this->cantidad_detalle_factura);
		$criteria->compare('total_detalle_factura',$this->total_detalle_factura,true);
		$criteria->compare('fecha_creacion_detalle_factura',$this->fecha_creacion_detalle_factura,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}