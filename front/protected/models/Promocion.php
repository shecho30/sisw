<?php

/**
 * This is the model class for table "tbl_promocion".
 *
 * The followings are the available columns in table 'tbl_promocion':
 * @property integer $id_promocion
 * @property string $nombre_promocion
 * @property string $imagen_promocion
 * @property string $enlace_promocion
 * @property integer $posicion_promocion
 */
class Promocion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Promocion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_promocion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre_promocion, posicion_promocion', 'required'),
			array('posicion_promocion', 'numerical', 'integerOnly'=>true),
			array('nombre_promocion', 'length', 'max'=>50),
			array('imagen_promocion, enlace_promocion', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_promocion, nombre_promocion, imagen_promocion, enlace_promocion, posicion_promocion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_promocion' => 'Id Promocion',
			'nombre_promocion' => 'Nombre Promocion',
			'imagen_promocion' => 'Imagen Promocion',
			'enlace_promocion' => 'Enlace Promocion',
			'posicion_promocion' => 'Posicion Promocion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_promocion',$this->id_promocion);
		$criteria->compare('nombre_promocion',$this->nombre_promocion,true);
		$criteria->compare('imagen_promocion',$this->imagen_promocion,true);
		$criteria->compare('enlace_promocion',$this->enlace_promocion,true);
		$criteria->compare('posicion_promocion',$this->posicion_promocion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}