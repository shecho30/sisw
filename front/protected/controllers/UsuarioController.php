<?php

class UsuarioController extends Controller {

    public $rutaImagen;



    public function accessRules() {
        return array(array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array(''),
                'users' => array('*')),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'pass', 'compras'),
                'users' => array('@')),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'pass', 'compras'),
                'users' => array('admin')),
            array('deny', // deny all users
                'users' => array('*')));
    }

    public function init() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(array('/site/index'));
        }

        $scriptDir = "";
        $this->rutaImagen = $scriptDir . Yii::app()->params['imagePath']. "/";
        parent::init();
    }

    public function actionIndex() {
        $this->pageTitle = "Mi cuenta - ".YII::app()->name;
        $model = Cliente::model()->findByPk(Yii::app()->user->id);
        if (isset($_POST['Cliente'])) {
            $model->attributes = $_POST['Cliente'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', "Se actualizaron los datos correctamente");
//                $this->redirect(array('view', 'id' => $model->id_cliente));
            }
        }
        $this->render('index', array("model" => $model));
    }

    public function actionPass() {
        $this->pageTitle = "Cambiar contraseña - ".YII::app()->name;
        $model = Cliente::model()->findByPk(Yii::app()->user->id);
        $model->contrasena_cliente = "";
        if (isset($_POST['Cliente'])) {
            $model->attributes = $_POST['Cliente'];
            if ($model->contrasena_cliente === $model->repetir_contrasena) {
                $model->contrasena_cliente= md5($model->contrasena_cliente);
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Se actualizaron los datos correctamente");
//                $this->redirect(array('view', 'id' => $model->id_cliente));
                }
            }else{
                Yii::app()->user->setFlash('error', "Las contraseñas no coinciden");
            }
        }

        $this->render('password', array("model" => $model));
    }
    
    public function actionCompras() {
        $this->pageTitle = "Mis compras - ".YII::app()->name;
//        $model = Factura::model()->findByPk(Yii::app()->user->id);
        
        $dataProvider = new CActiveDataProvider('Factura', array(
            'criteria' => array(
                'condition' => 'id_cliente=' . Yii::app()->user->id,
                'order' => 'id_factura ASC',
            ),
            'pagination' => array(
                'pageSize' => 12,
            ),
        ));
        
        $this->render('compras', array("dataProvider" => $dataProvider));
    }
    
    public function actionDetalleFactura($id) {
        $this->pageTitle = "Detalle de compra #".$id." - ".YII::app()->name;
        $model = Factura::model()->findByPk($id);
        $detalle = DetalleFactura::model()->findAll("id_factura=".$id);
        $this->render('detallefactura', array("model" => $model, "detalle"=>$detalle));
    }


}
