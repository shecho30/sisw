<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public $rutaImagen;
    public $rutaSlide;

    /*
     * Construccion ruta de bodega de imagenes
     */

    public function init() {
//        if (Yii::app()->user->isguest) {
//            $this->redirect(array('/site/login'));
//        }
        $scriptDir = "";
        $this->rutaImagen = $scriptDir . Yii::app()->params['imagePath'] . "/";
        $this->rutaSlide = "mi-ruta/";
        parent::init();
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $promo = Promocion::model()->findAll();
        $this->pageTitle = YII::app()->name;
        $criteria = new CDbCriteria;
        $criteria->addCondition('id_estado_producto = 1');
        $criteria->addCondition('cantidad_producto > 0');
        $criteria->order = 'RAND()';
        $criteria->limit = 9;

        $productos = Producto::model()->findAll($criteria);
        $this->render('index', array("promo" => $promo, "productos" => $productos));
    }

    public function actionTerminos() {
        $this->pageTitle = "Terminos y condiciones" . " - " . YII::app()->name;
        $this->render('terminos');
    }

    public function actionPrivacidad() {
        $this->pageTitle = "Privacidad" . " - " . YII::app()->name;
        $this->render('privacidad');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /*
     * Accion para envio de correo y recordar contraseña
     */

    public function actionRecordarpass() {
        $this->pageTitle = "Recordar contraseña" . " - " . YII::app()->name;

        $model = new Cliente('recordar');

        if (isset($_POST['Cliente']['correo_cliente'])) {
            $correo = $_POST['Cliente']['correo_cliente'];
            $model->attributes = $_POST['Cliente'];

            $model->validate("correo_cliente");

            $criteria = new CDbCriteria;
            $criteria->addCondition("correo_cliente = '" . $model->correo_cliente . "'");
            $criteria->addCondition('id_estado_usuario = 1');
            $envio = Cliente::model()->find($criteria);

            if ($envio) {
                $link = Yii::app()->createAbsoluteUrl("site/reset", array("m" => md5($envio->correo_cliente)));

                $message = '<p>Para restablecer su contraseña <a href="' . $link . '">Clic Aquí</a></p>';

                if ($_SERVER["SERVER_NAME"] == "localhost") {
                    dump($message);
                    Yii::app()->user->setFlash('warning', '<p>El sistema esta instalado en una maquina local y no se envia correo. </p>' . $message);
                } else {
                    $headers = "From: {" . Yii::app()->params['adminEmail'] . "}\r\nReply-To: {" . Yii::app()->params['adminEmail'] . "}";
                    mail($envio->correo_cliente, "Recuperar contraseña", $message, $headers);
                }
            } else {
                Yii::app()->user->setFlash('error', 'El correo no se encuentra registrado');
            }
        }
        $this->render('recordar', array("model" => $model));
    }

    public function actionReset() {
        $this->pageTitle = "Recuperar contraseña - " . YII::app()->name;

        $m = $_GET["m"];
        $clientes = Cliente::model()->findAll();
        foreach ($clientes as $value) {
            if (md5($value->correo_cliente) == $m) {
                $id_cliente = $value->id_cliente;
                $model = Cliente::model()->findByPk($id_cliente);
                $model->contrasena_cliente = "";
                if (isset($_POST['Cliente'])) {
                    $model->attributes = $_POST['Cliente'];
                    if ($model->contrasena_cliente === $model->repetir_contrasena) {
                        $model->contrasena_cliente = md5($model->contrasena_cliente);
                        if ($model->save()) {
                            Yii::app()->user->setFlash('success', "Se actualizaron los datos correctamente");
                            $this->redirect(array('site/login'));
                        }
                    } else {
                        Yii::app()->user->setFlash('error', "Las contraseñas no coinciden");
                        $this->render('reset', array("model" => $model));
                    }
                } else {
                    $this->render('reset', array("model" => $model));
                }
                break;
            }
        }
//        $this->render('reset');
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $identificacionemail = "<p>" . $model->body . "</p>";

                $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
                mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    public function actionRegistro() {
        $model = new Cliente('registro');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Cliente'])) {
            $fecha = date_create()->format('Y-m-d H:i:s');
            $model->attributes = $_POST['Cliente'];
            $model->contrasena_cliente = md5($model->contrasena_cliente);
            $model->fecha_creacion_cliente = $fecha;
            $model->id_estado_usuario = 1;
            if ($model->save()) {
                $this->redirect(array('login', 'id' => $model->id_cliente));
            }
        }
        $model->contrasena_cliente = "";
        $this->render('registro', array(
            'model' => $model,
        ));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if (isset($_GET[0]['r'])) {
                    $return = $_GET[0]['r'];
                    $this->redirect($return);
                } else {
                    $this->redirect(Yii::app()->user->returnUrl);
                }
//                $this->redirect($current);
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cliente-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
