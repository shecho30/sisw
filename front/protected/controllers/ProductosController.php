<?php

class ProductosController extends Controller {

    public $layout;
    public $pageTitle;
    public $rutaImagen;

    /*
     * Construccion ruta de bodega de imagenes
     */

    public function init() {
//        if (Yii::app()->user->isguest) {
//            $this->redirect(array('/site/login'));
//        }
        $scriptDir = "";
        $this->rutaImagen = $scriptDir . Yii::app()->params['imagePath'] . "/";
        parent::init();
    }

    /*
     * Reglas de acceso a las acciones
     */

    public function accessRules() {
        return array(array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('carrito', 'categoria', 'producto'),
                'users' => array('*')),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(),
                'users' => array('@')),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('index', 'pass', 'compras', 'confirmacion', 'pago'),
                'users' => array('admin')),
            array('deny', // deny all users
                'users' => array('*')));
    }

    /*
     * Pagina de despliegue de productos de categoria
     */

    public function actionCategoria($id) {
        Yii::app()->params['imagePath'];
        $categoria = Categoria::model()->findByPk($id);
        $dataProvider = new CActiveDataProvider('Producto', array(
            'criteria' => array(
                'condition' => 'cantidad_producto > 0 and id_estado_producto=1 and id_categoria=' . $id,
                'order' => 'nombre_producto ASC',
            ),
            'pagination' => array(
                'pageSize' => 12,
            ),
        ));
        $this->pageTitle = ucfirst($categoria->nombre_categoria) . " - " . YII::app()->name;
        $this->render('categoria', array("categoria" => $categoria, 'dataProvider' => $dataProvider));
    }

    /*
     * Pagina de despliegue detalle de producto
     */

    public function actionVerProducto($id) {
        $model = Producto::model()->findByPk($id);
        $criteria = new CDbCriteria;
        $criteria->addCondition('id_producto <> '.$id);
        $criteria->addCondition('id_estado_producto = 1');
        $criteria->addCondition('cantidad_producto > 0');
        $criteria->limit = 4;
        $relacionados = Producto::model()->findAll($criteria);
        $this->pageTitle = ucfirst($model->nombre_producto) . " - " . YII::app()->name;
        $this->render('producto', array("model" => $model, "relacionados"=>$relacionados));
    }

    public function actionCarrito() {
        $carrito = array();
        if (isset(Yii::app()->session['carrito'])) {
            $carrito = Yii::app()->session['carrito'];
        }
        $this->pageTitle = "Carrito - " . YII::app()->name;
        $this->render('carrito', array("carrito" => $carrito));
    }

    public function actionConfirmacion() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(array('/site/login', array("r"=>"productos/confirmacion")));
        }
        $model = new Factura;

        if (isset($_POST['Factura'])) {
            $model->attributes = $_POST['Factura'];
            $fra = Factura::model()->findAll();
            $iddian = 0;
            foreach ($fra as $valuex) {
                if ($valuex->id_dian_factura >= $iddian) {
                    $iddian = $valuex->id_dian_factura;
                }
            }

            $detalle = array();
            $carrito = Yii::app()->session['carrito'];
            if (count($carrito) > 0) {
                foreach ($carrito as $key => $value) {
                    $item = Producto::model()->findByPk($value["idproducto"]);
                    $totalindividual = $item->precio_salida_producto * $value["cantidad"];
                    $detalle_producto = array("id" => $value["idproducto"], "descripcion" => $item->nombre_producto, "cantidad" => $value["cantidad"], "stock" => $item->cantidad_producto, "precio" => $item->precio_salida_producto, "total" => $totalindividual);
                    array_push($detalle, $detalle_producto);
                }
            }

            $model->id_dian_factura = $iddian + 1;
            $model->detalle_factura = json_encode($detalle);
            $model->id_cliente = Yii::app()->user->id;
            $model->id_usuario = 1;
            if ($model->save()) {
                $totalProductos=0;
                foreach ($carrito as $key => $value) {
                    $item = Producto::model()->findByPk($value["idproducto"]);
                    $item->cantidad_producto= $item->cantidad_producto-$value["cantidad"];
                    $item->vendidos= $item->vendidos+$value["cantidad"];
                    $totalProductos=$totalProductos+$value["cantidad"];
                    $item->save();
                }
                $fecha = date_create()->format('Y-m-d H:i:s');
                $cliente= Cliente::model()->findByPk(Yii::app()->user->id);
                $cliente->compras=$cliente->compras+$totalProductos;
                $cliente->ultima_compra=$fecha;
                $cliente->save();
                unset(Yii::app()->session['carrito']);
                $this->redirect(array('pago', 'id' => $model->id_factura));
            }
        }

        $carrito = array();
        if (isset(Yii::app()->session['carrito'])) {

            $carrito = Yii::app()->session['carrito'];
        }
        $this->pageTitle = "Datos de pago - " . YII::app()->name;
        $this->render('confirmacion', array("carrito" => $carrito, 'model' => $model));
    }

    public function actionPago($id) {
        $this->pageTitle = "Gracias por su compra - " . YII::app()->name;
        $model= Factura::model()->findByPk($id);
        $this->render('pago', array("model"=>$model));
    }

    public function actionSetProduct() {
        $i = 0;
        $prod = null;
        $paso = array();
        $data = array("idproducto" => $_POST['id'], "nombre_producto" => $_POST['nomProducto'], "cantidad" => 1);
        $carrito = array();
        if (isset(Yii::app()->session['carrito'])) {
            $carrito = Yii::app()->session['carrito'];
        } else {
            $carrito = [];
        }

        foreach ($carrito as $value => $prod) {
            if ($prod["idproducto"] == $data["idproducto"]) {
                $prod["cantidad"] += 1;
                $paso = $prod;
                $key = $value;
                unset($carrito[$key]);
                $i = 1;
            }
        }

        if ($i == 1) {
            array_push($carrito, $paso);
        } else {
            array_push($carrito, $data);
        }


        if (empty($carrito)) {
            array_push($carrito, $data);
        }


        Yii::app()->session['carrito'] = $carrito;
        echo json_encode(count(Yii::app()->session['carrito']));
    }

    public function actionDelProduct() {
        $data = $_POST['id'];
        $sesion = Yii::app()->session['carrito'];
        if ($sesion) {
            foreach ($sesion as $valor => $arreglo) {
                if ($arreglo["idproducto"] == $data) {
                    $key = $valor;
                    unset($sesion[$key]);
                }
            }
        }


        Yii::app()->session['carrito'] = $sesion;

        $cont = json_encode(Yii::app()->session['carrito']);
        echo ($cont == 0) ? "" : $cont;
    }

    public function actionActuProduct() {
        $i = 1;
        $prod = null;
        $value = null;
        $paso = array();
        $data = array("idproducto" => $_POST['id'], "cantidad" => $_POST['cantidad']);
        $carrito = array();
        if (isset(Yii::app()->session['carrito'])) {
            $carrito = Yii::app()->session['carrito'];
        } else {
            $carrito = [];
        }

        foreach ($carrito as $value => $prod) {
            if ($prod["idproducto"] == $data["idproducto"]) {
                if ($i == 1) {
                    $paso = $prod;
                    if ($prod["cantidad"] > $data["cantidad"]) {
                        $paso["cantidad"] = $paso["cantidad"] - ($prod["cantidad"] - $data["cantidad"]);
                    } else if ($prod["cantidad"] < $data["cantidad"]) {
                        $paso["cantidad"] = $paso["cantidad"] + ($data["cantidad"] - $prod["cantidad"]);
                    }
                    $key = $value;
                    unset($carrito[$key]);
                    $i = 0;
                    array_push($carrito, $paso);
                }
            }
        }


        Yii::app()->session['carrito'] = $carrito;
        echo json_encode(count(Yii::app()->session['carrito']));
    }

    public function actionPruebaProduct() {
        
    }

}
