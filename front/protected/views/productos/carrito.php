

<div class="col-md-12 col-xs-12 title-carrito">
    <h1>Carrito de compras</h1>
</div>

<div class="col-md-9 col-xs-12">
    <?php

//    dump($carrito);
    $valor = 0;
    if (count($carrito) > 0) {
        foreach ($carrito as $key => $value) {
            $item = Producto::model()->findByPk($value["idproducto"]);
            $valor = $valor + ($item->precio_salida_producto * $value["cantidad"]);
            echo '<div id="prod' . $item->id_producto . '" class="cart-item row">
            <div class="image-container col-xs-2">
                <a href="#" title="' . ucfirst($item->nombre_producto) . '">
                    <img alt="' . $this->rutaImagen . $item->imagen_producto . '" src="' . $this->rutaImagen . $item->imagen_producto . '">
                </a>
            </div>
            <div class="item-title col-md-12 col-sm-11 col-xs-10">
                <span><a href="#">' . ucfirst($item->nombre_producto) . '</a></span>
                <div ng-switch="item.conditionTypeNote">
                    <!---->
                </div>
                
            </div>
            <div class="delete-section col-xs-1">
                <span class="glyphicon glyphicon-trash del-from-cart" data-id=' . $item->id_producto . ' aria-hidden="true"></span>
                
            </div>
            <div class="col-md-3 col-xs-8">
                <div class="price-section">
                    <div class="discount-container">
                        <span class="original-price">' . Helper::separarMiles($item->precio_salida_producto) . '</span>
                    </div>
                </div>
            </div>
            <div class="amount-section col-md-3 col-xs-12">
                <p>Cantidad:</p>
                <input type="number" class="form-control nuevaCantidadProducto" data-id='. $item->id_producto .' data-precio='. $item->precio_salida_producto .' name="nuevaCantidadProducto" min="1" max="'. $item->cantidad_producto .'" value='.$value["cantidad"].' id="cant" >
            </div>
            
        </div>';

        }
    } else {
        echo '<div class="cart-item row">
            <div class="image-container col-xs-2">
            No hay productos en el carrito.
            </div>
            </div>';
    }
    
    ?>


</div>

<!--sidebar-->
<div class="sidebar-carrito col-md-3 col-xs-12">
    <p class="title-summary">Resumen de tu orden</p>
    <p class="first">Total de productos: <span class="price-secondary pull-right"><?= count($carrito) ?></span></p>
    <hr>
    <p>Total: <input type="number" class="form-control nuevoCambioPrecio" id="nuevoCambioPrecio" value="<?=Helper::separarMiles($valor)?>"  readonly required></span></p>
    <input type="hidden" class="totalVenta" name="totalVenta" id="totalVenta" value="">
    <hr>
    <a href="<?php if (count($carrito) > 0) { echo Yii::app()->controller->createUrl("productos/confirmacion");}
                    else {echo Yii::app()->controller->createUrl("site/index");} ?>" class="btn btn-success col-xs-12">Comprar</a>
    <div class="clearfix"></div>
</div>

<script>

    $(document).ready(function () {

   
        var arrayJS = <?php echo json_encode($carrito);?>;
        var precioa = Number(<?php echo $valor;?>);
        var preciosindecimales = precioa.toFixed();
        $('.nuevoCambioPrecio').val(preciosindecimales);
        $('.totalVenta').val(precioa);

        

        $('.nuevaCantidadProducto').change(function(){
            var total = Number($('.totalVenta').val());
            var idp = $(this).data('id');
            var nuevac = $(this).val();
            var cantidadc = Number($(this).data('precio'));
            var cambio = 0;
            var final = 0;
//            console.log("prueba precio123", total);

            var data = [];
            data = {'id': $(this).data('id'),'cantidad': $(this).val()};

            Object.keys(arrayJS).forEach(function(value, key){
                if (arrayJS[value].idproducto == idp) {

                    if(arrayJS[value].cantidad > nuevac){
//                        console.log("antes del cambio1", arrayJS[value].cantidad);
                        cambio = cantidadc * (arrayJS[value].cantidad - nuevac)
                        total = total - cambio;
                        
                        arrayJS[value].cantidad = nuevac;

//                        console.log("despues del cambio1", arrayJS[value].cantidad);
//                        console.log("muestra del precio1", total);

                }else   if(arrayJS[value].cantidad < nuevac){
//                            console.log("antes del cambio2", arrayJS[value].cantidad);
                            cambio = cantidadc * (nuevac - arrayJS[value].cantidad)
                            total = total + cambio;
                            arrayJS[value].cantidad = nuevac;

//                            console.log("despues del cambio2", arrayJS[value].cantidad);
//                            console.log("muestra del precio2", total);
                        }
                        $('.totalVenta').val(total);
                        final = total.toFixed();
//                        console.log("muestra sin decimales", final);
                        $('.nuevoCambioPrecio').val(final);

                }
            })
            var data = [];
            data = {'id': $(this).data('id'),'cantidad': $(this).val()};
            $.post('<?= Yii::app()->controller->createUrl("productos/ActuProduct") ?>', data, function (response) {
  
            });
            
            

        });

        $('.del-from-cart').click(function () {
            var data = [];
            data = {'id': $(this).data('id')};
            $.post('<?= Yii::app()->controller->createUrl("productos/DelProduct") ?>', data, function (response) {
                location.reload();
            });
        });
    })

</script>