<?php
//$criteria = new cdbcriteria();
//$data = Productos::model()->finAll($criteria)
?>

<div class="single">
    <div class="col-md-9">
        <div class="single_grid">
            <div class="grid images_3_of_2">
                <ul id="etalage">
                    <li>
                        <img class="etalage_thumb_image img-responsive" src="<?php echo Yii::app()->request->baseUrl . "/" . $this->rutaImagen . $model->imagen_producto; ?>"  >
                    </li>

                </ul>
                <div class="clearfix"> </div>		
            </div> 
            <!---->

            <div class="span1_of_1_des">
                <div class="desc1">
                    <h3><?= ucfirst($model->nombre_producto) ?></h3>
                    <p><?= $model->description_producto ?></p>
                    <h5><?= Helper::separarMiles($model->precio_salida_producto); ?></h5>

                    <div class="form-in">
                        <a href="#"data-nombre="<?= $model->nombre_producto ?>" data-id="<?= $model->id_producto ?>" class="add-to-cart">Añadir al carrito</a>
                    </div>
                    <span class="span_right"><a href="#"></a></span>
                    <div class="clearfix"></div>
                </div>
                <div class="share-desc">
                    <div class="share">
                        <h4>Compartir Producto:</h4>
                        <ul class="share_nav">
                            <li><a href="https://www.facebook.com/SENAColombiaOficial/" target="_blank"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/facebook.png" title="facebook"></a></li>
                            <li><a href="https://twitter.com/senacomunica?lang=es" target="_blank"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/twitter.png" title="Twiiter"></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12" style="margin-bottom: 20px;"></div>
        </div>
        <div class="clearfix"></div>
    </div>

    <!----- tabs-box ---->

    <div class="row">

        <div class="col-sm-12" style="margin-bottom: 10px" >
            <h2>Quizas te interese...</h2>
        </div>

    </div>
    <div class="row">
        <?php
        
        foreach ($relacionados as $rel) {
//            echo $rel->id_producto;
            echo '<div class="col-lg-3 col-md-4 col-sm-6 item-producto">

            <a href="'.Yii::app()->controller->createUrl("productos/VerProducto", array("c" => Helper::createurlSlug($rel->idCategoria->nombre_categoria), "p" => Helper::createurlSlug($rel->nombre_producto), "id" => $rel->id_producto)) .'" title="'.$rel->nombre_producto.'">
                <img src="../'.$this->rutaImagen . $rel->imagen_producto.'" class="img-responsive" alt="'.$rel->nombre_producto.'">
            </a>
            <div class="tab_desc1">
                <h3>'.$rel->nombre_producto.'</h3>
                <p>'.Helper::separarMiles($rel->precio_salida_producto).'</p>
                <a href="#" data-nombre="'.ucfirst($rel->nombre_producto) .'" data-id="'. $rel->id_producto .'" class="to-cart add-to-cart"><span>Añadir al carrito</span>
                    <img src="'. Yii::app()->request->baseUrl.'/images/plus.png" alt="'. $rel->nombre_producto .'"></a>
            </div>
        </div>';
        }
        
        
        ?>
    </div>


