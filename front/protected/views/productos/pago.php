

<div class="col-md-12 col-xs-12 title-carrito">
    <h1>Gracias por su compra</h1>
</div>

<div class="col-md-6 col-xs-12">
    <?php
    $productos = json_encode($model->detalle_factura);
    $datoid = base64_encode($model->id_factura);

//    echo $productos;
    ?>


</div>

<div class="col-md-12 col-xs-12">
    <h2>Detalle compra # <?= $model->id_dian_factura ?></h2>
    
    <div class="clearfix">
        <table class="table table-striped table-bordered table-hover table-condensed text-left">
            <thead class="thead-dark">
                <tr>
                    <td>Item</td>
                    <td>Cantidad</td>
                    <td>Valor</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $data = json_decode($model->detalle_factura);
//                       dump($model);
                foreach ($data as $key => $item) {
                    echo "<tr>
                            <td>" . $item->descripcion . "</td>
                            <td>" . $item->cantidad . "</td>
                            <td>" . Helper::separarMiles($item->total) . "</td>
                            
                        </tr>";
                }
                ?>
            </tbody>
        </table>
        <div class="container">
                <div class="row">
                <div class="col-sm-5">
                <p style="valor"><strong>Total impuesto:</strong><?=  Helper::separarMiles($model->iva_factura) ?></p>
                <p style="valor"><strong>Total compra:</strong> <?= Helper::separarMiles($model->total_factura) ?></p>
                </div>

                <div class="col-md-1 col-md-offset-2"><button class="btn btn-info btnImprimirFactura" codigoVenta=" <?=$model["id_factura"]?> "><span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                      </button></div>

                
                </div>
            </div>
    </div>
</div>

<div class="col-md-12" style="margin-bottom: 10px;"></div>

<script>
$('.btnImprimirFactura').click(function () {
            var data = '<?php echo $datoid;?>';
            
            window.open("<?php echo Yii::app()->request->baseUrl."/".$this->rutaImagen."extensiones/tcpdf/pdf/factura.php?codigo=";?>"+data);

        });
</script>

