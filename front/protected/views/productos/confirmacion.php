<div class="col-md-12 col-xs-12 title-carrito">
    <h1>Datos de pago</h1>
</div>

<div class="col-md-12 col-xs-12">
    <?php
    $valor = 0;
    $totalindividual = 0;
    $detalle = array();
    if (count($carrito) > 0) {
        foreach ($carrito as $key => $value) {
            $item = Producto::model()->findByPk($value["idproducto"]);
            $totalindividual = $item->precio_salida_producto * $value["cantidad"];
            $valor = $valor + $totalindividual;
            $detalle_producto = array("id" => $value["idproducto"], "descripcion" => $item->nombre_producto, "cantidad" => $value["cantidad"], "stock" => $item->cantidad_producto, "precio" => $item->precio_salida_producto, "total" => $totalindividual);
            array_push($detalle, $detalle_producto);
        
        }
//        dump($detalle);
        $iva = $valor * 0.19;
        $total = $valor + $iva;
    }

    /*
     * [{"id":"4","descripcion":"taladro inalambrico","cantidad":"1","stock":"57","precio":"380","total":"380"}]
     */
    ?>


    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fac-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl("productos/confirmacion"),
    ));
    ?>
    <div class="clearfix">
    <?php echo $form->errorSummary($model); ?>
    </div>
    <div class="col-md-6 col-xs-12">
        <label>Detalle</label>
        <textarea class="form-control" rows="8" id="comment" readonly resize="false"> <?php
            if (count($carrito) > 0) {
                foreach ($carrito as $key => $value) {
                    $item = Producto::model()->findByPk($value["idproducto"]);
                    echo "producto: ".$item->nombre_producto." --- Cantidad: ".$value["cantidad"]."\n ";
                }
            }?>
        </textarea>
        
    </div>

    <div class="col-md-6 col-xs-12">
        <label>Forma de pago</label>
        <?php echo $form->dropDownList($model, 'id_forma_pago', CHtml::listData(FormaPago::model()->findAll(), 'id_forma_pago', 'nombre_forma_pago'), array('empty' => 'Seleccione forma de pago', 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'id_forma_pago'); ?>
    </div>
    <div class="col-md-6 col-xs-12">
        <label>Sub-Total</label>
        <input type="text" id="neto" value="<?= Helper::separarMiles($valor) ?>" class="form-control text-right" disabled="disabled" />
        <?php echo $form->hiddenField($model, 'neto_factura', array('value' => $valor)); ?>
    </div>
    <div class="col-md-6 col-xs-12">
        <label>IVA</label>
        <input type="text" id="neto" value="<?= Helper::separarMiles($iva) ?>" class="form-control text-right"  disabled="disabled" />
        <?php echo $form->hiddenField($model, 'iva_factura', array('value' => $iva)); ?>
        
    </div>
    <div class="col-md-12 col-xs-12">
        <label>Total</label>
        <input type="text" id="total" value="<?= Helper::separarMiles($total) ?>" class="form-control text-right" disabled="disabled" />
        <?php echo $form->hiddenField($model, 'total_factura', array('value' => $total)); ?>
        

        <?php echo CHtml::submitButton('Pagar', array('class' => 'btn btn-success col-xs-12 confirmado', 'style' => "margin-top: 20px;margin-bottom: 20px")); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>

<script>

//$(document).ready(function () {
//    var detalle_venta = <?php echo json_encode($detalle);?>;
//    console.log("prueba precio123", detalle_venta);
//    $('.confirmado').click(function () {
//        var data = [];
//        data = JSON.stringify(detalle_venta);;
//        $.post('<?= Yii::app()->controller->createUrl("productos/PruebaProduct") ?>', data, function (response) {
//            location.reload();
//        });
//    });
//})
</script>


