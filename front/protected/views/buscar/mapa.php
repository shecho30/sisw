
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" integrity="sha512-07I2e+7D8p6he1SIM+1twR5TIrhUQn9+I6yjqD53JQjFiMf8EtC93ty0/5vJTZGF8aAocvHYNEDJajGdNx1IsQ==" crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js" integrity="sha512-A7vV8IFfih/D732iSSKi20u/ooOfj/AGehOKq0f4vLT1Zr2Y+RX7C+w8A1gaSasGtRUZpF/NZgzSAu4/Gc41Lg==" crossorigin=""></script>

<section id="map-holder">
    <div id="mapid" style="min-height: 700px; width: 100%;"></div>


     <div class="container top-map-container">
        <div id="zonas">
            <div id="titulo-zonas" class="titulo">Zonas:</div>
            <?php //foreach ($lista_zonas as $key => $value) { ?>
            <?php foreach ($lista_zonas as $zona) { ?>
                <div class="zona" data-target="<?= $zona->nombre ?>"><?= $zona->nombre ?></div>
            <?php } ?>
            <div class="zona-todas">Ver todas</div>
        </div><!-- #zonas -->
    </div><!-- .conainer -->

   <div class="container bottom-map-container">
        <div id="tipos">
            <div id="titulo-tipos" class="titulo">Tipos:</div>
            <?php // foreach ($lista_tipos as $tipo => $value) { ?>
            <?php foreach ($lista_tipos as $tipo) { ?>
                <div class="tipo" data-target="<?= $tipo->nombre ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/pines/Apartamento.png" alt="<?= $tipo->nombre ?>"><?= $tipo->nombre ?></div>
                <!--<div class="tipo" data-target="<?= $tipo->nombre ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/pines/<?= $tipo->nombre ?>.png" alt="<?= $tipo->nombre ?>"><?= $tipo->nombre ?></div>-->
            <?php } ?>
            <div class="tipo-todos">Ver todas</div>
        </div><!-- #tipos -->
    </div><!-- .conainer -->

    <div id="pin-info-holder">
        <a id="pin-url" href="">
            <div id="image-holder" class="col-sm-6">
                <img id="pin-image" src="" alt="">
            </div><!-- #image-holder -->
            <div id="data-holder" class="col-sm-6">
                <div class="data-section">
                    <div class="tag">Barrio</div>
                    <div id="pin-nombre"></div>
                </div><!-- .data-section -->
                <div class="data-section">
                    <div class="tag">Valor</div>
                    <div id="pin-precio"></div>
                </div><!-- .data-section -->
                <div class="data-section">
                    <div class="tag">Área</div>
                    <div id="pin-area"></div>
                </div><!-- .data-section -->
            </div><!-- #data-holder -->
        </a>
        <div id="close-btn">&times;</div>
    </div><!-- #pin-info-holder -->

</section><!-- #map-holder -->


<script>
    var map;
    var marcadores = [];
    var database = [];
    var listaTipos = [];
    var listaZonas = []
	var map = L.map('mapid').setView([3.4397425017168017, -76.51771545410158], 12);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
			'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="http://mapbox.com">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(map);
    getData();
    
    
    function getData() {

        var request = $.getJSON("<?php echo Yii::app()->controller->createUrl("buscar/data"); ?>", function () {
//            console.log("success");
        })
                .done(function (data) {
                    $.each(data, function (key, val) {
                        //console.log( val );
                        database.push(val);
                    });
                    addPins();
                })
                .fail(function () {
                    //console.log("error");
                })
                .always(function () {
                    //console.log("complete");
                });

        addPins();

    }// getData();
    
    
    function addPins() {
        
        for (var i = 0; i < marcadores.length; i++) {
            map.removeControl(marcadores[i]);
        }
        marcadores = [];

//        console.log(marcadores);
        //console.log(database);

        if (database.length > 0) {
//            console.log(database.length);
            for (var i = 0; i < database.length; ++i) {
                var pin = database[i];
                
                if (filterTipo(pin.tipo) && filterZona(pin.zona)) {

                    if (pin.lat && pin.lon) {
                        var marker_icon = L.icon({
                            iconUrl: '<?php echo Yii::app()->request->baseUrl; ?>/images/pines/pin2h.png',
//                            iconUrl: '<?php echo Yii::app()->request->baseUrl; ?>/images/pines/' + pin.tipo + '.png',
                            iconSize: [38, 50],
                            iconAnchor: [19, 50],
                            popupAnchor: [0, -45]
                        });

                        var marker = L.marker([pin.lat, pin.lon], {icon: marker_icon});
                        marker.dataIndex = i;
                        marker.on('click', function ( ) {
                            map.setView(this.getLatLng(), 15);
                            showData(database[this.dataIndex]);
                        });
                        marcadores.push(marker);
                        marker.addTo(map);
                    }// if lat && lng

                }// if filters pased

            }// for data
        }//if data

    }// addPins()

    function filterZona(pinZona) {

        result = false;

        if (listaZonas.length > 0) {
            for (var i = 0; i < listaZonas.length; i++) {
                if (pinZona == listaZonas[i]) {
                    result = true;
                }// if pin.tipo == listazonas->i
           }// for listaZonass
        }// if listaZonas > 0
        else {
            result = true;
        }
        return result;

    }// filterZona()
    
    
    function showData(pin) {
        $("#pin-image").attr("src", "<?php echo Yii::app()->request->baseUrl; ?>/uploads/proyectos/thumbs/"+pin.image);
//        $("#pin-image").attr("src", "<?php echo Yii::app()->request->baseUrl; ?>/uploads/proyectos/thumbs/"+pin.image);
        $("#pin-url").attr("href", pin.url);
//        $("#pin-logo").attr("src", "<?php echo Yii::app()->request->baseUrl; ?>/uploads/logos_constructoras/"+pin.logo);
//        $("#pin-logo").attr("alt", pin.constructora);
        $("#pin-zona").html(pin.ciudad + "-" + pin.zona + "-" + pin.tipo);
        $("#pin-nombre").html(pin.nombre);
        $("#pin-precio").html(pin.valor);
        $("#pin-area").html(pin.area+"m<sup>2</sup>.");
        $("#pin-info-holder").show();
    }// showData()

    
    function filterTipo(pinTipo) {

        result = false;

        //console.log( "comparando "+pinTipo + " con " +listaTipos + ", size: "+listaTipos.length);

        if (listaTipos.length > 0) {
            for (var i = 0; i < listaTipos.length; i++) {
                //console.log( pinTipo + " = " +listaTipos[i] +" ?");
                if (pinTipo == listaTipos[i]) {
                    result = true;
                }// if pin.tipo == listatipos->i
            }// for listaTipos
        }// if listaTipos > 0
        else {
            result = true;
        }
        return result;

    }// filterTipo()

    
    $("#close-btn").click(function () {
        $("#pin-info-holder").hide();
    });
    function countFilterTipo() {
        var filters = $(".tipo").length;
        var activos = $(".tipo-selected").length;
        //console.log( "tipos: "+ filters +"vs"+ activos );
        if (filters == activos) {
            $(".tipo-todos").addClass("active");
        } else {
            $(".tipo-todos").removeClass("active");
        }
    }// countFilterTipo();
    
    $("#close-btn").click(function () {
        $("#pin-info-holder").hide();
    });
    
    $(".todas").click( toggleFooter );
    
    $(".tipo").click(function () {
        var target = $(this).data("target");
        //console.log( "target = "+target );

        if ($(this).hasClass("tipo-selected")) {
            $(this).removeClass("tipo-selected");
            for (var i = 0; i < listaTipos.length; i++) {
                if (listaTipos[i] == target) {
                    listaTipos.splice(i, 1);
                }
            }
        } else {
            //$(".categoria").removeClass("selected");
            $(this).addClass("tipo-selected");
            listaTipos.push(target);
        }
        countFilterTipo();
        addPins();
    });
    
    function countFilterTipo() {
        var filters = $(".tipo").length;
        var activos = $(".tipo-selected").length;
        //console.log( "tipos: "+ filters +"vs"+ activos );
        if (filters == activos) {
            $(".tipo-todos").addClass("active");
        } else {
            $(".tipo-todos").removeClass("active");
        }
    }// countFilterTipo();

    $(".tipo-todos").click(function () {

        $(this).toggleClass("active");
        var filters = $(".tipo").length;
        var activos = $(".tipo-selected").length;

        if (filters != activos) {
            listaTipos = [];
            $(".tipo").addClass("tipo-selected");
            $(".tipo").each(function () {
                var target = $(this).data("target");
                //console.log( "adding "+ target);
                listaTipos.push(target);
            });
        } else {
            listaTipos = [];
            $(".tipo").removeClass("tipo-selected");
        }

        //console.log( listaTipos );
        addPins();
    });




    $(".zona").click(function () {
        var target = $(this).data("target");

        if ($(this).hasClass("zona-selected")) {
            $(this).removeClass("zona-selected");
            for (var i = 0; i < listaZonas.length; i++) {
                if (listaZonas[i] == target) {
                    listaZonas.splice(i, 1);
                }
            }
        } else {
            $(this).addClass("zona-selected");
            listaZonas.push(target);
        }
        countFilterZona();
        addPins();
    });

    function countFilterZona() {
        var filters = $(".zona").length;
        var activos = $(".zona-selected").length;
        //console.log( "zonas: "+  filters +"vs"+ activos );
        if (filters == activos) {
            $(".zona-todas").addClass("active");
        } else {
            $(".zona-todas").removeClass("active");
        }
        //console.log(catSelected);
    }//countFilterZona()

    $(".zona-todas").click(function () {

        $(this).toggleClass("active");
        var filters = $(".zona").length;
        var activos = $(".zona-selected").length;

        if (filters != activos) {
            listaZonas = [];
            $(".zona").addClass("zona-selected");
            $(".zona").each(function () {
                var target = $(this).data("target");
                //console.log( "adding "+ target);
                listaZonas.push(target);
            });
        } else {
            listaZonas = [];
            $(".zona").removeClass("zona-selected");
        }

        //console.log( listaZonas );
        addPins();
    });

    $("#titulo-zonas").click(function () {
        videow = parseInt($(window).width());
        if (videow < 767) {
            $(".zona").fadeToggle();
            $(".zona-todas").fadeToggle();
        }
    });// ·titulo-zonas click

    $("#titulo-tipos").click(function () {
        videow = parseInt($(window).width());
        if (videow < 767) {
            $(".tipo").fadeToggle();
            $(".tipo-todos").fadeToggle();
        }
    });// ·titulo-zonas click


    function toggleFooter() {
        $("footer").fadeToggle();
        $("footer").toggleClass("floating");
    }
    
    </script>