<?php $this->widget('Buscador', array('id' => 'superior', 'tipo' => "sup")); ?>

<div class="col-lg-12 col-md-12">
    <div class="container margin--t1 margin--b1">
        <div class="row">
            <div class="col-lg-6 col-md-6 hidden-xs hidden-sm">
                <ul class="nav nav-pills breadcumb">
                    <li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl("site/index"); ?>">Inicio &gt;</a></li>
                    <li role="presentation" class="active"><a href="#">Buscar</a></li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 text-right">
<!--                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-default">Lista</button>
                    <button type="button" class="btn btn-default active">Foto</button>
                    <button type="button" class="btn btn-default">Mapa</button>
                </div>-->
            </div>
        </div>
    </div>
</div>

<!-- Search Result -->
<div class="col-lg-12 col-md-12 margin--t1 mi ">
    <div class="container">
        <!-- List block -->
        <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
            <!-- list items -->


            <?php
            $this->widget('zii.widgets.CListView', array(
                'id' => 'itemsLista',
                'dataProvider' => $dataProvider,
                'pager' => array(
                    'maxButtonCount' => 5,
                    'cssFile' => false,
                    'header' => "",
                    'htmlOptions' => array('class' => "pagination")
                ),
                'itemView' => '_view',
                'ajaxUpdate' => false,
                'cssFile' => false,
                'template' => "{items}\n{pager}",
//                'template' => "{headerpersonal}\n{items}\n{pager}",
                'sortableAttributes' => array(
                    'nombre',
                    'valorUnitario' => 'Valor',
                ),
            ));
            ?>



        </div>
        <div class="col-lg-4 col-md-4 hidden-xs hidden-sm">
            <!-- Search Form -->
            <div class="searchFormDefault">
                <?php $this->widget('Buscador', array('id' => 'main-search', 'tipo' => "home")); ?>

            </div>
        </div>
    </div>
</div>







<div class="col-lg-12 col-md-12 quickSearchBar hidden-xs hidden-sm margin--t1">
    <div class="row">
        <div class="container text-center">
            <form class="form-inline">
                <div class="form-group">
                    BUSCAR <strong>DE NUEVO</strong> &nbsp;|&nbsp;
                </div>
                <div class="radio">
                    <label class="radio-inline">
                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                        ARRENDAR
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                        COMPRAR
                    </label>
                </div>
                <div class="form-group">
                    <select class="form-control">
                        <option value="">Tipo de inmueble</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control">
                        <option value="">Sector de la ciudad</option>
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control">
                        <option value="">Tipo de inmueble</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn search-btn" value="BUSCAR" >
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12">
    <div class="container margin--t1 margin--b1">
        <div class="row">
            <div class="col-lg-6 col-md-6 hidden-xs hidden-sm">
                <ul class="nav nav-pills breadcumb">

                    <li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl("site/index"); ?>">Inicio ></a></li>
                    <li role="presentation" class="active"><a href="#">Inmuebles ></a></li>
                    <!--<li role="presentation"><a href="#">Arrendar ></a></li>-->
                    <!--          <li role="presentation"><a href="#">Apartamento ></a></li>
                              <li role="presentation"><a href="#">Cali ></a></li>
                              <li role="presentation" class="active"><a href="#">Sur</a></li>-->
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 text-right">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-default">Lista</button>
                    <button type="button" class="btn btn-default active">Foto</button>
                    <button type="button" class="btn btn-default">Mapa</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 paginationBar hidden-xs hidden-sm">
    <div class="row">
        <div class="container">
            <div class="col-lg-4 col-md-4 text-center veticaladd">
                <p>1 al 12 de 20</p>
            </div>
            <div class="col-lg-4 col-md-4 text-center">
                <nav aria-label="Page navigation">
                    <h4 class="text-uppercase">Usted encontro <strong>20 RESULTADOS</strong></h4>
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                Anterior
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                Siguiente
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-4 col-md-4 veticaladd">
                <form>
                    <div class="form-group">
                        <select class="form-control">
                            <option value="">Ordernar por</option>
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




