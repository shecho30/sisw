<div class="col-lg-3 col-md-4 col-sm-6">
    <a href="<?= Yii::app()->controller->createUrl("productos/VerProducto", array("c" => Helper::createurlSlug($data->idCategoria->nombre_categoria), "p" => Helper::createurlSlug($data->nombre_producto), "id" => $data->id_producto)) ?>" title="<?= $data->nombre_producto ?>">
        <img src="<?= $this->rutaImagen . $data->imagen_producto ?>" class="img-responsive" alt="<?= ucfirst($data->nombre_producto) ?>">
    </a>
    <div class="tab_desc1">
        <h3><?= ucfirst($data->nombre_producto) ?></h3>
        <p><?= Helper::separarMiles($data->precio_salida_producto); ?></p>
        <a href="#" data-nombre="<?=ucfirst($data->nombre_producto)?>" data-id="<?= $data->id_producto ?>" class="to-cart add-to-cart"><span>Añadir al carrito</span><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/plus.png" alt="<?=ucfirst($data->nombre_producto)?>"></a>
    </div>
</div>