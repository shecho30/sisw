<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
        <!-- Custom Theme files -->

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous"/>
        <!-- Tema opcional -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous"/>
        <!-- JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--fonts-->
        <link href='http://fonts.googleapis.com/css?family=Libre+Baskerville:400,700,400italic' rel='stylesheet' type='text/css' />
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css' />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css' />
        <!--//fonts-->

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/complemento.css" rel="stylesheet" type="text/css" media="all" />	
    </head>
    <body> 
        <!--header-->
        <div class="header">
            <div class="header-top  navbar-fixed-top">
                <div class="container">
                    <div class="header-gridd col-lg-6 col-md-6 col-sm-12 text-right pull-right">
                        <?php $this->widget('UserLoginForm', array("id"=>"loginform")); ?>
                    </div>

                    <div class="clearfix"> </div>
                </div>
            </div>
            
            <div class="container" style="margin-top: 100px;">
                <div class="header-bottom">			
                    <div class="logo">
                        <a href="<?php echo Yii::app()->controller->createUrl("site/index"); ?>">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/Logo/2logo.png" class="img-fluid" style="height: auto; width: 100%" />
                        </a>
                    </div>
                    <div class="ad-right">

                    </div>
                    <div class="clearfix"> </div>
                </div>	
                <?php $this->widget('Menu', array("tipo" => "top")); ?> 
            </div>
        </div>
        <!--content-->

            <div class="container">
                <?php echo $content; ?>
            </div>
        <!---->
        <div class="footer">
            <div class="container">
                <div class="footer-class">
                    <div class="class-footer">
                        <ul>
                            <li><a href="<?php echo Yii::app()->controller->createUrl("site/index"); ?>" class="scroll">Inicio </a><label>|</label></li>
                            <li><a href="<?php echo Yii::app()->controller->createUrl("site/terminos"); ?>" class="scroll">Terminos y condiciones</a><label>|</label></li>
                            <li><a href="<?php echo Yii::app()->controller->createUrl("site/privacidad"); ?>" class="scroll">Privacidad</a><label>|</label></li>
                            <li><a href="<?php echo Yii::app()->controller->createUrl("site/contact"); ?>" class="scroll">Contacto </a></li>
                        </ul>
                        <p class="footer-grid">&copy; 2018 Derechos Reservados  #ADSI-150 </p>
                    </div>	 
                    <div class="footer-left">
                        <a href="<?php echo Yii::app()->controller->createUrl("site/index"); ?>">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/Logo/3logo.png" class="img-fluid" style="height: auto; width: 100%"/>
                        </a>
                    </div> 
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.add-to-cart').click(function () {
                    var data = [];
                    data = {'id': $(this).data('id'), 'nomProducto': $(this).data('nombre')};
                    $.post('<?= Yii::app()->controller->createUrl("productos/SetProduct") ?>', data, function (response) {
                        //swal( 'Agregado!', 'Se agrego! '+data.nomProducto, 'success' );
                        alert("Elemento agregado: " + data.nomProducto);
                        $('.contcarr').html(response);

                    });
                    return false;
                });
            });
        </script>
    </body>
</html>