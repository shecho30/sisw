<h1 class="">Mi cuenta</h1>
        <ul>
            <li><a href="<?=Yii::app()->controller->createUrl("usuario/index")?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Mis datos</a></li>
            <li><a href="<?=Yii::app()->controller->createUrl("usuario/pass")?>"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Cambiar contraseña</a></li>
            <li><a href="<?=Yii::app()->controller->createUrl("usuario/compras")?>"><span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span> Mis compras</a></li>
        </ul>