<div class="row">
    <div id="micuenta" class="container">
        <div id="" class="col-md-3 micuenta">
            <?php $this->renderPartial("_menu_cuenta"); ?>
        </div>
        <div class="col-md-9">
            <h2>Detalle compra # <?= $model->id_dian_factura ?></h2>
            <div class="clearfix">
                <table class="table table-striped table-bordered table-hover table-condensed text-left">
                    <thead class="thead-dark">
                        <tr>
                            <td>Item</td>
                            <td>Cantidad</td>
                            <td>Valor</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $data = json_decode($model->detalle_factura);
                        $datoid = base64_encode($model->id_factura);
//                       dump($model);
                        foreach ($data as $key => $item ) {
                            echo "<tr>
                            <td>" . $item->descripcion . "</td>
                            <td>" . $item->cantidad . "</td>
                            <td>" . Helper::separarMiles($item->total) . "</td>
                            
                        </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            <div class="container">
                <div class="row">
                <div class="col-sm-5">
                <p style="valor"><strong>Total impuesto:</strong><?=  Helper::separarMiles($model->iva_factura) ?></p>
                <p style="valor"><strong>Total compra:</strong> <?= Helper::separarMiles($model->total_factura) ?></p>
                </div>

                <div class="col-md-1 col-md-offset-2"><button class="btn btn-info btnImprimirFactura" ><span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                      </button></div>

                
                </div>
            </div>       
            </div>
        </div>
    </div>

</div>

<script>
$('.btnImprimirFactura').click(function () {
            var data = '<?php echo $datoid;?>';
            
            window.open("<?php echo Yii::app()->request->baseUrl."/".$this->rutaImagen."../sis/extensiones/tcpdf/pdf/factura.php?codigo="; ?>"+data);

        });
</script>




