<div class="row">
    <div id="micuenta" class="container">
        <div id="" class="col-md-3 micuenta">
            <?php $this->renderPartial("_menu_cuenta"); ?>
        </div>
        <div class="col-md-9">
            <h2>Mis compras</h2>
            <div class="clearfix">
                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <td>#</td>
                            <td>Valor</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $this->widget('zii.widgets.CListViewmm', array(
                        'id' => 'itemsListaFacturas',
                        'dataProvider' => $dataProvider,
                        'itemView' => '_facturas',
                        'ajaxUpdate' => false,
                        'template' => "{pager}\n{items}",
                        'cssFile' => false,
                        'htmlOptions' => array('class' => "clistWidgetView")
                    ));
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>




