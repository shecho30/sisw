<div class="row">
    <div id="micuenta" class="container">
        <div class="col-md-3 micuenta">
            <h1>Mi cuenta</h1>
            <ul>
                <li><a href="<?= Yii::app()->controller->createUrl("usuario/index") ?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Mis datos</a></li>
                <li><a href="<?= Yii::app()->controller->createUrl("usuario/pass") ?>"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Cambiar contraseña</a></li>
                <li><a href="<?= Yii::app()->controller->createUrl("usuario/compras") ?>"><span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span> Mis compras</a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="col-md-12">
                <h2>Cambiar contraseña</h2>
                <?php if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>
                <?php if (Yii::app()->user->hasFlash('error')): ?>
                    <div class="alert alert-warning fade in alert-dismissible" style="margin-top:18px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <?php echo Yii::app()->user->getFlash('error'); ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'cliente-form',
                'enableAjaxValidation' => false,
            ));
            ?>


            <?php echo $form->errorSummary($model); ?>

            <div class="col-md-12">
                <?php echo $form->labelEx($model, 'contrasena_cliente'); ?>
                <?php echo $form->passwordField($model, 'contrasena_cliente', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
                <?php echo $form->error($model, 'contrasena_cliente'); ?>
            </div>

            <div class="col-md-12">
                <?php echo $form->labelEx($model, 'repetir_contrasena'); ?>
                <?php echo $form->passwordField($model, 'repetir_contrasena', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
                <?php echo $form->error($model, 'repetir_contrasena'); ?>
            </div>

            <div class="col-md-12 buttons" style="margin-bottom: 5px">
                <?php echo CHtml::submitButton('Cambiar', array("class" => "btn btn-success ")); ?>
            </div>


            <?php $this->endWidget(); ?>
        </div>
    </div>

</div>




