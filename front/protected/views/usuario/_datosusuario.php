<div class="col-md-12">
    <h2>Mis datos</h2>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
</div>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cliente-form',
        'enableAjaxValidation' => false,
    ));
    ?>


    <?php echo $form->errorSummary($model); ?>

    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'document_cliente'); ?>
        <?php echo $form->textField($model, 'document_cliente', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'document_cliente'); ?>
    </div>

    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'id_tipo_documento'); ?>
        <?php echo $form->dropDownList($model, 'id_tipo_documento', CHtml::listData(TipoDocumento::model()->findAll(), 'id_tipo_documento', 'nombre_tipo_documento'), array('empty' => 'Tipo de documento', 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'id_tipo_documento'); ?>
    </div>

    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'empresa_cliente'); ?>
        <?php echo $form->textField($model, 'empresa_cliente', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'empresa_cliente'); ?>
    </div>

    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'nombre_repre_cliente'); ?>
        <?php echo $form->textField($model, 'nombre_repre_cliente', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'nombre_repre_cliente'); ?>
    </div>

    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'apellido_repre_cliente'); ?>
        <?php echo $form->textField($model, 'apellido_repre_cliente', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'apellido_repre_cliente'); ?>
    </div>

    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'telefono_cliente'); ?>
        <?php echo $form->textField($model, 'telefono_cliente', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'telefono_cliente'); ?>
    </div>

    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'direccion_cliente'); ?>
        <?php echo $form->textField($model, 'direccion_cliente', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'direccion_cliente'); ?>
    </div>

    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'correo_cliente'); ?>
        <?php echo $form->textField($model, 'correo_cliente', array('size' => 50, 'maxlength' => 50, 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'correo_cliente'); ?>
    </div>




    <div class="col-md-12">
        <?php echo $form->labelEx($model, 'id_ciudad'); ?>
        <?php echo $form->dropDownList($model, 'id_ciudad', CHtml::listData(Ciudad::model()->findAll(), 'id_ciudad', 'nombre_ciudad'), array('empty' => 'Seleccione ciudad', 'class' => "form-control")); ?>
        <?php echo $form->error($model, 'id_ciudad'); ?>
    </div>

    <div class="col-md-12 buttons">
        <?php echo CHtml::submitButton('Guardar', array("class" => "btn btn-success ")); ?>
    </div>

    <?php $this->endWidget(); ?>
