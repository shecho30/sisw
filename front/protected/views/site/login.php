<h1>Iniciar sesion</h1>


<div class="form clearfix" style="margin-bottom: 3em">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>


    <div class="col-md-6 text-center col-md-offset-3">
        <?php echo $form->labelEx($model, 'username'); ?>
        <?php echo $form->textField($model, 'username', array("class"=>"form-control")); ?>
        <?php echo $form->error($model, 'username'); ?>
    </div>

    <div class="col-md-6 text-center col-md-offset-3">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password', array("class"=>"form-control")); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="col-md-6 text-center col-md-offset-3" style="margin-top: .0.5em">
        <?php echo CHtml::submitButton('Entrar', array("class"=>"btn btn-success")); ?>
    </div>

<?php $this->endWidget(); ?>
    <div class="clearfix"></div>
</div><!-- form -->

<div class="col-md-6 text-center col-md-offset-3" style="margin-bottom: 2em">
    <a href="<?php echo Yii::app()->controller->createUrl("site/recordarpass"); ?>" >¿Olvido la contraseña?</a>
    </div>
