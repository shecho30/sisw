<?php
$this->pageTitle = Yii::app()->name . ' - Contact Us';
$this->breadcrumbs = array(
    'Contact',
);
?>

<h1>Contacto</h1>



<div class="contact">
    <div class="contact-in">

        <div class=" col-md-9 contact-left">
            <?php if (Yii::app()->user->hasFlash('contact')): ?>

                <div class="flash-success">
                    <?php echo Yii::app()->user->getFlash('contact'); ?>
                </div>

            <?php else: ?>

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'contact-form',
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnSubmit' => true,
                    ),
                ));
                ?>


                <?php // echo $form->errorSummary($model); ?>

                <div class="row">
                    <span><?php echo $form->labelEx($model, 'name'); ?></span>
                    <?php echo $form->textField($model, 'name'); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>

                <div class="row">
                    <span><?php echo $form->labelEx($model, 'email'); ?></span>
                    <?php echo $form->textField($model, 'email', array("class" => "textbox")); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>

                <div class="row">
                    <span><?php echo $form->labelEx($model, 'telefono'); ?></span>
                    <?php echo $form->textField($model, 'telefono', array("class" => "textbox")); ?>
                    <?php echo $form->error($model, 'telefono'); ?>
                </div>

                <div class="row">
                    <span><?php echo $form->labelEx($model, 'subject'); ?></span>
                    <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 128)); ?>
                    <?php echo $form->error($model, 'subject'); ?>
                </div>

                <div class="row">
                    <span><?php echo $form->labelEx($model, 'body'); ?></span>
                    <?php echo $form->textArea($model, 'body', array('rows' => 6, 'cols' => 50)); ?>
                    <?php echo $form->error($model, 'body'); ?>
                </div>

                <?php if (CCaptcha::checkRequirements()): ?>
                    <div class="row">
                        <?php echo $form->labelEx($model, 'verifyCode'); ?>
                        <div>
                            <?php $this->widget('CCaptcha'); ?>
                            <?php echo $form->textField($model, 'verifyCode'); ?>
                        </div>
                        <div class="hint">Please enter the letters as they are shown in the image above.
                            <br/>Letters are not case-sensitive.</div>
                        <?php echo $form->error($model, 'verifyCode'); ?>
                    </div>
                <?php endif; ?>

                <div class="row buttons">
                    <?php echo CHtml::submitButton('Enviar'); ?>
                </div>

                <?php $this->endWidget(); ?>

            <?php endif; ?>


        </div>

        <div class=" col-md-3 contact-right">
            <h5>Informacion de la Compañia</h5>
            <p>Calle 52 2 BIS - 15.</p>
            <p>Cali, Valle del Cauca</p>
            <p>Colombia</p>
            <p>Telefono:(+57) 317 2429481</p>
            <p>Fax: (000) 000 00 00 0</p>
            <p>Email: adsi150sena@hotmail.com</p>

        </div>
        <div class="clearfix"></div>
    </div>

    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.5142527725707!2d-76.50278443480822!3d3.46744625207933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e30a7cb4223ea99%3A0x60b26f8a8c3496eb!2sCentro+De+Electricidad+Y+Automatizaci%C3%B3n+Industrial+(C.E.A.I)!5e0!3m2!1ses!2sco!4v1540166292612" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
