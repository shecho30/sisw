<style>
    .errorMessage{ width:100% }
</style>
<h1>Restaurar contraseña</h1>
<?php if (Yii::app()->user->hasFlash('error')) { ?>
    <div class="col-md-12 flash-error alert alert-error fade in alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php } ?>
<?php if (Yii::app()->user->hasFlash('warning')) { ?>
    <div class="col-md-12 flash-warning alert alert-warning fade in alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
        <?php echo Yii::app()->user->getFlash('warning'); ?>
    </div>
<?php } ?>

<div class="form clearfix" style="margin-bottom: 3em">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'email-form',
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => false,
        ),
    ));
    ?>

    <div class="col-md-5 text-center col-md-offset-3">
        <?php echo $form->labelEx($model, 'contrasena_cliente'); ?>
        <?php echo $form->passwordField($model, 'contrasena_cliente', array("class" => "form-control", "required"=>"required")); ?>
        <?php echo $form->error($model, 'contrasena_cliente'); ?>
    </div>
    

    <div class="col-md-5 text-center col-md-offset-3">
        <?php echo $form->labelEx($model, 'repetir_contrasena'); ?>
        <?php echo $form->passwordField($model, 'repetir_contrasena', array("class" => "form-control", "required"=>"required")); ?>
        
    </div>


    <div class="col-md-5 text-center col-md-offset-3" style="margin-top: 10px; margin-bottom: 10px;">
        <?php echo CHtml::submitButton('Restaurar', array("class" => "btn btn-success")); ?>
    </div>

    <?php $this->endWidget(); ?>
    <div class="clearfix"></div>
</div><!-- form -->
