<?php
$this->pageTitle = Yii::app()->name . ' - Error';
$this->breadcrumbs = array(
    'Error',
);
?>
<div class="col-md-12">
     <h1>Error <?php echo $code; ?></h1>

    <div class="error">
        <?php echo CHtml::encode($message); ?>
    </div>
</div>