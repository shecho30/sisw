

<div class="women-in">

    <div class="col-md-9  col-xs-12 no-gutter">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Carousel indicators -->
            <?php
            $cont = 0;
            $li = "";
            $itemsCarousel = "";
            ?>
            <ol class="carousel-indicators">
                <?php
                foreach ($promo as $promocion) {
                    if ($promocion->posicion_promocion == 1) {
                        if ($cont == 0) {
                            $class = "active";
                        }
                        echo '<li data-target="#myCarousel" data-slide-to="' . $cont . '"  class="' . $class . '"></li>';
                        $itemsCarousel = $itemsCarousel . '<div class="item ' . $class . '">
                            <a href="' . $promocion->enlace_promocion . '">'
                                . '<img src="' . $this->rutaImagen  . $promocion->imagen_promocion . '" alt="' . $promocion->id_promocion . '">
                            </a>
                            </div>';
                        $cont++;
                        $class = "";
                    }
                }
                ?>
            </ol>   
            <!-- Wrapper for carousel items -->
            <div class="carousel-inner">
                <?php echo $itemsCarousel; ?>
            </div>
            <!-- Carousel controls -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>


        <div class="in-line">
            <div class="para-an">
                <h3>Temporada de descuentos!</h3>
                <p>Consulte nuestros últimos productos en esta sección.</p>
            </div>
            <div class="lady-in">
                <?php
                foreach ($productos as $value) {
                    echo '<div class="col-md-4 you-para">
                    <a href="' . Yii::app()->controller->createUrl("productos/VerProducto", array("c" => Helper::createurlSlug($value->idCategoria->nombre_categoria), "p" => Helper::createurlSlug($value->nombre_producto), "id" => $value->id_producto)) . '">
                        <img class="img-responsive pic-in" src="' . $this->rutaImagen . $value->imagen_producto . '" alt=" " ></a>
                    <p>' . ucfirst($value->nombre_producto) . '</p>
                    <span>' . Helper::separarMiles($value->precio_salida_producto) . '  | <label class="cat-in"> </label> <a href="#" class="add-to-cart" data-nombre="' . $value->nombre_producto . '"   data-id="' . $value->id_producto . '" >Añadir al carrito</a></span>
                </div>';
                }
                ?>

                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <?php
        $cont = 0;
        $pos2 = "";
        $pos3 = "";
        foreach ($promo as $promocion) {
            if ($promocion->posicion_promocion == 2) {
                $pos2 = '<a href="' . $promocion->enlace_promocion . '"><img class="img-responsive pic-in" src="' . $this->rutaImagen . $promocion->imagen_promocion . '" alt=" " >	</a>
                        <a href="' . $promocion->enlace_promocion . '" class="know-more">Conocer Más</a>';
                break;
            }

            if ($promocion->posicion_promocion == 3) {
                $pos3 = '<a href="' . $promocion->enlace_promocion . '"><img class="img-responsive pic-in" src="' . $this->rutaImagen . $promocion->imagen_promocion . '" alt=" " >	</a>
                        <a href="' . $promocion->enlace_promocion . '" class="know-more">Conocer Más</a>';
                break;
            }
        }
        if ($pos2 != "") {
            echo '<div class="discount">' . $pos2 . '</div>';
        }
        if ($pos3 != "") {
            echo '<div class="discount">' . $pos3 . '</div>';
        }
        ?>

        <!--        <div class="discount">
                    <a href="single.html"><img class="img-responsive pic-in" src="<?php echo Yii::app()->request->baseUrl; ?>/images/p3.jpg" alt=" " ></a>			
                <p class="no-more no-get">Obtiene<b>descuentos exclusivos </b> <span>en ropa para mujer</span></p>					
                <a href="#" class="know-more">Ver más</a>
            </div>-->
        <div class="twitter-in">
            <a class="twitter-timeline" data-lang="es" data-height="300" href="https://twitter.com/SENAComunica?ref_src=twsrc%5Etfw">Tweets by SENAComunica</a> 
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

        </div>
    </div>
    <div class="clearfix"> </div>
</div>
<!---->