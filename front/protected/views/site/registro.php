<h1>Registro</h1>


<div class="register">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cliente-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <div class="register-top-grid">
        <h3>INFORMACION PERSONAL</h3>

        <?php echo $form->errorSummary($model); ?>

        <div class="mation">
            <span><?php echo $form->labelEx($model, 'document_cliente'); ?></span>
            <?php echo $form->textField($model, 'document_cliente', array('size' => 50, 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'document_cliente'); ?>

            <span> <?php echo $form->labelEx($model, 'id_tipo_documento'); ?></span>
            <?php echo $form->dropDownList($model, 'id_tipo_documento', CHtml::listData(TipoDocumento::model()->findAll(), 'id_tipo_documento', 'nombre_tipo_documento'), array('empty' => 'Tipo de documento', 'class' => "form-controll")); ?>
            <?php echo $form->error($model, 'id_tipo_documento'); ?>

            <span> <?php echo $form->labelEx($model, 'empresa_cliente'); ?></span>
            <?php echo $form->textField($model, 'empresa_cliente', array('size' => 50, 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'empresa_cliente'); ?>


            <span><?php echo $form->labelEx($model, 'nombre_repre_cliente'); ?></span>
            <?php echo $form->textField($model, 'nombre_repre_cliente', array('size' => 50, 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'nombre_repre_cliente'); ?>

            <span> <?php echo $form->labelEx($model, 'apellido_repre_cliente'); ?></span>
            <?php echo $form->textField($model, 'apellido_repre_cliente', array('size' => 50, 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'apellido_repre_cliente'); ?>

            <span>  <?php echo $form->labelEx($model, 'telefono_cliente'); ?></span>
            <?php echo $form->textField($model, 'telefono_cliente', array('size' => 50, 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'telefono_cliente'); ?>

            <span><?php echo $form->labelEx($model, 'direccion_cliente'); ?></span>
            <?php echo $form->textField($model, 'direccion_cliente', array('size' => 50, 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'direccion_cliente'); ?>
            
            <span><?php echo $form->labelEx($model, 'id_ciudad'); ?></span>
            <?php echo $form->dropDownList($model, 'id_ciudad', CHtml::listData(Ciudad::model()->findAll(), 'id_ciudad', 'nombre_ciudad'), array('empty' => 'Seleccione ciudad', 'class' => "form-controll")); ?>
            <?php echo $form->error($model, 'id_ciudad'); ?>

            <span><?php echo $form->labelEx($model, 'correo_cliente'); ?></span>
            <?php echo $form->textField($model, 'correo_cliente', array('size' => 50, 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'correo_cliente'); ?>

            <span> <?php echo $form->labelEx($model, 'contrasena_cliente'); ?></span>
            <?php echo $form->passwordField($model, 'contrasena_cliente', array('size' => 60, 'maxlength' => 60)); ?>
            <?php echo $form->error($model, 'contrasena_cliente'); ?>
            
            <span> <?php echo $form->labelEx($model, 'repetir_contrasena'); ?></span>
            <?php echo $form->passwordField($model, 'repetir_contrasena', array('size' => 60, 'maxlength' => 60)); ?>
            <?php echo $form->error($model, 'repetir_contrasena'); ?>
            


            <div class="clearfix"> </div>
            <a class="news-letter" href="#">
                <label class="checkbox"><input type="checkbox" required name="checkbox" ><i> </i>Acepto los terminos y condiciones de uso.</label>
            </a>
            
            

            <div class="row buttons">
                <?php echo CHtml::submitButton('Registrarse', array("class"=>"btn btn-success") ); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div>