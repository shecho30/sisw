<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Sisprova - ADSI 150',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
    ),
    'language' => 'es', // Este es el lenguaje en el que quieres que muestre las cosas
    'sourceLanguage' => 'en', // Este es el lenguaje por defecto de los archivos
    'defaultController' => 'site/index', // Vista por defecto
    // application components
    'components' => array(
        'widgetFactory' => array(
            'widgets' => array(
                'CLinkPager' => array(
                    'cssFile' => false,
                ),
            ),
        ),
        'user' => array(
            'allowAutoLogin' => true,
            'returnUrl' => array('site/index'),
            'loginUrl' => array('site/login'),
        ),
        'clientScript' => array(
            'packages' => array(
                'jquery' => array(
                    'baseUrl' => 'js',
                    'js' => array('jquery-1.12.3.min.js')
                ),
            ),
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
//            'urlSuffix' => '.html',
            'rules' => require(dirname(__FILE__) . '/urlrules.php'),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=db_sisprova_qa',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
        'session' => array(
            'class' => 'system.web.CDbHttpSession',
            'connectionID' => 'db',
            'sessionTableName' => 'usersess',
            'timeout' => 3600,
            'cookieMode' => 'allow',
            'cookieParams' => array(
                'path' => '/',
                //'domain' => 'www.2h.com.co',
                'httpOnly' => true,
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'adminEmail' => 'ajcobo@misena.edu.co',
        'imagePath' => '../sis',
    ),
);
