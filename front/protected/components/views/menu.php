<?php

?>
<nav id="menuppal" class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-ex1-collapse">
            <span class="sr-only">Desplegar navegación</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?=Yii::app()->controller->createUrl("site/index")?>">INICIO</a>
    </div>

    <!-- Agrupar los enlaces de navegación, los formularios y cualquier
         otro elemento que se pueda ocultar al minimizar la barra -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <?php
            foreach ($cat as $value) {
                echo '<li><a href="' . Yii::app()->controller->createUrl("productos/categoria", array("c" => Helper::createurlSlug($value->nombre_categoria), "id" => $value->id_categoria)) . '">' . strtoupper($value->nombre_categoria) . '</a></li>';
            }
            ?>
            
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                </a>
                <ul class="dropdown-menu">
                    <li><a href="#">Acción #1</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Acción #2</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Acción #3</a></li>
                </ul>
            </li>
        </ul>
        <div class="search">
            <?php $this->widget('Buscador', array("id"=>"buscador")); ?> 
        </div>


        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                </a>
                <ul class="dropdown-menu">
                    <li><a href="#">Acción #1</a></li>
                    <li><a href="#">Acción #2</a></li>
                    <li><a href="#">Acción #3</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Acción #4</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>

