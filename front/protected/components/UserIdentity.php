<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    public $_id;
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $record = Cliente::model()->findByAttributes(array('correo_cliente' => $this->username, 'id_estado_usuario' => '1'));
        
        
        if ($record === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if ($record->contrasena_cliente != md5($this->password))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
        
            $this->_id = $record->id_cliente;
//            $this->setState('name', $record->username. " ". $record->apellidos);
            $this->setState('name', $record->nombre_repre_cliente);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
    
    
    public function getId() {
        return $this->_id;
    }

}
