<?php

/**
 * @author Antonio Cobo <ajcobo@elpais.com.co>
 * @version 1.0
 * @package components
 * @since 1.0
 */
Yii::import('zii.widgets.CPortlet');

class Buscador extends CPortlet {
    /*
     * Buscador del header
     */


    protected function renderContent() {
        $buscador = new Producto('mbuscador');
        $buscador->unsetAttributes();
        $pro = filter_input(INPUT_GET, 'Producto');
        $this->render('buscador', array('buscador' => $buscador));
    }

}
