<?php

/**
 * @author Antonio Cobo <ajcobo@elpais.com.co>
 * @version 1.0
 * @package components
 * @since 1.0
 */
Yii::import('zii.widgets.CPortlet');

class UserLoginForm extends CPortlet {


    protected function renderContent() {
        $model = new LoginForm;
        $contador =0;
        if(isset(Yii::app()->session['carrito'])){
            $contador = count(Yii::app()->session['carrito']);  
        }
        $this->render('loginform', array("model"=>$model,"contador"=>$contador));
    }

}
