<?php

/**
 * CListView class file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

Yii::import('application.components.CBaseListViewmm');

/**
 * CListView displays a list of data items in terms of a list.
 *
 * Unlike {@link CGridView} which displays the data items in a table, CListView allows one to use
 * a view template to render each data item. As a result, CListView could generate more flexible
 * rendering result.
 *
 * CListView supports both sorting and pagination of the data items. The sorting
 * and pagination can be done in AJAX mode or normal page request. A benefit of using CListView is that
 * when the user browser disables JavaScript, the sorting and pagination automatically degenerate
 * to normal page requests and are still functioning as expected.
 *
 * CListView should be used together with a {@link IDataProvider data provider}, preferably a
 * {@link CActiveDataProvider}.
 *
 * The minimal code needed to use CListView is as follows:
 *
 * <pre>
 * $dataProvider=new CActiveDataProvider('Post');
 *
 * $this->widget('zii.widgets.CListView', array(
 *     'dataProvider'=>$dataProvider,
 *     'itemView'=>'_post',   // refers to the partial view named '_post'
 *     'sortableAttributes'=>array(
 *         'title',
 *         'create_time'=>'Post Time',
 *     ),
 * ));
 * </pre>
 *
 * The above code first creates a data provider for the <code>Post</code> ActiveRecord class.
 * It then uses CListView to display every data item as returned by the data provider.
 * The display is done via the partial view named '_post'. This partial view will be rendered
 * once for every data item. In the view, one can access the current data item via variable <code>$data</code>.
 * For more details, see {@link itemView}.
 *
 * In order to support sorting, one has to specify the {@link sortableAttributes} property.
 * By doing so, a list of hyperlinks that can sort the data will be displayed.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @package zii.widgets
 * @since 1.1
 */
class CListViewmm extends CBaseListViewmm {

    /**
     * @var string the view used for rendering each data item.
     * This property value will be passed as the first parameter to either {@link CController::renderPartial}
     * or {@link CWidget::render} to render each data item.
     * In the corresponding view template, the following variables can be used in addition to those declared in {@link viewData}:
     * <ul>
     * <li><code>$this</code>: refers to the owner of this list view widget. For example, if the widget is in the view of a controller,
     * then <code>$this</code> refers to the controller.</li>
     * <li><code>$data</code>: refers to the data item currently being rendered.</li>
     * <li><code>$index</code>: refers to the zero-based index of the data item currently being rendered.</li>
     * <li><code>$widget</code>: refers to this list view widget instance.</li>
     * </ul>
     */
    public $itemView;

    /**
     * @var string the HTML code to be displayed between any two consecutive items.
     * @since 1.1.7
     */
    public $separator;

    /**
     * @var array additional data to be passed to {@link itemView} when rendering each data item.
     * This array will be extracted into local PHP variables that can be accessed in the {@link itemView}.
     */
    public $viewData = array();

    /**
     * @var array list of sortable attribute names. In order for an attribute to be sortable, it must also
     * appear as a sortable attribute in the {@link IDataProvider::sort} property of {@link dataProvider}.
     * @see enableSorting
     */
    public $sortableAttributes;

    /**
     * @var string the template to be used to control the layout of various components in the list view.
     * These tokens are recognized: {summary}, {sorter}, {items} and {pager}. They will be replaced with the
     * summary text, the sort links, the data item list, and the pager.
     */
//    public $template = "{summary}\n{sorter}\n{items}\n{pager}";
    public $template = "{sorter}\n{items}\n{pager}";

    /**
     * @var string the CSS class name that will be assigned to the widget container element
     * when the widget is updating its content via AJAX. Defaults to 'list-view-loading'.
     * @since 1.1.1
     */
    public $loadingCssClass = 'list-view-loading';

    /**
     * @var string the CSS class name for the sorter container. Defaults to 'sorter'.
     */
    public $sorterCssClass = 'col-xs-6 col-sm-6 col-md-3 col-lg-2 sorter';

    /**
     * @var string the text shown before sort links. Defaults to 'Sort by: '.
     */
    public $sorterHeader;

    /**
     * @var string the text shown after sort links. Defaults to empty.
     */
    public $sorterFooter = '';

    /**
     * @var mixed the ID of the container whose content may be updated with an AJAX response.
     * Defaults to null, meaning the container for this list view instance.
     * If it is set false, it means sorting and pagination will be performed in normal page requests
     * instead of AJAX requests. If the sorting and pagination should trigger the update of multiple
     * containers' content in AJAX fashion, these container IDs may be listed here (separated with comma).
     */
    public $ajaxUpdate;

    /**
     * @var string the jQuery selector of the HTML elements that may trigger AJAX updates when they are clicked.
     * If not set, the pagination links and the sorting links will trigger AJAX updates.
     * @since 1.1.7
     */
    public $updateSelector;

    /**
     * @var string a javascript function that will be invoked if an AJAX update error occurs.
     *
     * The function signature is <code>function(xhr, textStatus, errorThrown, errorMessage)</code>
     * <ul>
     * <li><code>xhr</code> is the XMLHttpRequest object.</li>
     * <li><code>textStatus</code> is a string describing the type of error that occurred.
     * Possible values (besides null) are "timeout", "error", "notmodified" and "parsererror"</li>
     * <li><code>errorThrown</code> is an optional exception object, if one occurred.</li>
     * <li><code>errorMessage</code> is the CGridView default error message derived from xhr and errorThrown.
     * Usefull if you just want to display this error differently. CGridView by default displays this error with an javascript.alert()</li>
     * </ul>
     * Note: This handler is not called for JSONP requests, because they do not use an XMLHttpRequest.
     *
     * Example (add in a call to CGridView):
     * <pre>
     *  ...
     *  'ajaxUpdateError'=>'function(xhr,ts,et,err,id){ $("#"+id).text(err); }',
     *  ...
     * </pre>
     * @since 1.1.13
     */
    public $ajaxUpdateError;

    /**
     * @var string the name of the GET variable that indicates the request is an AJAX request triggered
     * by this widget. Defaults to 'ajax'. This is effective only when {@link ajaxUpdate} is not false.
     */
    public $ajaxVar = 'ajax';

    /**
     * @var mixed the URL for the AJAX requests should be sent to. {@link CHtml::normalizeUrl()} will be
     * called on this property. If not set, the current page URL will be used for AJAX requests.
     * @since 1.1.8
     */
    public $ajaxUrl;

    /**
     * @var string the type ('GET' or 'POST') of the AJAX requests. If not set, 'GET' will be used.
     * You can set this to 'POST' if you are filtering by many fields at once and have a problem with GET query string length.
     * Note that in POST mode direct links and {@link enableHistory} feature may not work correctly!
     * @since 1.1.14
     */
    public $ajaxType;

    /**
     * @var string a javascript function that will be invoked before an AJAX update occurs.
     * The function signature is <code>function(id)</code> where 'id' refers to the ID of the list view.
     */
    public $beforeAjaxUpdate;

    /**
     * @var string a javascript function that will be invoked after a successful AJAX response is received.
     * The function signature is <code>function(id, data)</code> where 'id' refers to the ID of the list view
     * 'data' the received ajax response data.
     */
    public $afterAjaxUpdate;

    /**
     * @var string the base script URL for all list view resources (e.g. javascript, CSS file, images).
     * Defaults to null, meaning using the integrated list view resources (which are published as assets).
     */
    public $baseScriptUrl;

    /**
     * @var string the URL of the CSS file used by this list view. Defaults to null, meaning using the integrated
     * CSS file. If this is set false, you are responsible to explicitly include the necessary CSS file in your page.
     */
    public $cssFile;

    /**
     * @var string the HTML tag name for the container of all data item display. Defaults to 'div'.
     * @since 1.1.4
     */
    public $itemsTagName = 'div';

    /**
     * @var boolean whether to leverage the {@link https://developer.mozilla.org/en/DOM/window.history DOM history object}.  Set this property to true
     * to persist state of list across page revisits.  Note, there are two limitations for this feature:
     * - this feature is only compatible with browsers that support HTML5.
     * - expect unexpected functionality (e.g. multiple ajax calls) if there is more than one grid/list on a single page with enableHistory turned on.
     * @since 1.1.11
     */
    public $enableHistory = false;

    /**
     * Initializes the list view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init() {
        
        if ($this->itemView === null)
            throw new CException(Yii::t('zii', 'The property "itemView" cannot be empty.'));
        parent::init();

        if (!isset($this->htmlOptions['class']))
            $this->htmlOptions['class'] = 'list-view';

        if ($this->baseScriptUrl === null)
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')) . '/listview';

        if ($this->cssFile !== false) {
            if ($this->cssFile === null)
                $this->cssFile = $this->baseScriptUrl . '/styles.css';
            Yii::app()->getClientScript()->registerCssFile($this->cssFile);
        }
    }

    /**
     * Registers necessary client scripts.
     */
    public function registerClientScript() {
        $id = $this->getId();

        if ($this->ajaxUpdate === false)
            $ajaxUpdate = array();
        else
            $ajaxUpdate = array_unique(preg_split('/\s*,\s*/', $this->ajaxUpdate . ',' . $id, -1, PREG_SPLIT_NO_EMPTY));
        $options = array(
            'ajaxUpdate' => $ajaxUpdate,
            'ajaxVar' => $this->ajaxVar,
            'pagerClass' => $this->pagerCssClass,
            'loadingClass' => $this->loadingCssClass,
            'sorterClass' => $this->sorterCssClass,
            'enableHistory' => $this->enableHistory
        );
        if ($this->ajaxUrl !== null)
            $options['url'] = CHtml::normalizeUrl($this->ajaxUrl);
        if ($this->ajaxType !== null)
            $options['ajaxType'] = strtoupper($this->ajaxType);
        if ($this->updateSelector !== null)
            $options['updateSelector'] = $this->updateSelector;
        foreach (array('beforeAjaxUpdate', 'afterAjaxUpdate', 'ajaxUpdateError') as $event) {
            if ($this->$event !== null) {
                if ($this->$event instanceof CJavaScriptExpression)
                    $options[$event] = $this->$event;
                else
                    $options[$event] = new CJavaScriptExpression($this->$event);
            }
        }

        $options = CJavaScript::encode($options);
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('bbq');
        if ($this->enableHistory)
            $cs->registerCoreScript('history');
        $cs->registerScriptFile($this->baseScriptUrl . '/jquery.yiilistview.js', CClientScript::POS_END);
        $cs->registerScript(__CLASS__ . '#' . $id, "jQuery('#$id').yiiListView($options);");
    }

    /**
     * Renders the data item list.
     */
    public function renderItems() {
        /*
         * Renderiza items y formulario
         */
 
        echo CHtml::openTag($this->itemsTagName, array('class' => "product_box1")) . "\n";
        $data = $this->dataProvider->getData();
        if (($n = count($data)) > 0) {
            $owner = $this->getOwner();
            $viewFile = $owner->getViewFile($this->itemView);
            $j = 0;
            foreach ($data as $i => $item) {
                $data = $this->viewData;
                $data['index'] = $i;
                $data['data'] = $item;
                $data['widget'] = $this;
                $owner->renderFile($viewFile, $data);
                if ($j++ < $n - 1)
                    echo $this->separator;
            }
        } else
            $this->renderEmptyText();
        echo CHtml::closeTag($this->itemsTagName);



    }

    /**
     * Renders the sorter.
     */
    public function renderSorter() {
        if ($this->dataProvider->getItemCount() <= 0 || !$this->enableSorting || empty($this->sortableAttributes))
            return;
        echo CHtml::openTag('div', array('class' => $this->sorterCssClass)) . "\n";
        echo $this->sorterHeader === null ? Yii::t('zii', 'Sort by: ') : $this->sorterHeader;
        echo "<ul>\n";
        $sort = $this->dataProvider->getSort();
        foreach ($this->sortableAttributes as $name => $label) {
            echo "<li>";
            if (is_integer($name))
                echo $sort->link($label);
            else
                echo $sort->link($name, $label);
            echo "</li>\n";
        }
        echo "</ul>";
        echo $this->sorterFooter;
        echo CHtml::closeTag('div');
    }

    public function miga() {
        if (($count = $this->dataProvider->getItemCount()) <= 0)
            return;

        $curpage = "";
        $curpage = Yii::app()->getController()->getAction()->controller->id;
        $curpage .= '/' . Yii::app()->getController()->getAction()->controller->action->id;

        $gets[] = "";
        $listactive = "";
        $gridactive = "";
//        if (CHttpRequest::getParam('Proyecto_sort')){
//            $gets['Proyecto_sort'] = CHttpRequest::getParam('Proyecto_sort');
//        }
        if ($curpage == "buscar/index") {
            $ps = $_GET['Proyecto'];
            if ($ps) {
                $gets['Proyecto'] = $ps;
            }
        } else {
            $ps = filter_input(INPUT_GET, 'Proyecto_sort');
            if ($ps) {
                $gets['Proyecto_sort'] = $ps;
            }
        }

        $st = filter_input(INPUT_GET, 'style');
        
        if ($st == "grid") {
            $gridactive = ' active';
            $listactive = '';
        } elseif ($st == "list") {
            $gridactive = '';
            $listactive = ' active';
        } else {
            $gridactive = '';
            $listactive = ' class="active"';
        }

        $pg = Yii::app()->request->getParam('Proyecto_page', "false");
        if ($pg != "false") {
            $gets["Proyecto_page"] = $pg;
        }
        if (count($gets) == 0) {
            unset($gets);
        } else {
            unset($gets[0]);
        }

        $gets["style"] = "list";
        
        $link = Yii::app()->controller->createUrl($curpage, $gets);
        /* BOTONES DE ORDEN VISUAL */
        
        echo '<div class="col-lg-12 col-md-12">
  <div class="container margin--t1 margin--b1">
    <div class="row">
      <div class="col-lg-6 col-md-6 hidden-xs hidden-sm">
        <ul class="nav nav-pills breadcumb">
          <li role="presentation"><a href="' . Yii::app()->controller->createUrl("site/index") . '">Inicio &gt;</a></li>
          <li role="presentation" class="active"><a href="#">Buscar</a></li>
          
        </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 text-right">
        <div class="btn-group" role="group">';
        
          echo '<a  href="'.$link.'" class="btn btn-default'.$listactive.'">Lista</a>';
          
          $gets["style"] = "grid";
          $link = Yii::app()->controller->createUrl($curpage, $gets);
          echo '<a  href="'.$link.'" class="btn btn-default'.$gridactive.'">Foto</a>';
          echo '<a  href="'.Yii::app()->controller->createUrl("buscar/mapa").'" class="btn btn-default">Mapa</a>';
          
          
          /* <button type="button" onclick="window.location.href("'.$link.'")" class="btn btn-default">Lista</button>
          <button type="button" onclick="window.location.href("'.$link.'")" class="btn btn-default active">Foto</button> 
          <button type="button" onclick="window.location.href("'.Yii::app()->controller->createUrl("buscar/mapa").'")" class="btn btn-default">Mapa</button>*/
        echo '</div>
      </div>
    </div>
  </div>
</div>';
    }

    public function paginadorSuperior() {
        
    }

    public function paginadorInferior() {
//        echo '<div class="col-lg-12 col-md-12 paginationBar hidden-xs hidden-sm">
//  <div class="row">
//    <div class="container text-center">
//      <nav aria-label="Page navigation" style="margin-top:0;margin-bottom:0;">
//        <ul class="pagination" style="margin-bottom:0;">
//          <li>
//            <a href="#" aria-label="Previous">
//              Anterior
//            </a>
//          </li>
//          <li><a href="#">1</a></li>
//          <li><a href="#">2</a></li>
//          <li><a href="#">3</a></li>
//          <li><a href="#">4</a></li>
//          <li>
//            <a href="#" aria-label="Next">
//              Siguiente
//            </a>
//          </li>
//        </ul>
//      </nav>
//    </div>
//  </div>
//</div>';
    }

    public function renderHeaderpersonal() {

        echo $this->miga();
        
//        echo '<div class="col-lg-12 col-md-12 margin--t1 mi ">
//    <div class="container ">';
        echo $this->elSumario();
//
//        echo '
//    </div>
//</div>';
    }

    function elSorter() {
        if ($this->dataProvider->getItemCount() <= 0 || !$this->enableSorting || empty($this->sortableAttributes))
            return;
        echo CHtml::openTag('div', array('class' => 'dropdown')) . "\n";

        echo '<button class="elsorter dropdown-toggle" type="button" data-toggle="dropdown">Ordenar por:
    <span class="caret"></span></button>';
        echo '<ul id="customsort" class="dropdown-menu">'."\n";
		$sort=$this->dataProvider->getSort();
		foreach($this->sortableAttributes as $name=>$label)
		{
			echo "<li>";
			if(is_integer($name))
				echo $sort->link($label);
			else
				echo $sort->link($name,$label);
			echo "</li>\n";
		}
		echo "</ul>";
        echo $this->sorterFooter;
        echo CHtml::closeTag('div');
    }

    public function elSumario() {
 if (!$this->enablePagination)
            return;

        $pager = array();
        $class = 'CLinkPager';
        if (is_string($this->pager))
            $class = $this->pager;
        elseif (is_array($this->pager)) {
            $pager = $this->pager;
            if (isset($pager['class'])) {
                $class = $pager['class'];
                unset($pager['class']);
            }
        }
        $pager['pages'] = $this->dataProvider->getPagination();

        echo '<div class="col-lg-12 col-md-12 paginationBar hidden-xs hidden-sm">
  <div class="row">
    <div class="container">
      <div class="col-lg-4 col-md-4 text-center veticaladd">';
       if($pager['pages']->getPageCount() > 1) {
            echo '<p>' . $this->resumen() . '</p>';
        }
        echo '</div>
      <div class="class111">
        <nav aria-label="Page navigation">
          <h4 class="text-uppercase">Usted encontro <strong>' . $pager['pages']->getItemCount() . '  RESULTADOS</strong></h4>';

        if ($pager['pages']->getPageCount() > 1) {
            $this->widget($class, $pager);
        } else
            $this->widget($class, $pager);

        echo '</nav>
      </div>
      <div class="col-lg-4 col-md-4 veticaladd">
        <form>
          <div class="form-group">';
          echo $this->elSorter();
//          echo '<select class="form-control">
//              <option value="">Ordernar por</option>
//            </select>';
          echo '</div>
        </form>
      </div>
    </div>
  </div>
</div>';
          
    }

    public function renderOpenlist() {
        echo '';
    }

    public function renderSumariox() {

        //Pagina <b>2</b> de 36

        if (($count = $this->dataProvider->getItemCount()) <= 0)
            return;

        echo CHtml::openTag($this->summaryTagName, array('class' => 'counter pull'));
        if ($this->enablePagination) {
            $pagination = $this->dataProvider->getPagination();
            $total = $this->dataProvider->getTotalItemCount();
            $start = $pagination->currentPage * $pagination->pageSize + 1;
            $end = $start + $count - 1;
            if ($end > $total) {
                $end = $total;
                $start = $end - $count + 1;
            }
            if (($summaryText = $this->summaryText) === null)
                $summaryText = '<div class="limiter visible-desktop"><label>' . Yii::t('zii', 'Displaying {start}-{end} of 1 result.|Displaying {start}-{end} of {count} results.', $total) . "</label></div>";
            echo strtr($summaryText, array(
                '{start}' => $start,
                '{end}' => $end,
                '{count}' => $total,
                '{page}' => $pagination->currentPage + 1,
                '{pages}' => $pagination->pageCount,
            ));
        }
        else {
            if (($summaryText = $this->summaryText) === null)
                $summaryText = Yii::t('zii', 'Total 1 result.|Total {count} results.', $count);
            echo strtr($summaryText, array(
                '{count}' => $count,
                '{start}' => 1,
                '{end}' => $count,
                '{page}' => 1,
                '{pages}' => 1,
            ));
        }
        echo CHtml::closeTag($this->summaryTagName);
    }

    public function renderCloselist() {
        echo '';
    }

}
